package com.willy.util.string;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.LoadingCache;
import com.willy.util.cache.WCache;
import com.willy.util.date.WDate;
import com.willy.util.type.WType;

public class WString {
	public final static String nextLine = "\r\n";
	public final static String splitLine = "-----------------------------------------------------------";
	public final static String splitToNextLine = "-----------------------------------------------------------\r\n";
	private static final Logger LOGGER = LoggerFactory.getLogger(WString.class);

	// 從將${}中的值視為key，從cache中取出值，將${}替換掉
	public static String parseParamsToStrByCache(String rawStr) throws ExecutionException {
		String key;
		List<String> matchedStrList = WRegex.getMatchedStrList(rawStr, "[${][^}]*[}]");
		for (String matchedStr : matchedStrList) {
			key = matchedStr.substring(matchedStr.indexOf("{") + 1, matchedStr.length() - 1);// 去掉括號
			if(WCache.cache.getIfPresent(key) == null) {
				continue;
			}
			rawStr = rawStr.replace(matchedStr, WCache.cache.get(key).toString());
		}
		return rawStr;
	}

	/**
	 * mapStr格式為 key:value,key1:value1
	 * 
	 * @param rawStr
	 * @param json
	 * @return
	 * @throws JSONException 
	 */
	public static String parseParamsToStrByJson(String rawStr, JSONObject json) throws JSONException {
		String paramKey;
		List<String> params = WRegex.getMatchedStrList(rawStr, "[${][^}]*[}]");
		for (String param : params) {
			paramKey = param.substring(2, param.length() - 1);
			if(json.has(paramKey)) {
				rawStr = rawStr.replace(param, json.getString(paramKey));
			}
		}
		return rawStr;
	}

	/**
	 * 填滿字串長度 length>0加在後面ㄝ, length<0加在前面
	 * 
	 * @param intStr
	 * @param chars
	 * @param length
	 * @return
	 */
	public static String fillStrLength(int intStr, String chars, int length) {
		return fillStrLength(String.valueOf(intStr), chars, length);
	}
	public static String fillStrLength(String str, String chars, int length) {
		if (length < 0) {
			length = -length;
			while (str.length() < length) {
				str = chars + str;
			}
		} else {
			while (str.length() < length) {
				str = str + chars;
			}
		}

		return str;
	}

	/**
	 * 移除多個字元
	 * 
	 * @param str
	 * @param extraStrs rexss
	 */
	public static String rmStr(String str, String... extraStrs) {
		for (String extraStr : extraStrs) {
			str = str.replace(extraStr, "");
		}
		return str;
	}

	public static String rmStr(String str, List<String> extraStrs) {
		for (String extraStr : extraStrs) {
			str = str.replace(extraStr, "");
		}
		return str;
	}

	public static List<String> rmStrFromStrList(List<String> strList, String... extraStrs) {
		for (String str : strList) {
			for (String extraStr : extraStrs) {
				str = str.replace(extraStr, "");
			}
		}
		return strList;
	}

	public static boolean isNumeric(String str) {
		if (str != null && !"".equals(str.trim())) {
			return str.matches("[-]?[0-9]*[.]?[0-9]*");
		} else {
			return false;
		}
	}

	public static String getInSqlScriptByList(List<String> objList) {
		StringBuffer inScriptSb = new StringBuffer();
		inScriptSb.append(" (");
		for (int objIndex = 0; objIndex < objList.size() - 1; objIndex++) {
			inScriptSb.append("?,");
		}
		inScriptSb.append("?) ");
		return inScriptSb.toString();
	}

	public static String arToCsvFormat(String[] strAr) {
		StringBuffer resultSb = new StringBuffer();
		for (String cellStr : strAr) {
			resultSb.append("\"").append(cellStr).append("\",");
		}
		resultSb = resultSb.replace(resultSb.length() - 1, resultSb.length(), "\r\n");
		return resultSb.toString();
	}

	public static String arToCsvFormat(String[][] strAr) {
		StringBuffer resultSb = new StringBuffer();
		for (String[] rowAr : strAr) {
			resultSb.append(arToCsvFormat(rowAr));
		}
		return resultSb.toString();
	}

	public static String arListToCsvFormat(List<String[]> strAr) {
		StringBuffer resultSb = new StringBuffer();
		for (String[] rowAr : strAr) {
			resultSb.append(arToCsvFormat(rowAr));
		}
		return resultSb.toString();
	}

	/**
	 * 取得被前後綴字元包含的字串
	 * 
	 * @param str
	 * @param prefix
	 * @param suffix
	 * @return
	 */
	public static List<String> getIncludedStrList(String str, String prefix, String suffix) {
		String tempStr = str;
		LinkedList<Integer> prefixIndexList = new LinkedList<Integer>();
		LinkedList<Integer> suffixIndexList = new LinkedList<Integer>();

		// 蒐集前綴字元IndexList
		int prefixIndex = tempStr.indexOf(prefix);
		while (prefixIndex > -1) {
			// 因為第二輪開始會substring所以需加入上一輪的index+1
			prefixIndexList.add(prefixIndex
					+ (prefixIndexList.size() > 0 ? (prefixIndexList.get(prefixIndexList.size() - 1) + 1) : 0));
			tempStr = tempStr.substring(prefixIndex + 1);
			prefixIndex = tempStr.indexOf(prefix);
		}

		// 蒐集後綴字元IndexList
		tempStr = str;
		int sufffixIndex = tempStr.indexOf(suffix);
		while (sufffixIndex > -1) {
			suffixIndexList.add(sufffixIndex
					+ (suffixIndexList.size() > 0 ? (suffixIndexList.get(suffixIndexList.size() - 1) + 1) : 0));
			tempStr = tempStr.substring(sufffixIndex + 1);
			sufffixIndex = tempStr.indexOf(suffix);
		}

		// 統整出被前後綴包含字串
		ArrayList<String> funcList = new ArrayList<String>();
		for (int suffixIndex : suffixIndexList) {
			for (int prefIndex = 0; prefIndex < prefixIndexList.size(); prefIndex++) {
				if (prefixIndexList.get(prefIndex) > suffixIndex) {
					funcList.add(str.substring(prefixIndexList.get(prefIndex - 1), suffixIndex + 1));
					prefixIndexList.remove(prefIndex - 1);
					break;
				} else if (prefIndex == prefixIndexList.size() - 1) {
					funcList.add(str.substring(prefixIndexList.get(prefIndex), suffixIndex + 1));
					prefixIndexList.remove(prefIndex);
				}
			}
		}
		return funcList;
	}

	public static String rTrim(String str) {
		return StringUtils.stripEnd(str, null);
	}

	public static String[] combineArray(String[] array1, String[] array2) {
		int ar1Length = array1.length;
		String[] result = new String[array1.length + array2.length];
		for (int i = 0; i < ar1Length; i++) {
			result[i] = array1[i];
		}
		for (int i = ar1Length; i < array2.length + ar1Length; i++) {
			result[i] = array1[i - ar1Length];
		}
		return result;
	}

	@SuppressWarnings("restriction")
	public static String compress(String str) throws IOException {
		if (str == null || str.length() == 0) {
			return str;
		}
		ByteArrayOutputStream out = null;
		GZIPOutputStream gzip = null;
		String compress = "";
		try {
			out = new ByteArrayOutputStream();
			gzip = new GZIPOutputStream(out);
			gzip.write(str.getBytes());
			gzip.close();
			compress = Base64.getEncoder().encodeToString(out.toByteArray());
		} catch (IOException e) {
			LOGGER.error("字串[" + str + "]壓縮失敗", e);
		} finally {
			if (null != out) {
				try {
					out.close();
				} catch (IOException e) {
					LOGGER.error("字串壓縮[" + str + "]壓縮失敗", e);
				}
			}
		}
		return compress;
	}

	// 解壓縮
	public static String unCompress(String str) throws IOException {
		if (str == null || str.length() == 0) {
			return str;
		}
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ByteArrayInputStream in = new ByteArrayInputStream(Base64.getDecoder().decode(str));
		GZIPInputStream gunzip = new GZIPInputStream(in);
		byte[] buffer = new byte[256];
		int n;
		while ((n = gunzip.read(buffer)) >= 0) {
			out.write(buffer, 0, n);
		}
		// toString()使用平臺預設編碼，也可以顯式的指定如toString("GBK")
		return out.toString();
	}

	public static String toString(Object obj) {
		StringBuffer sb = new StringBuffer();
		try {
			if (obj instanceof String) {
				return obj.toString();
			} else if (obj instanceof String[]) {
				String[] ar = (String[]) obj;
				for (int i = 0; i < ar.length; i++) {
					sb.append(i + ":" + ar[i] + ",");
				}
				if (sb.length() > 0) {
					sb = sb.delete(sb.length() - 1, sb.length());
				}
				sb.append("\r\n");
			} else if (obj instanceof String[][]) {
				String[][] ar2d = (String[][]) obj;
				for (int i = 0; i < ar2d.length; i++) {
					for (int j = 0; j < ar2d[i].length; j++) {
						sb.append(ar2d[i][j]).append("\t");
					}
					sb.append("\r\n");
				}
			} else if (obj instanceof double[][]) {
				double[][] ar2d = (double[][]) obj;
				for (int i = 0; i < ar2d.length; i++) {
					for (int j = 0; j < ar2d[i].length; j++) {
						sb.append(ar2d[i][j]).append("\t");
					}
					sb.append("\r\n");
				}
			} else if (obj instanceof Object[][]) {
				Object[][] ar2d = (Object[][]) obj;
				for (int i = 0; i < ar2d.length; i++) {
					for (int j = 0; j < ar2d[i].length; j++) {
						sb.append(ar2d[i][j]).append("\t");
					}
					sb.append("\r\n");
				}
			} else if (obj instanceof List<?>) {
				List<?> list = (List) obj;
				for (Object o : list) {
					sb.append(toString(o)).append(",");
				}
				if (sb.length() > 0) {
					sb = sb.delete(sb.length() - 1, sb.length());
				}
				sb.append("\r\n");
			} else if (obj instanceof Set<?>) {
				Set<?> list = (Set) obj;
				for (Object o : list) {
					sb.append(toString(o)).append(",");
				}
				sb = sb.delete(sb.length() - 1, sb.length());
				sb.append("\r\n");
			} else if (obj instanceof Map) {
				Map map = (Map) obj;
				Set<String> keySet = map.keySet();
				for (Object key : keySet) {
					sb.append("{Key:").append(toString(key)).append(",value:").append(toString(map.get(key)))
							.append("}\r\n");
				}
			} else if (obj instanceof ResultSet) {
				ResultSet rs = (ResultSet) obj;
				int columnCount = rs.getMetaData().getColumnCount();
				rs.last();
				int rowCount = rs.getRow();
				rs.first();
				for (int rowIndex = 1; rowIndex < rowCount; rowIndex++) {
					for (int columnIndex = 1; columnIndex < columnCount; columnIndex++) {
						sb.append(rs.getString(columnIndex)).append("\t");
					}
					rs.next();
					sb.append("\r\n");
				}
			} else if (obj instanceof BigDecimal) {
				return obj.toString();
			} else if (obj instanceof Long || obj instanceof Integer || obj instanceof Double || obj instanceof Float) {
				return String.valueOf(obj);
			} else if (obj instanceof Date) {
				return WType.dateToStr((Date) obj, WDate.dateFormat_DateTime);
			} else {
				return ToStringBuilder.reflectionToString(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public static String getDomainNameByUrl(String url) {
		String urlDomain = url.substring(url.indexOf("/") + 2);// 去除http://
		urlDomain = urlDomain.substring(0, urlDomain.indexOf("/") > -1 ? urlDomain.indexOf("/") : urlDomain.length());
		return urlDomain;
	}

	public static String[][] subCol(String[][] strAr, String colIndexs) {
		if (strAr == null || strAr.length == 0) {
			return new String[1][1];
		}
		String[][] result = new String[strAr.length][strAr[0].length];
		for (int rowIndex = 0; rowIndex < strAr.length; rowIndex++) {
			result[rowIndex] = subCol(strAr[rowIndex], colIndexs);
		}
		return result;
	}

	public static String[] subCol(String[] strAr, String colIndexs) {
		if (strAr == null || strAr.length == 0) {
			return new String[1];
		}
		String[] colIndexAr = colIndexs.split(",");
		String[] result = new String[colIndexAr.length];
		int newColIndex = 0;
		for (String colIndex : colIndexAr) {
			if (strAr.length > Integer.valueOf(colIndex)) {
				result[newColIndex] = strAr[Integer.valueOf(colIndex)];
			}
			newColIndex++;

		}
		return result;
	}

	public static String[][] subStrAr(String[][] strAr, int startRow) {
		return subStrAr(strAr, startRow, strAr.length);
	}

	public static String[][] subStrAr(String[][] strAr, int startRow, int endRow) {
		if (strAr == null || strAr.length < 1 || startRow < 1) {
			return strAr;
		}

		int resultRow = 0;
		String[][] result = new String[strAr.length - startRow][strAr[0].length];
		for (int rowIndex = startRow; rowIndex < strAr.length; rowIndex++) {
			result[resultRow] = strAr[rowIndex];
			resultRow++;
			if (rowIndex >= endRow) {
				break;
			}
		}
		return result;
	}

	public static String cleanDigitData(String rawData) {
		rawData = WString.rmStr(rawData, ",", "\"", "\t", " ", "-");
		rawData = (rawData.equals("")) ? "0" : rawData;
		return rawData;
	}

	public static String cleanIntegerData(String rawData) {
		rawData = WString.rmStr(rawData, ",", "\"", "\t", " ");
		rawData = (rawData.equals("")) ? "0" : rawData;
		if (rawData.indexOf(".") > -1) {
			rawData = rawData.substring(0, rawData.indexOf("."));
		}
		return rawData;
	}

	/**
	 * 全形自轉半形
	 * 
	 * @param fullTypeStr
	 * @return
	 */
	public static String fullTypeString2Half(String fullTypeStr) {
		if (null == fullTypeStr || fullTypeStr.length() <= 0) {
			return "";
		}
		char[] charArray = fullTypeStr.toCharArray();
		// 對全形字元轉換的char陣列遍歷
		for (int i = 0; i < charArray.length; i++) {
			int charIntValue = (int) charArray[i];
			// 如果符合轉換關係,將對應下標之間減掉偏移量65248;如果是空格的話,直接做轉換
			if (charIntValue >= 65281 && charIntValue <= 65374) {
				charArray[i] = (char) (charIntValue - 65248);
			} else if (charIntValue == 12288) {
				charArray[i] = (char) 32;
			}
		}
		return new String(charArray);
	}

	public static String[] fullTypeString2Half(String[] fullTypeStrArray) {
		String[] halfTypeStrArray = new String[fullTypeStrArray.length];
		for (int i = 0; i < fullTypeStrArray.length; i++) {
			halfTypeStrArray[i] = fullTypeString2Half(fullTypeStrArray[i]);
		}
		return halfTypeStrArray;
	}
	
	public static String unicodeToString(String str) {
		Pattern pattern = Pattern.compile("(\\\\u(\\p{XDigit}{4}))");
        Matcher matcher = pattern.matcher(str);
        char ch;
        while (matcher.find()) {
            ch = (char) Integer.parseInt(matcher.group(2), 16);
            str = str.replace(matcher.group(1), ch+"" );
        }
		return str;
	}
}