package com.willy.util.string;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WRegex {

	public final static String _REGEX_BRACKETS_WRAPED = "[(][^)(]*[)]";
	public final static String _REGEX_SQUARE_BRACKETS_WRAPED = "[\\[][^\\]\\[]*[\\]]";
	public final static String _REGEX_CURLY_BRACKETS_WRAPED = "[{][^}{]*[}]";
	public final static String _REGEX_PARAMETER_WRAPED = "[$][{][^}]*[}]";


	public static boolean match(String str, String regex) {
		return Pattern.matches(regex, str);
	}

	public static List<String> getMatchedStrList(String str, String regex) {
		if(str == null) {
			return null;
		}
		List<String> matchedStrList = new LinkedList<>();
		Pattern r = Pattern.compile(regex);
		Matcher m = r.matcher(str);
		while (m.find()) {
			matchedStrList.add(m.group().trim());
        }
		return matchedStrList;
	}
	public static Set<String> getMatchedStrSet(String str, String regex) {
		return new HashSet<> (getMatchedStrList(str, regex));
	}
}
