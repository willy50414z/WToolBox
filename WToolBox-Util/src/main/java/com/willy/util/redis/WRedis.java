package com.willy.util.redis;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Service;

import redis.clients.jedis.Jedis;

@Service
@ConditionalOnProperty(name = {"spring.redis.host", "spring.redis.port"}) 
public class WRedis {
	@Autowired
    private RedisTemplate<String,Object> redisTemplate;
	@Autowired
	private Jedis jedis;

    public void put(String key, Object value) {
        RedisSerializer<String> redisSerializer =new StringRedisSerializer();
        redisTemplate.setKeySerializer(redisSerializer);
        ValueOperations<String,Object> vo = redisTemplate.opsForValue();
        vo.set(key, value);
    }

    public Object get(String key) {
        ValueOperations<String,Object> vo = redisTemplate.opsForValue();
        return vo.get(key);
    }
    
    public Set<String> getKeys() {
        return getKeys("*");
    }
    
    public Set<String> getKeys(String pattern) {
        return jedis.keys(pattern);
    }
    
    public Long del(String... keys) {
        return jedis.del(keys);
    }
    
    public boolean exists(String key) {
        return jedis.exists(key);
    }
    
    public Long exists(String... keys) {
        return jedis.exists(keys);
    }
}
