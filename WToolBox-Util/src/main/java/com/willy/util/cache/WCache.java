package com.willy.util.cache;

import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;

public class WCache {
	public static LoadingCache<String, Object> cache;

	static {
		cache = CacheBuilder.newBuilder().maximumSize(30)
				// 寫入後5分鐘清掉cache
				.expireAfterAccess(5, TimeUnit.MINUTES)
				// 加入cache清理監聽
				.removalListener(new RemovalListener<String, Object>() {
					@Override
					public void onRemoval(RemovalNotification<String, Object> notification) {
						// TODO Auto-generated method stub
						WLog.trace("WCache clear Key: " + WString.toString(notification.getKey()) + "\tvalue: ");
					}
				})
				// 加入cacheloader
				.build(new CacheLoader<String, Object>() {
					// 沒有key時回傳null
					public String load(String key) throws Exception {
						return null;
					}
				});
	}

	public static <T, S> LoadingCache<T, S> getDefaultCache() {
		return (LoadingCache<T, S>) CacheBuilder.newBuilder().maximumSize(30)
				// 寫入後5分鐘清掉cache
				.expireAfterWrite(5, TimeUnit.MINUTES)
				// 加入cache清理監聽
				.removalListener(new RemovalListener<String, Object>() {
					@Override
					public void onRemoval(RemovalNotification<String, Object> notification) {
						// TODO Auto-generated method stub
						WLog.trace("WCache clear Key: " + WString.toString(notification.getKey()) + "\tvalue: ");
					}
				})
				// 加入cacheloader
				.build(new CacheLoader<String, Object>() {
					// 沒有key時回傳null
					public String load(String key) throws Exception {
						return null;
					}
				});
	}
	
	public static void waitBetweenAccess(String cacheKey,int interval) throws InterruptedException, ExecutionException {
		String lastAccessTimeKey = cacheKey+"_lastAccessTime";
		Date lastAccessTime = (Date) cache.getIfPresent(lastAccessTimeKey);
		if(lastAccessTime == null) {
			cache.put(lastAccessTimeKey, new Date());
		}else {
			Thread.sleep(Math.max(0,interval - (new Date().getTime() - lastAccessTime.getTime())));
			cache.put(lastAccessTimeKey, new Date());
		}
	}
}
