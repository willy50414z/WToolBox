package com.willy.util.reflect;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.willy.util.cache.WCache;
import com.willy.util.type.WType;

public class WReflect {
	private static LoadingCache<String, Object> reflectionCache = CacheBuilder.newBuilder().maximumSize(30)
			.expireAfterAccess(5, TimeUnit.MINUTES)
			.build(new CacheLoader<String, Object>() {
				public String load(String key) throws Exception {
					return null;
				}
			});
	public static Class<?> getFieldClass(Object obj, String fieldName) throws Exception {
		Class<?> result = null;
		try {
			HashMap<String, Field> fields = getAllFieldMap(obj);
			Field field = fields.get(fieldName.toLowerCase());
			result = field.getType();
		} catch (Exception e) {
			throw e;
		}
		return result;
	}
	public static Object getFieldValue(Object obj, String fieldName) throws Exception {
		Object result = null;
		try {
			HashMap<String, Field> fields = getAllFieldMap(obj);
			Field field = fields.get(fieldName.toLowerCase().replace("_", ""));
			field.setAccessible(true);
			result = field.get(obj);
		} catch (Exception e) {
			throw e;
		}
		return result;
	}
	public static void setFieldValue(Object obj, String fieldName,Object value) throws Exception {
		try {
			Field field = obj.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			field.set(obj, value);
		} catch (Exception e) {
			throw e;
		}
	}
	public static Object setFieldValueByLowerCaseFieldName(Object obj, String fieldName,Object value) throws Exception {
		Object result = null;
		HashMap<String, Field> fieldMap = getAllFieldMap(obj);
		try {
			Field field = fieldMap.get(fieldName.toLowerCase());
			field.setAccessible(true);
			field.set(obj, WType.objToSpecificType(value,field.getType()));
		} catch (Exception e) {
			throw e;
		}
		return result;
	}

	public static Object getFieldValueByGetMethod(Object obj, String paramName) throws Exception {
		Object result = null;
		try {
			List<Method> methodList = getAllMethodList(obj);
			boolean noMethod = true;
			for (Method method : methodList) {
				if (method.getName().equalsIgnoreCase("get" + paramName)) {
					result = method.invoke(obj);
					noMethod = false;
				}
			}
		} catch (Exception e) {
			throw e;
		}
		return result;
	}
	// 一般物件使用
	public static void setFieldValueBySetMethod(Object obj, String paramName, Object paramValue)
			throws Exception, SecurityException {
		Method method;
		HashMap<String, Method> methodMap;
		try {
			if (paramValue != null) {
				methodMap = getAllMethodMap(obj.getClass());
				method = methodMap.get("set" + paramName.toLowerCase());
				if (paramValue.getClass().equals(method.getParameterTypes()[0])) {
					method.invoke(obj, paramValue);
				} else {
					method.invoke(obj, WType.objToSpecificType(paramValue, method.getGenericParameterTypes()[0]));
				}

			}
		} catch (Exception e) {
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public static List<Method> getAllMethodList(Object obj) throws ExecutionException {
		String allMethodsKey = "AllMethodList_" + obj.getClass().getSimpleName();
		if (WCache.cache.getIfPresent(allMethodsKey)!=null) {
			return (List<Method>) reflectionCache.get(allMethodsKey);
		} else {
			List<Method> methodList = new ArrayList<>();
			Class<?> cl = obj.getClass();
			// 取得所有Field
			while (cl != null) {
				methodList.addAll(Arrays.asList(cl.getDeclaredMethods()));
				cl = cl.getSuperclass(); // 得到父类,然后赋给自己
			}
			reflectionCache.put(allMethodsKey, methodList);
			return methodList;
		}
	}

	public static List<Method> getAllMethodList(Class<?> cl) throws InstantiationException, IllegalAccessException, ExecutionException {
		return getAllMethodList(cl.newInstance());
	}

	public static HashMap<String, Method> getAllMethodMap(Class<?> cl) throws ExecutionException {
		String allStrMethodMapKey = "AllStrMethodMap" + cl.getSimpleName();
		if (reflectionCache.getIfPresent(allStrMethodMapKey)!=null) {
			return (HashMap<String, Method>) reflectionCache.get(allStrMethodMapKey);
		} else {
			Method[] methodAr;
			HashMap<String, Method> methodMap = new HashMap<String, Method>();
			// 取得所有Field
			while (cl != null) {
				methodAr = cl.getDeclaredMethods();
				for (Method m : methodAr) {
					methodMap.put(m.getName().toLowerCase(), m);
				}
				cl = cl.getSuperclass(); // 得到父类,然后赋给自己
			}
			reflectionCache.put(allStrMethodMapKey, methodMap);
			return methodMap;
		}
	}

	public static HashMap<String, Method> getAllStrMethodMap(Class<?> cl) throws ExecutionException {
		String allStrMethodMapKey = "AllStrMethodMap_" + cl.getSimpleName();
		if (reflectionCache.getIfPresent(allStrMethodMapKey)!=null) {
			return (HashMap<String, Method>) reflectionCache.get(allStrMethodMapKey);
		} else {
			Method[] methodAr;
			HashMap<String, Method> methodMap = new HashMap<String, Method>();
			// 取得所有Field
			while (cl != null) {
				methodAr = cl.getDeclaredMethods();
				for (Method m : methodAr) {
					if (m.getParameterTypes().length > 0 && m.getParameterTypes()[0].getSimpleName().equals("String")) {
						methodMap.put(m.getName().toLowerCase(), m);
					}
				}
				cl = cl.getSuperclass(); // 得到父类,然后赋给自己
			}
			reflectionCache.put(allStrMethodMapKey, methodMap);
			return methodMap;
		}
	}

	public static HashMap<String, Method> getAllStrMethodMap(Object obj) throws ExecutionException {
		String allStrMethodMapKey = "AllStrMethodMap_" + obj.getClass().getSimpleName();
		if (reflectionCache.getIfPresent(allStrMethodMapKey)!=null) {
			return (HashMap<String, Method>) reflectionCache.get(allStrMethodMapKey);
		} else {
			Method[] methodAr;
			HashMap<String, Method> methodMap = new HashMap<String, Method>();
			Class cl = obj.getClass();
			// 取得所有Field
			while (cl != null) {
				methodAr = cl.getDeclaredMethods();
				for (Method m : methodAr) {
					if (m.getParameterTypes().length > 0 && m.getParameterTypes()[0].getSimpleName().equals("String")) {
						methodMap.put(m.getName().toLowerCase(), m);
					}
				}
				cl = cl.getSuperclass(); // 得到父类,然后赋给自己
			}
			reflectionCache.put(allStrMethodMapKey, methodMap);
			return methodMap;
		}
	}

	public static List<Field> getAllFieldList(Object obj) throws ExecutionException {
		String allFieldsListKey = "AllFieldList_" + obj.getClass().getSimpleName();
		if (reflectionCache.getIfPresent(allFieldsListKey)!=null) {
			return (List<Field>) reflectionCache.get(allFieldsListKey);
		} else {
			List<Field> fieldList = new ArrayList<>();
			Class cl = obj.getClass();
			// 取得所有Field
			while (cl != null) {
				fieldList.addAll(Arrays.asList(cl.getDeclaredFields()));
				cl = cl.getSuperclass(); // 得到父类,然后赋给自己
			}
			reflectionCache.put(allFieldsListKey, fieldList);
			return fieldList;
		}
	}

	public static HashMap<String, Field> getAllFieldMap(Object obj) throws ExecutionException {
		String allFieldMapKey = "AllFieldMap_" + obj.getClass().getSimpleName();
		if (reflectionCache.getIfPresent(allFieldMapKey)!=null) {
			return (HashMap<String, Field>) reflectionCache.get(allFieldMapKey);
		} else {
			Field[] fieldAr;
			HashMap<String, Field> fieldMap = new HashMap<String, Field>();
			Class cl = obj.getClass();
			// 取得所有Field
			while (cl != null) {
				fieldAr = cl.getDeclaredFields();
				for (Field f : fieldAr) {
					fieldMap.put(f.getName().toLowerCase(), f);
				}
				cl = cl.getSuperclass(); // 得到父类,然后赋给自己
			}
			reflectionCache.put(allFieldMapKey, fieldMap);
			return fieldMap;
		}
	}
	
	public static List<Method> getFieldsWithAnnotation(Object obj, Class<? extends Annotation> annotationType) {
		return Arrays.asList(obj.getClass().getDeclaredMethods()).stream()
				.filter(method -> method.getAnnotationsByType(annotationType).length > 0).collect(Collectors.toList());
	}

	public static Object invoke(Object methodClazzObj, Method method, Object... args)
			throws InvocationTargetException, IllegalAccessException {
		Object[] argObjs = new Object[args.length];
		Class<?>[] methodParaClazzs = method.getParameterTypes();
		if (args.length != methodParaClazzs.length) {
			throw new IllegalArgumentException(
					"Method args count[" + methodParaClazzs.length + "] is not Match args count["
							+ args.length + "]");
		}
		for (int i = 0; i < argObjs.length; i++) {
			argObjs[i] = WType.objToSpecificType(args[i], methodParaClazzs[i]);
		}
		return method.invoke(methodClazzObj, argObjs);
	}
}
