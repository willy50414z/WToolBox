package com.willy.util.encrypt;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentPBEConfig;
import org.springframework.beans.factory.annotation.Value;

public class WJasypt {
	private static String jasyptKey;
	private static StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
	private static EnvironmentPBEConfig config = new EnvironmentPBEConfig();
	public static String encrypt(String plainText) throws Exception {
        config.setPassword(jasyptKey);                        // 加密的密钥
        standardPBEStringEncryptor.setConfig(config);
        return standardPBEStringEncryptor.encrypt(plainText);
    }
  public static String encrypt(String key, String plainText) throws Exception {
    config.setPassword(key);                        // 加密的密钥
    standardPBEStringEncryptor.setConfig(config);
    return standardPBEStringEncryptor.encrypt(plainText);
  }
 
    public static String decrypt(String encodeStr) throws Exception {
        config.setPassword(jasyptKey);
        standardPBEStringEncryptor.setConfig(config);
        return standardPBEStringEncryptor.decrypt(encodeStr);
    }
  public static String decrypt(String key, String encodeStr) throws Exception {
    config.setPassword(key);
    standardPBEStringEncryptor.setConfig(config);
    return standardPBEStringEncryptor.decrypt(encodeStr);
  }
    @Value("${jasypt.encryptor.password}")
	public static void setJasyptKey(String jasyptKey) {
		WJasypt.jasyptKey = jasyptKey;
	}
    public static void main(String[] args) throws Exception {
    	WJasypt.jasyptKey = "sias";
		System.out.println(encrypt("Ssh50213!"));
	}
}
