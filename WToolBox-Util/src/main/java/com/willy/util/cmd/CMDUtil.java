package com.willy.util.cmd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.willy.util.type.WType;
import org.springframework.stereotype.Component;

@Component
public class CMDUtil {
	private Process process;
	private Runtime runtime;
	private InputStreamReader isr;
	private BufferedReader br;

	private static String cmdPreffix;
	static {
		String osName = System.getProperty("os.name");
		if (osName.equals("Windows 10")) {
			cmdPreffix = "cmd.exe /C ";
		} else if (osName.equals("Windows 95")) {
			cmdPreffix = "cmd.exe /C ";
		} else {
			cmdPreffix = "";
		}
	}

	public CMDUtil() {
		runtime = Runtime.getRuntime();
	}

	public String exec(String cmd) throws Exception {
		process = runtime.exec(cmdPreffix + cmd);
		return getEcho(cmd, process);
	}

	public String getEcho(String cmd, Process process) throws IOException, InterruptedException {
		String lineStr;
		StringBuffer echo = new StringBuffer();
		try {
			isr = new InputStreamReader(process.getErrorStream(), "MS950");
			br = new BufferedReader(isr);
			while ((lineStr = br.readLine()) != null) {
				echo.append(lineStr).append("\r\n");
			}
			isr = new InputStreamReader(process.getInputStream(), "MS950");
			br = new BufferedReader(isr);
			while ((lineStr = br.readLine()) != null) {
				echo.append(lineStr).append("\r\n");
			}
		} catch (Exception e) {
			echo.append(WType.exceptionToString(e));
		} finally {
			if (br != null) {
				br.close();
				br = null;
			}
			if (isr != null) {
				isr.close();
				isr = null;
			}
		}
		return echo.toString();
	}
}
