package com.willy.util.log;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.LoadingCache;
import com.willy.util.cache.WCache;
import com.willy.util.type.WType;

public class WLog {
	private static LoadingCache<String, Logger> loggerCache = WCache.getDefaultCache();
	public static void trace(String msg) {
		getLogger().trace(msg);
	}
	
	public static void debug(String msg) {
		getLogger().debug(msg);
	}

	public static void info(String msg) {
		getLogger().info(msg);
	}

	public static void error(Exception e) {
		System.out.print(WType.exceptionToString(e));
		error("", e);
	}

	public static void error(Throwable e) {
		error("", e);
	}
	
	public static void error(String msg) {
		getLogger().error(msg);
	}

	public static void error(String msg, Exception e) {
		getLogger().error((msg==null?"":msg), e);
	}

	public static void error(String msg, Throwable e) {
		getLogger().error((msg==null?"":msg), e);
	}
	public static void warn(String msg) {
		getLogger().warn(msg);
	}
	private static Logger getLogger() {
		try {
			String className = new Exception().getStackTrace()[2].getClassName();
			Logger logger = loggerCache.getIfPresent(className);
			if(logger==null) {
				logger = LoggerFactory.getLogger(Class.forName(className));
				loggerCache.put(className, logger);
			}
			return logger; 
		}catch(Exception e) {
			return LoggerFactory.getLogger(WLog.class);
		}
	}
	public static void main(String[] args) {
		getLogger().trace("Trace message");
		getLogger().debug("Debug message");
		getLogger().info("Info message");
		getLogger().warn("Warn message");
		getLogger().error("Error message");
	}
	public static long performanceLog(String logkey, long startTime) {
		long nowTime = System.currentTimeMillis();
		getLogger().info("LogKey["+logkey+"] spend ["+(nowTime-startTime)+"]ms");
		return nowTime;
	}
}
