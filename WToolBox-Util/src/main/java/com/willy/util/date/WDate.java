package com.willy.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.willy.util.type.WType;


public class WDate {
	public final static String dateFormat_yyyyMMdd="yyyyMMdd";
	public final static String dateFormat_yyyyMMdd_Slash="yyyy/MM/dd";
	public final static String dateFormat_yyyyMMdd_Dash="yyyy-MM-dd";
	public final static String dateFormat_yyyyMMddHHmmss="yyyyMMddHHmmss";
	public final static String dateFormat_yyyMMdd="yyyMMdd";
	public final static String dateFormat_yyyMMdd_Slash="yyy/MM/dd";
	public final static String dateFormat_DateTime="yyyyMMdd HH:mm:ss";
	public final static String dateFormat_Time="HH:mm:ss";
	public final static String dateFormat_DateTime_Dash="yyyy-MM-dd HH:mm:ss";
	public final static String dateFormat_DateTime_Slash="yyyy/MM/dd HH:mm:ss";
	public final static String dateFormat_MMddHHmmss="MMddHHmmss";
	public final static String dateFormat_Timestamp="yyyy-MM-dd hh:mm:ss.S";
	private static Calendar ca;
	public static String getNowDate() {
		 return new SimpleDateFormat(dateFormat_yyyyMMdd).format(new Date());
	}
	public static String getNowTime(String format) {
		 return new SimpleDateFormat(format).format(new Date());
	}
	
	public static String addDate(String format,Date date,int millisecond) {
		return new SimpleDateFormat(format).format(date.getTime() + millisecond);
	}
	public static String addDate(String format,String strDate,int millisecond) {
		return new SimpleDateFormat(format).format(WType.strToDate(strDate,format).getTime() + millisecond);
	}
	public static Date addDate(Date date, int calendar,int count) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(calendar, count);
		return cal.getTime();
	}
	
	public static Integer addMonth(int month, int count) {
		month = month + count;
		return (month>12)? month-12 : (month<1) ? month + 12 : month;
	}
	public static Date addDay(Date date,int days) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, days);
		return calendar.getTime();
	}
	public static String addDay(String format,Date date,int days) {
		return new SimpleDateFormat(format).format(addDay(date, days));
	}
	
	public static int getDateInfo(String date, int calendar) {
		ca =Calendar.getInstance();
		ca.setTime(WType.strToDate(date));
		return ca.get(calendar)+((calendar==Calendar.MONTH)?1:0);
	}
	public static int getDateInfo(Date date, int calendar) {
		ca =Calendar.getInstance();
		ca.setTime(date);
		return ca.get(calendar)+((calendar==Calendar.MONTH)?1:0);
	}
	
	public static int getPartOfMonth(Date date) throws ParseException {
		int day = getDateInfo(date, Calendar.DAY_OF_MONTH);
		if(day<11) {
			return 1;
		}else if(day>10 && day<21) {
			return 2;
		}else {
			return 3;
		}
	}
	
	public static String reFormat(String date,String newFormat) throws ParseException {
		return new SimpleDateFormat(newFormat).format(WType.strToDate(date));
	}
	public static String reFormat(String date,String oldFormat,String newFormat) throws ParseException {
		return new SimpleDateFormat(newFormat).format(WType.strToDate(date, oldFormat));
	}
	
	public static String convertCEYear(String year) {
		int yearInt = Integer.parseInt(year.trim());
		return String.valueOf((yearInt>1911) ? (yearInt-1911) : (yearInt+1911));
	}
	public static int convertCEYear(int year) {
		return (year>1911) ? (year-1911) : (year+1911);
	}
	
	public static String convertCE(String date) {
		String year = date.substring(0, date.length() - 4);
		return convertCEYear(year)+date.substring(date.length() - 4);
	}

	public static boolean beforeOrEquals(Date date1, Date date2) {
		if(date1 == null || date2 == null) {
			return false;
		}
		return (date1.before(date2) || date1.compareTo(date2) == 0);
	}
	public static boolean afterOrEquals(Date date1, Date date2) {
		return (date1.after(date2) || date1.compareTo(date2) == 0);
	}

	public static boolean between(Date date, Date startDate, Date endDate) {
		return beforeOrEquals(date, endDate) && afterOrEquals(date, startDate);
	}
	
	public static int getQuarter(Date date) {
		int month = getDateInfo(date, Calendar.MONTH);
		if(month>0 && month<4) {
			return 1;
		}else if(month>3 && month<7) {
			return 2;
		}else if(month>6 && month<10) {
			return 3;
		}else if(month>9 && month<13) {
			return 4;
		}
		return 0;
	}
}
