package com.willy.util.constant;


public class WConst {
	public static String getExeFilePath(Class<?> targetClass) {
		return targetClass.getProtectionDomain().getCodeSource().getLocation().toString().replace("file:/", "");
	}
	public static String getExeFileDic(Class<?> targetClass) {
		String exeFilePath=getExeFilePath(targetClass);
		return getExeFilePath(targetClass).substring(0, Math.max(exeFilePath.lastIndexOf("/"),exeFilePath.lastIndexOf("\\")) + 1);
	}
}
