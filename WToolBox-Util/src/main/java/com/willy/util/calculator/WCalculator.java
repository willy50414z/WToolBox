package com.willy.util.calculator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.jexl3.JexlBuilder;
import org.apache.commons.jexl3.MapContext;

public class WCalculator {

  private static JexlBuilder engine;

  public static BigDecimal eval(String formula) {
    if (engine == null) {
      engine = new JexlBuilder();
    }

    Object result = engine.create().createExpression(formula).evaluate(new MapContext());
    if (result instanceof Boolean) {
      return (Boolean) result ? new BigDecimal(1) : new BigDecimal(-1);
    }
    return new BigDecimal(result.toString());
  }

  public static <T> List<T> collectionAnd(Collection<T> list1, Collection<T> list2) {
    return list1.stream().filter(list2::contains).collect(Collectors.toList());
  }

  public static <T> List<T> collectionOr(Collection<T> list1, Collection<T> list2) {
    Set<T> tmpSet = new HashSet<>(list1);
    tmpSet.addAll(list2);
    List<T> result = new ArrayList<>();
    result.addAll(tmpSet);
    return result;
  }
}
