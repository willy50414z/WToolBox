package com.willy.util.encryptor;

import com.willy.util.log.WLog;
import com.willy.util.string.WString;
import java.nio.charset.StandardCharsets;
import java.security.spec.KeySpec;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class WEncryptor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String rawStr = "aaaaabbbccdd";
		String encodeStr = WEncryptor.encode_DES("key1","123456789123",rawStr);
		System.out.println(encodeStr);
		System.out.println(WEncryptor.decode_DES("123456789123","123456789123",encodeStr));
	}

	/** BASE64 **/
	public static String encode_Base64(String str) {
		return Base64.getEncoder().encodeToString(str.getBytes());
	}

	public static String decode_Base64(String pwd) {
		return new String(Base64.getDecoder().decode(pwd.getBytes()));
	}
	public static String encode_Base64(byte[] str) {
		return Base64.getEncoder().encodeToString(str);
	}

	public static String decode_Base64(byte[] pwd) {
		return new String(Base64.getDecoder().decode(pwd));
	}
	public static byte[] encode_byte_Base64(String str) {
		return Base64.getEncoder().encode(str.getBytes());
	}

	public static byte[] decode_byte_Base64(String pwd) {
		return Base64.getDecoder().decode(pwd.getBytes());
	}

	/** AES **/
	public static String encode_AES(String key, String text)
	{
		String encryptedString = "";
		try
		{
			byte encrypted[] = getAESCipher(1, key).doFinal(text.getBytes());
			encryptedString = WEncryptor.encode_Base64(encrypted);
		}
		catch (Exception e)
		{
			WLog.error(e);
		}
		return encryptedString;
	}

	public static String decode_AES(String key, String encryptedText)
	{
		String decryptedString = "";
		try
		{
			byte decodedValue[] = WEncryptor.decode_byte_Base64(encryptedText);
			decryptedString = new String(getAESCipher(2, key).doFinal(decodedValue));
		}
		catch (Exception e)
		{
			WLog.error(e);
		}
		return decryptedString;
	}

	private static Cipher getAESCipher(int mode, String key)
		throws Exception
	{
		java.security.Key aesKey = new SecretKeySpec(getKeyForAES(key).getBytes(), "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(mode, aesKey);
		return cipher;
	}
	private static String getKeyForAES(String key)
	{
		String keyForAES = "";
		if (key.length() > 16)
			keyForAES = key.substring(0, 16);
		else
			for (keyForAES = key; keyForAES.length() != 16; keyForAES = (new StringBuilder(String.valueOf(keyForAES))).append(" ").toString());
		return keyForAES.substring(0, 16);
	}
	
	/** DES **/
	
	/**
	 * DES加密
	 * @param key
	 * @param str
	 * @return
	 */
	public static String encode_DES(String key,String str) {
		return desProcess(key,str, false);
	}

	/**
	 *
	 * @param key
	 * @param str
	 * @return
	 */
	public static String decode_DES(String key,String str) {
		return desProcess(key,str, true);
	}

	/**
	 *
	 * @param key1
	 * @param key2
	 * @param str
	 * @return
	 */
	public static String encode_DES(String key1,String key2,String str) {
		return desProcess(key1,key2,str, false);
	}

	/**
	 *
	 * @param key1
	 * @param key2
	 * @param str
	 * @return
	 */
	public static String decode_DES(String key1,String key2,String str) {
		return desProcess(key1,key2,str, true);
	}
	
	/**
	 * DES加解密
	 * @param password - 密碼(編碼 : 明碼 / 解碼 : Base64 暗碼)
	 * @param isDecode - 是否為解碼
	 * @return 處理後字串(編碼 : Base64 暗碼 / 解碼 : 明碼)
	 */
	private static String desProcess(String encyKey, String password, boolean isDecode) {
		/** Private Key */
		byte[] PRIVATE_KEY = ((encyKey.length()>=9)?encyKey:WString.fillStrLength(encyKey, "0", 9)).getBytes();
		
		/** Initialization vector (IV) */
		byte[] IV = "Initiali".getBytes();
		
		/** encode format */
		String ALGORITHM = "DES/CBC/PKCS5Padding";
		
		String str = null;
		byte[] pwd = null;
		try {
			//處理來源密碼
			if (!isDecode)
				pwd = password.getBytes();
			else
				pwd = Base64.getDecoder().decode(password.getBytes());
			
			//建立Key
			KeySpec keySpec = new DESKeySpec(PRIVATE_KEY);
			IvParameterSpec iv = new IvParameterSpec(IV);
			
			//產生Key
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey key = keyFactory.generateSecret(keySpec);
			
			//處理DES
			Cipher cf = Cipher.getInstance(ALGORITHM);
			if (!isDecode)
				cf.init(Cipher.ENCRYPT_MODE, key, iv);
			else
				cf.init(Cipher.DECRYPT_MODE, key, iv);
	        
			//產生結果
	        byte[] theCph = cf.doFinal(pwd);
	        if (!isDecode)
	        	str = new String(Base64.getEncoder().encode(theCph));
	        else
	        	str = new String(theCph, StandardCharsets.UTF_8);
	        
		} catch (Exception e) {
			WLog.error(e);
		}
		return str;
	}
	/**
	 * DES加解密
	 * @param password - 密碼(編碼 : 明碼 / 解碼 : Base64 暗碼)
	 * @param isDecode - 是否為解碼
	 * @return 處理後字串(編碼 : Base64 暗碼 / 解碼 : 明碼)
	 */
	private static String desProcess(String encyKey1,String encyKey2, String password, boolean isDecode) {
		/** Private Key */
		byte[] PRIVATE_KEY = ((encyKey1.length()>=9)?encyKey1:WString.fillStrLength(encyKey1, "0", 9)).getBytes();
		
		/** Initialization vector (IV) */
		byte[] IV = ((encyKey2.length()<8)?WString.fillStrLength(encyKey2, "0", 8) : encyKey2.substring(0, 8)).getBytes();
		
		/** encode format */
		String ALGORITHM = "DES/CBC/PKCS5Padding";
		
		String str = null;
		byte[] pwd = null;
		try {
			//處理來源密碼
			if (!isDecode)
				pwd = password.getBytes();
			else
				pwd = Base64.getDecoder().decode(password.getBytes());
			
			//建立Key
			KeySpec keySpec = new DESKeySpec(PRIVATE_KEY);
			IvParameterSpec iv = new IvParameterSpec(IV);
			
			//產生Key
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey key = keyFactory.generateSecret(keySpec);
			
			//處理DES
			Cipher cf = Cipher.getInstance(ALGORITHM);
			if (!isDecode)
				cf.init(Cipher.ENCRYPT_MODE, key, iv);
			else
				cf.init(Cipher.DECRYPT_MODE, key, iv);
	        
			//產生結果
	        byte[] theCph = cf.doFinal(pwd);
	        if (!isDecode)
	        	str = new String(Base64.getEncoder().encode(theCph));
	        else
	        	str = new String(theCph, StandardCharsets.UTF_8);
	        
		} catch (Exception e) {
			WLog.error(e);
		}
		return str;
	}
}
