package com.willy.util.type;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;

import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.reflect.WReflect;

public class WType {
  // 轉換
  //DB

  /**
   * ResultSet 轉 陣列
   *
   * @param rs
   * @return
   * @throws SQLException
   */
  public static String[][] rsToArray(ResultSet rs) throws SQLException {
    String[][] result = null;
    rs.last();
    int rows = rs.getRow();
    int cols = rs.getMetaData().getColumnCount();
    rs.beforeFirst();
    result = new String[rows][cols];
    int row = 0;
    while (rs.next()) {
      for (int i = 1; i <= cols; i++) {
        result[row][i - 1] = rs.getString(i);
      }
      row++;
    }
    return result;
  }

  public static String[][] strListToArray2D(List<String> listStr) {
    return strListToArray2D(listStr, ",");
  }

  public static String[][] strListToArray2D(List<String> listStr, String separator) {
    if (listStr == null || listStr.size() == 0) {
      return new String[][]{{"", ""}};
    }
    int maxColCount = 0;
    for (String str : listStr) {
      maxColCount = Math.max(str.split(separator).length, maxColCount);
    }
    String[][] resultAr = new String[listStr.size()][maxColCount];
    for (int rowIndex = 0; rowIndex < listStr.size(); rowIndex++) {
      resultAr[rowIndex] = listStr.get(rowIndex).split(separator);
    }
    return resultAr;
  }


  public static String[][] strArListToArray2D(List<String[]> arList) {
    if (arList == null || arList.size() == 0) {
      return new String[0][0];
    }
    int maxColCount = 0;
    for (String[] ar : arList) {
      maxColCount = Math.max(ar.length, maxColCount);
    }
    String[][] resultAr = new String[arList.size()][maxColCount];
    for (int rowIndex = 0; rowIndex < arList.size(); rowIndex++) {
      resultAr[rowIndex] = arList.get(rowIndex);
    }
    return resultAr;
  }

  public static <T> T rsToBean(ResultSet rs, Class<T> beanClass) throws Exception {
    // 取得rs相關資料
    int columnCount;
    ResultSetMetaData rsmd = rs.getMetaData();
    columnCount = rsmd.getColumnCount();
    // 塞進List
    T bean;
    bean = beanClass.newInstance();
    for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
      WReflect.setFieldValueByLowerCaseFieldName(bean, rsmd.getColumnName(columnIndex),
          rs.getString(columnIndex));
    }
    return bean;
  }

  public static <T> List<T> rsToBeanList(ResultSet rs, Class<T> beanClass) throws Exception {
    // 取得rs相關資料
    int columnCount;
    ResultSetMetaData rsmd = rs.getMetaData();
    columnCount = rsmd.getColumnCount();
    // 塞進List
    T bean;
    List<T> beanList = new ArrayList<T>();
    do {
      bean = beanClass.newInstance();
      for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
        WReflect.setFieldValue(bean, rsmd.getColumnName(columnIndex), rs.getString(columnIndex));
      }
      beanList.add(bean);
    } while (rs.next());
    return beanList;
  }

  public static <T, S> List<S> castList(List<T> list, WCaster<T, S> caster) {
    List<S> result = new ArrayList<>();
    for (T obj : list) {
      result.add(caster.cast(obj));
    }
    return result;
  }

  /**
   * 轉換為特定類型 type為轉 轉換後類型類別
   *
   * @param obj
   * @param type
   * @return
   * @throws ParseException
   */
  @SuppressWarnings("unchecked")
  public static <T> T objToSpecificType(Object obj, Type type) {
    if (obj == null) {
      return null;
    }
    switch (type.getTypeName()) {
      case "java.lang.String":
        return (T) obj.toString();
      case "java.lang.Integer":
      case "int":
        if (obj.toString().equals("")) {
          obj = "0";
        } else if (obj.toString().indexOf(".") > -1) {
          obj = obj.toString().substring(0, obj.toString().indexOf("."));// 排除小數點
        }
        return (T) Integer.valueOf(obj.toString().replace(",", ""));
      case "java.lang.Double":
        if (obj.toString().equals("")) {
          obj = "0";
        }
        return (T) Double.valueOf(obj.toString().replace(",", ""));
      case "java.lang.Boolean":
      case "boolean":
        return (T) Boolean.valueOf(obj.toString());
      case "java.lang.Float":
        return (T) Float.valueOf(obj.toString().replace(",", ""));
      case "java.lang.Long":
        return (T) Long.valueOf(obj.toString().replace(",", ""));
      case "java.util.Date":
        return (T) WType.strToDate(obj.toString());
      case "java.math.BigDecimal":
        return (T) new BigDecimal(obj.toString());
      default:
        return (T) (obj);
    }
  }

  /**
   * 日期文字轉換
   */
  public static String dateToStr(Date date) {
    return dateToStr(date, WDate.dateFormat_yyyyMMdd);
  }

  public static String dateToStr(Date date, String format) {
    return new SimpleDateFormat(format).format(date);
  }

  public static Date strToDate(String strDate) {
    String dateFormat = "";
    if (strDate.matches("[0-9]{8}")) {
      dateFormat = WDate.dateFormat_yyyyMMdd;
    } else if (strDate.matches("[0-9]{4}[-][0-9]{2}[-][0-9]{2}")) {
      dateFormat = WDate.dateFormat_yyyyMMdd_Dash;
    } else if (strDate.matches("[0-9]{4}[/][0-9]{2}[/][0-9]{2}")) {
      dateFormat = WDate.dateFormat_yyyyMMdd_Slash;
    } else if (strDate.matches("[0-9]{4}[-][0-9]{2}[-][0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}")) {
      dateFormat = WDate.dateFormat_DateTime_Dash;
    } else if (strDate.matches(
        "[0-9]{4}[-][0-9]{2}[-][0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]*")) {
      dateFormat = WDate.dateFormat_Timestamp;
    } else if (strDate.matches("[0-9]{8} [0-9]{2}:[0-9]{2}:[0-9]{2}")) {
      dateFormat = "yyyyMMdd hh:mm:ss";
    } else {
      WLog.error("未指定日期格式");
      return null;
    }
    return strToDate(strDate, dateFormat);
  }

  public static Date strToTimestamp(String strDate) {
    return new Timestamp(strToDate(strDate).getTime());
  }

  public static Date strToDate(String strDate, String format) {
    try {
      return new SimpleDateFormat(format).parse(strDate);
    } catch (ParseException e) {
      WLog.error(e);
      return null;
    }
  }

  // 進位置轉換 10-2 5-101 6-110 35 10010
  public static int transToTenCarry(int fromCarry, String rawInt) {
    // 轉換10進位
    int tenCarryInt = 0;
    char[] digitArray = rawInt.toCharArray();
    int digitArrayLength = digitArray.length;
    for (int i = 1; i <= digitArrayLength; i++) {
      tenCarryInt += Math.pow(fromCarry, (digitArrayLength - i))
          * Integer.valueOf(String.valueOf(digitArray[i - 1]));
    }
    // 轉換為任意進位
    return tenCarryInt;
  }

  // 進位制轉換
  public static String transToOtherCarry(int fromCarry, int toCarry, String rawInt) {
    int tenCarryInt;
    if (fromCarry == 10) {
      tenCarryInt = Integer.valueOf(rawInt);
    } else {
      tenCarryInt = transToTenCarry(fromCarry, rawInt);
      if (toCarry == 10) {
        return String.valueOf(tenCarryInt);
      }
    }
    // int[] toCarryArray = null;
    StringBuffer digitBuffer = new StringBuffer();
    // 取得最大指數
    int maxIndex = 1;// 指數
    while (Math.pow(toCarry, maxIndex) < tenCarryInt) {
      maxIndex++;
    }
    int digit = 0;
    for (int index = (maxIndex - 1); index >= 0; index--) {
      digit = (int) (tenCarryInt / Math.pow(toCarry, index));
      // toCarryArray[index] = digit;
      digitBuffer.append(digit);
      tenCarryInt = (int) (tenCarryInt % Math.pow(toCarry, index));// 保留餘數進入下一輪
    }
    return digitBuffer.toString();
  }

  public static String exceptionToString(Exception e) {
    StringBuffer errorMsg = new StringBuffer();
    StackTraceElement[] trace = e.getStackTrace();
    errorMsg.append("Error Msg -- ").append(e.getMessage()).append("\r\n");
    for (int i = 0; i < trace.length; i++) {
      errorMsg.append(trace[i]).append("\r\n");
    }
    return errorMsg.toString();
  }

  public static String throwableToString(Throwable e) {
    StringBuffer errorMsg = new StringBuffer();
    StackTraceElement[] trace = e.getStackTrace();
    errorMsg.append("Error Msg -- ").append(e.getMessage()).append("\r\n");
    for (int i = 0; i < trace.length; i++) {
      errorMsg.append(trace[i]).append("\r\n");
    }
    return errorMsg.toString();
  }

  public static List<String[]> sqlRSToStrArList(SqlRowSet rs) {
    // 取得rs相關資料
    int columnCount;
    SqlRowSetMetaData rsmd = rs.getMetaData();
    columnCount = rsmd.getColumnCount();
    // 塞進List
    String[] rowAr;
    List<String[]> arList = new ArrayList<String[]>();
    while (rs.next()) {
      rowAr = new String[columnCount];
      for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
        rowAr[columnIndex] = rs.getString(columnIndex + 1);
      }
      arList.add(rowAr);
    }
    ;
    return arList;
  }

  public static String[][] sqlRSToAr(SqlRowSet rs) {
    return com.willy.util.type.WType.strArListToArray2D(sqlRSToStrArList(rs));
  }

  public static boolean isTheSameType(Object obj1, Object obj2) {
    return (obj1.getClass() == obj2.getClass());
  }

  public static Object json2Bean(String json, Class<?> clazz) throws Exception {
    return json2Bean(new JSONObject(json), clazz);
  }

  public static Object json2Bean(JSONObject json, Class<?> clazz) throws Exception {
    Object t = clazz.newInstance();
    //Method[] methods = clazz.getDeclaredMethods(); //取用現在class內定義好了的Method
    Method[] methods = clazz.getMethods(); //取用現在class內定義好了的Method包含superClass

    if (methods == null || methods.length == 0) {
      return null;
    }

    for (Method method : methods) {
      String methodName = method.getName();
      if (methodName.startsWith("set")) {
        StringBuffer fieldName = new StringBuffer();
        //logger.info("methodName="+methodName);
        //取得參數型態
        Class<?> paramClazz = method.getParameterTypes()[0];
        // 通過set的Method取得名稱
        //fieldName.append(methodName.substring(3, 4).toLowerCase())
        //        .append(methodName.substring(4));
        if (Character.isUpperCase(methodName.charAt(4))) {
          fieldName.append(methodName.substring(3));
        } else {
          fieldName.append(methodName.substring(3, 4).toLowerCase())
              .append(methodName.substring(4));
        }
        //若JSONObject找不到key則continue
        //logger.info("json : "+json);
        if (!json.has(fieldName.toString())) {
          WLog.debug("Can't find key[" + fieldName + "] in input JSONObject");
          continue;
        }
        WLog.debug(
            "Class[" + t.getClass().getName() + "], FieldName[" + fieldName.toString() + "] Value["
                + json.get(fieldName.toString()) + "]");

        if (paramClazz == boolean.class || paramClazz == Boolean.class) {
          method.invoke(t, new Object[]{json.optBoolean(fieldName.toString(), false)});
        } else if (paramClazz == int.class || paramClazz == Integer.class) {
          method.invoke(t, new Object[]{json.optInt(fieldName.toString(), 0)});
        } else if (paramClazz == long.class || paramClazz == Long.class) {
          method.invoke(t, new Object[]{json.optLong(fieldName.toString(), 0)});
        } else if (paramClazz == double.class || paramClazz == Double.class) {
          method.invoke(t, new Object[]{json.optDouble(fieldName.toString(), 0)});
        } else if (paramClazz == String.class) {
          method.invoke(t, new Object[]{json.optString(fieldName.toString(), "")});
        } else if (paramClazz == Date.class) {
          method.invoke(t, new Object[]{json.optString(fieldName.toString(), "")});
        } else if (paramClazz == BigDecimal.class) {
          method.invoke(t, new Object[]{new BigDecimal(json.getString(fieldName.toString()))});
        } else if (List.class.isAssignableFrom(paramClazz)) {
          JSONArray array = json.getJSONArray(fieldName.toString());
          ArrayList<Object> list = new ArrayList<>();
          for (int i = 0; array != null && i < array.length(); i++) {
            JSONObject jsonobj = array.getJSONObject(i);
            Class<?> listType = (Class<?>) ((ParameterizedType) clazz   // 取得List的泛型類型
                .getDeclaredField(fieldName.toString()).getGenericType())
                .getActualTypeArguments()[0];
            Object obj = json2Bean(jsonobj, listType);
            list.add(obj);
          }
          method.invoke(t, list);
        } else {
          try {
            JSONObject getjsonObj = json.getJSONObject(fieldName.toString());
            Object obj = json2Bean(getjsonObj, paramClazz);
            method.invoke(t, obj);
          } catch (JSONException e) {
            WLog.error(e);
            continue;
          }
        }
      }
    }
    return t;
  }
}
