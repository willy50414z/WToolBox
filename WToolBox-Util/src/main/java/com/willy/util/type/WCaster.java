package com.willy.util.type;

public interface WCaster <T,S> {
	public S cast(T obj);
}
