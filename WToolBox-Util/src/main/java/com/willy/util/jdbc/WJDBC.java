package com.willy.util.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.persistence.Table;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.willy.util.cache.WCache;
import com.willy.util.log.WLog;
import com.willy.util.reflect.WReflect;
import com.willy.util.string.WString;
import com.willy.util.type.WType;

@Service
public class WJDBC {
	private String sql;
	@Autowired
	public JdbcTemplate jdbcTemplate;
	@Autowired
	private DataSource ds;

	// 撈出Key,Value兩個欄位組成map
	public HashMap<String, String> queryForMap(String sql) {
		HashMap<String, String> resultMap = new HashMap<String, String>();
		SqlRowSet srs = jdbcTemplate.queryForRowSet(sql);
		while (srs.next()) {
			resultMap.put(srs.getString(1), srs.getString(2));
		}
		return resultMap;
	}

	// 取得字串陣列
	public String[][] queryForAr(String sql, Object... args) {
		SqlRowSet sqlRS = null;
		try{
			sqlRS = jdbcTemplate.queryForRowSet(sql, args);
		}catch(Exception e) {
			WLog.error(e);
		}
		return WType.sqlRSToAr(sqlRS);
	}

	// 取得字串List
	public List<String[]> queryForArList(String sql, Object... args) {
		String[] rowAr;
		List<String[]> resultArList = new ArrayList<String[]>();
		SqlRowSet srs = jdbcTemplate.queryForRowSet(sql, args);
		SqlRowSetMetaData metaData = srs.getMetaData();
		while (srs.next()) {
			rowAr = new String[metaData.getColumnCount()];
			for (int colIndex = 1; colIndex <= rowAr.length; colIndex++) {
				rowAr[colIndex - 1] = srs.getString(colIndex);
			}
			resultArList.add(rowAr);
		}
		return resultArList;
	}
	
	public String[][] queryForAr(String sql, List<Object> argList) {
		Object[] argAr = new Object[argList.size()]; 
		argList.toArray(argAr);
		SqlRowSet sqlRS = jdbcTemplate.queryForRowSet(sql, argAr);
		return WType.sqlRSToAr(sqlRS);
	}

	// 判斷是否有資料
	public boolean isExist(String sql, Object... args) {
		SqlRowSet rowSet = jdbcTemplate.queryForRowSet(sql, args);
		rowSet.last();
		return rowSet.getRow() > 0;
	}

	public <T> T queryForBean(String sql, Class<T> beanClass, Object... args) {
		List<T> list = queryForBeanList(sql, beanClass, args);
		return list == null ? null : list.get(0);
	}

	public <T> List<T> queryForBeanList(String sql, Class<T> beanClass, Object... args) {
		return jdbcTemplate.query(sql, new RowMapper() {
			T resultBean = null;
			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				// TODO Auto-generated method stub
				try {
						resultBean = (T) WType.rsToBean(rs, beanClass);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					WLog.error(e);
				}
				return resultBean;
			}
		}, args);
	}

	public <T> Set<T> queryForBeanSet(String sql, Class<T> beanClass, Object... args) {
		List<T> beanList = jdbcTemplate.query(sql, new RowMapper() {
			T resultBean = null;
			@Override
			public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
				// TODO Auto-generated method stub
				try {
					resultBean = (T) WType.rsToBean(rs, beanClass);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					WLog.error(e);
				}
				return resultBean;
			}
		}, args);
		return new HashSet<T>(beanList);
	}
	public <T> List<T> queryForBeanList(String sql, RowMapper<T> rm, Object... args) {
		return jdbcTemplate.query(sql,rm, args);
	}
	public <T> Set<T> queryForBeanSet(String sql, RowMapper<T> rm, Object... args) {
		return new LinkedHashSet<T>(queryForBeanList(sql,rm, args));
	}
	

	// 用客製化SQL塞BeanList到DB
	public <T> void batchUpdate(String sql, final List<T> beanList) {
		// 取得table名稱
		if (beanList.size() < 1) {
			return;
		}
		String tableName = beanList.get(0).getClass().getSimpleName();
		tableName = tableName.substring(0, tableName.length() - 4);
		// 取得Insert語法
		this.sql = sql;
		// Insert
		jdbcTemplate.batchUpdate(this.sql, new BatchPreparedStatementSetter() {
			private T bean;
			private String[] paramAr;
			private String[] colsNameAr;
			private String sql = "";
			private String scriptLog;

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				// TODO Auto-generated method stub
				try {
					bean = beanList.get(i);
					if (this.sql.isEmpty()) {
						this.sql = WReflect.getFieldValue(ps, "sql").toString();
						this.colsNameAr = getColNameAr();
					}
					this.scriptLog = this.sql;
					for (int colIndex = 1; colIndex < colsNameAr.length + 1; colIndex++) {
						ps.setObject(colIndex, WReflect.getFieldValue(bean, colsNameAr[colIndex - 1]));
						scriptLog = scriptLog.replaceFirst("[?]",
								"'" + WReflect.getFieldValue(bean, colsNameAr[colIndex - 1]) + "'");
					}
					WLog.trace(scriptLog);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					WLog.error("importBeanList 發生錯誤", e);
				}
			}

			@Override
			public int getBatchSize() {
				// TODO Auto-generated method stub
				return beanList.size();
			}

			private String[] getColNameAr() {
				String paramStr = this.sql.split("values")[1];
				// 排除sql中含有的Func參數
				this.paramAr = paramStr.substring(paramStr.indexOf("(") + 1, paramStr.lastIndexOf(")"))
						.replaceAll("[(][^)]*[)]", "").split(",");
				this.colsNameAr = WString.rmStr(sql.substring(sql.indexOf("(") + 1, sql.indexOf(")")), "[", "]")
						.split(",");
				StringBuffer paramSb = new StringBuffer();
				for (int i = 0; i < paramAr.length; i++) {
					if (paramAr[i].equals("?")) {
						paramSb.append(colsNameAr[i]).append(",");
					}

				}
				return paramSb.substring(0, paramSb.length() - 1).split(",");
			}
		});
	}
	
	public <T> void batchInsert(final Set<T> beanSet) throws Exception {
		List<T> beanList = new ArrayList<> ();
		beanSet.forEach(bean -> beanList.add(bean));
		batchInsert(beanList);
	}
	
	public <T> void batchInsert(List<T> beanList) throws Exception {
		if (beanList == null || beanList.size() == 0) {
			return;
		}
		Connection con = null;
		PreparedStatement ps = null;
		try {
			Object param;
			String scriptLog = "";
			
			// 取得table名稱
			String tableName = beanList.get(0).getClass().getAnnotation(Table.class).name();
			// 取得Insert語法
			this.sql = getInsertSqlTemplate(tableName).replaceAll("[$][{][^{]+[}]", "?");
			
			//取得欄位名稱
			String[] colsNameAr = WString.rmStr(sql.substring(sql.indexOf("(") + 1, sql.indexOf(")")), "[", "]").split(",");
			
			//取得連線
			int batchSize = 0;
			con = ds.getConnection();
			con.setAutoCommit(false);
			ps = con.prepareStatement(this.sql);
			for(T bean : beanList) {
				scriptLog = this.sql;
				for (int colIndex = 1; colIndex < colsNameAr.length + 1; colIndex++) {
					param = WReflect.getFieldValue(bean, colsNameAr[colIndex - 1]);
					ps.setObject(colIndex, param);
					scriptLog = scriptLog.replaceFirst("[?]",(param==null)?"NULL":"'" + param.toString() + "'");
				}
				WLog.debug(scriptLog);
				ps.addBatch();
				batchSize++;
				//1000筆執行一次
				if(batchSize==1000) {
					ps.executeBatch();
					batchSize = 0;
				}
			}
			ps.executeBatch();
			con.commit();
		}catch(Exception e) {
			con.rollback();
			throw e;
		}finally {
			if (con!=null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (ps!=null) {
				try {
					ps.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private String getInsertSqlTemplate(String tableName) throws ExecutionException {
		StringBuffer sqlTemplate;
		if (WCache.cache.getIfPresent("SqlTempl_" + tableName)!=null) {
			return WCache.cache.get("SqlTempl_" + tableName).toString();
		} else {
			sqlTemplate = new StringBuffer("Insert into ");
			sqlTemplate.append(tableName).append("(");
			List<String> columnNameList = jdbcTemplate.queryForList(
					"select COLUMN_NAME " + "from INFORMATION_SCHEMA.COLUMNS " + "where TABLE_NAME='" + tableName
							+ "'	and columnproperty(object_id(table_name), column_name,'IsIdentity ') <> 1 "
							+ "	and COLUMN_NAME not in (SELECT sc.name FROM sys.columns sc WHERE sc.is_computed = 1)" 
							+ "order by ORDINAL_POSITION",
					String.class);
			if (CollectionUtils.isEmpty(columnNameList)) {
				// 取synonyms相關table name
				List<String> relativeDBNameList = jdbcTemplate.queryForList(
						"SELECT COALESCE (PARSENAME (base_object_name, 3), DB_NAME (DB_ID ())) FROM sys.synonyms",
						String.class);
				for (String relativeDBName : relativeDBNameList) {
					columnNameList = jdbcTemplate.queryForList("select COLUMN_NAME " + "from [" + relativeDBName
							+ "].INFORMATION_SCHEMA.COLUMNS " + "where TABLE_NAME='" + tableName
							+ "' and COLUMN_NAME not in (select c.name from [" + relativeDBName
							+ "].sys.columns c join [" + relativeDBName
							+ "].sys.objects o on c.object_id = o.object_id join [" + relativeDBName
							+ "].sys.schemas s on s.schema_id = o.schema_id where s.name = 'dbo' "
							+ "  and o.is_ms_shipped = 0 and o.type = 'U' and c.is_identity = 1 "
							+ ") order by ORDINAL_POSITION", String.class);
					if (!CollectionUtils.isEmpty(columnNameList)) {
						break;
					}
				}
			}
			for (String columnName : columnNameList) {
				sqlTemplate.append("[").append(columnName).append("]").append(",");
			}
			sqlTemplate = new StringBuffer(sqlTemplate.substring(0, sqlTemplate.length() - 1));// 去除多餘逗號
			sqlTemplate.append(") values(");
			for (String columnName : columnNameList) {
				sqlTemplate.append("${").append(columnName).append("}").append(",");
			}
			sqlTemplate = new StringBuffer(sqlTemplate.substring(0, sqlTemplate.length() - 1));// 去除多餘逗號
			sqlTemplate.append(")");
			WCache.cache.put("SqlTempl_" + tableName, sqlTemplate.toString());
		}
		return sqlTemplate.toString();
	}
	public boolean isTableExist(String tableName) {
		SqlRowSet rs = jdbcTemplate.queryForRowSet("select count(1) from INFORMATION_SCHEMA.TABLES t where t.TABLE_NAME=?", tableName);
		return Integer.valueOf(WType.sqlRSToAr(rs)[0][0])>0;
	}

	public JdbcTemplate getJdbcTemplate()
	{
		return jdbcTemplate;
	}
}
