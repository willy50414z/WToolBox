package com.willy.util.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Properties;

import com.willy.util.constant.WConst;
import com.willy.util.encryptor.WEncryptor;

public class WLocalConfig{
	private Properties properties;
	
	public WLocalConfig(String configFileName) throws FileNotFoundException, IOException {
		super();
		this.properties=new Properties();
		this.properties.load(new FileInputStream(WConst.getExeFilePath(WLocalConfig.class)));
	}
	public String get(String key) {
		return properties.getProperty(key);
	}
	public String get_Base64(String key) {
		return WEncryptor.decode_Base64(properties.getProperty(key));
	}
	public String get(String key, String... param) {
		return MessageFormat.format(properties.get(key).toString(), param);
	}
}
