package com.willy.util.config;

import java.io.FileNotFoundException;
import java.text.MessageFormat;
import java.util.Properties;

import com.willy.util.encryptor.WEncryptor;

public class WConfig{
	private static Properties properties;
	static {
		try {
			properties = new Properties();
			if(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties") == null) {
				throw new FileNotFoundException("未設定config.properties");
			}else {
				properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties"));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String get(String key) {
		return properties.getProperty(key);
	}
	public static String get_Base64(String key) {
		return WEncryptor.decode_Base64(properties.getProperty(key));
	}
	public static String get(String key, String... param) {
		return MessageFormat.format(properties.get(key).toString(), param);
	}
	public static void setProperties(Properties properties) {
		WConfig.properties = properties;
	}
}
