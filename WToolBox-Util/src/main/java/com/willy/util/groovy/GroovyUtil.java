package com.willy.util.groovy;

import java.io.File;
import java.io.IOException;

import groovy.util.GroovyScriptEngine;
import groovy.util.ResourceException;
import groovy.util.ScriptException;

public class GroovyUtil {
	public static Object exec(String filePath) throws ResourceException, ScriptException, IOException {
		return exec(filePath, "");
	}
	
	public static Object exec(String filePath, String args) throws ResourceException, ScriptException, IOException {
		File file = new File(filePath);
        GroovyScriptEngine engine = new GroovyScriptEngine(file.getParent());
        //执行获取缓存Class,创建新的Script对象
        Object run = engine.run(file.getName(), args);
        return run;
	}
}
