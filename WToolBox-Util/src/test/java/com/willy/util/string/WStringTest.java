package com.willy.util.string;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.ImmutableList;
import com.google.common.reflect.Reflection;
import com.google.common.reflect.TypeToken;
import com.willy.util.cache.WCache;

@SpringBootTest
@ContextConfiguration
public class WStringTest {
	String expected;
	@Test
	public void parseParamToStrTest() throws ExecutionException {
		String str = "abcsdas${key1}jhfidsu${key2}vhdskjfhdj";
		WCache.cache.put("key1", "aaa");
		WCache.cache.put("key2", "bbb");
		expected = "abcsdasaaajhfidsubbbvhdskjfhdj";
		assertTrue(WString.parseParamsToStrByCache(str).equals(expected));
	}
	@Test
	public void parseParamsToStrByJsonTest() throws JSONException {
		String str = "abcsdas${key1}jhfidsu${key2}vhdskjfhdj";
		expected = "abcsdasaaajhfidsubbbvhdskjfhdj";
		assertTrue(WString.parseParamsToStrByJson(str,new JSONObject("{key1:aaa,key2:bbb}")).equals(expected));
	}
	@Test
	public void fillStrLengthTest() {
		String str = "123";
		expected = "000123";
		assertTrue(WString.fillStrLength(str, "0", -6).equals(expected));
		expected = "123aaa";
		assertTrue(WString.fillStrLength(str, "a", 6).equals(expected));
	}
	@Test
	public void isNumericTest() {
		String str = "102236a";
		String intStr = "12236954";
		assertTrue(!WString.isNumeric(str));
		assertTrue(WString.isNumeric(intStr));
	}
	@Test
	public void getInSqlScriptByListTest() {
		List<String> inElement = new ArrayList<String> ();
		inElement.add("aa");
		inElement.add("bb");
		inElement.add("cc");
		inElement.add("dd");
		expected = " (?,?,?,?) "; 
		assertTrue(WString.getInSqlScriptByList(inElement).equals(expected));
	}
	
}
