package com.willy.util.type;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration
public class WTypeTest {
	@Test
	public void strListToArray2DTest() {
		List<String> strList = new ArrayList<String> ();
		strList.add("ff,gg,hh,ii");
		strList.add("aa,bb,cc,dd,ee");
		String[][] strAr =WType.strListToArray2D(strList);
		String[][] strArResult = new String[][] {{"ff","gg","hh","ii"},{"aa","bb","cc","dd","ee"}};
		assertTrue(Arrays.deepEquals(strAr,strArResult));
		
		strList.clear();
		strList.add("ff:gg:hh:ii");
		strList.add("aa:bb:cc:dd:ee");
		strAr =WType.strListToArray2D(strList,":");
		assertTrue(Arrays.deepEquals(strAr, strArResult));
	}
	@Test
	public void strArListToArray2Dest() {
		List<String[]> strArList = new ArrayList<String[]> ();
		strArList.add(new String[] {"ff","gg","hh","ii"});
		strArList.add(new String[] {"aa","bb","cc","dd","ee"});
		String[][] strAr =WType.strArListToArray2D(strArList);
		String[][] strArResult = new String[][] {{"ff","gg","hh","ii"},{"aa","bb","cc","dd","ee"}};
		assertTrue(Arrays.deepEquals(strAr,strArResult));
	}
	@Test
	public void dateToStrTest() throws ParseException {
		String dateFormat = "yyyyMMdd HH:mm:ss";
		String strDateTime = "20191222 11:28:30";
		Date date = new SimpleDateFormat(dateFormat).parse(strDateTime);
		assertTrue(strDateTime.equals(WType.dateToStr(date,dateFormat)));
	}
	@Test
	public void isTheSameTypeTest() {
		String aa = "aa";
		String bb = "bb";
		assertTrue(WType.isTheSameType(aa, bb));
		int a1 = 1;
		boolean a2 = false;
		assertTrue(!WType.isTheSameType(a1, a2));
		ArrayList<Integer> intList = new ArrayList<Integer>();
		List<Integer> intList1 = new ArrayList<Integer>();
		assertTrue(WType.isTheSameType(intList, intList1));
		List<String> strList = new ArrayList<String>();
		Foo<String> foo = new Foo<String>();
        // 在类的外部这样获取
//        Type type = ((ParameterizedType)foo.get).getActualTypeArguments()[0];
//        System.out.println(type);
		Type t = strList.getClass().getGenericSuperclass();
	     System.out.println(t); 
	    
//	     ParameterizedType p = (ParameterizedType)t;
//	     System.out.println(p.getActualTypeArguments()[0]);
//		assertTrue(!WType.isTheSameType(intList, strList));
		
	}
	static class Foo<T>{
	    public Class<T> getTClass()
	    {
	        Class<T> tClass = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	        return tClass;
	    }
	}
}
