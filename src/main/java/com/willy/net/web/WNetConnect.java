package com.willy.net.web;

import com.willy.file.dictionary.WDictionary;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import org.jsoup.Connection;

public class WNetConnect {
	private static Connection con;
	private static HttpURLConnection urlCon;
	private static HashMap<String, String> dataMap = new HashMap<String, String>();
	private static HashMap<String, String> headerMap = new HashMap<String, String>();

	public static void main(String[] args) throws Exception {
//		urlCon = (HttpURLConnection) new URL(
//				"http://172.24.9.139:7003/ec-sk-web-1/mvc/ca/updateTradeResult")
//						.openConnection();
//		urlCon.setDoOutput(true);
//		urlCon.setUseCaches(false);
//		urlCon.setRequestMethod("POST");
//		urlCon.addRequestProperty("rqXml", "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><Request><TxCode/><Function>SP_INXL_RQ</Function><BizID>INXLEx</BizID><RETURNCODE>1</RETURNCODE><ERRMSG/><CADN>C=TW,O=Chunghwa Telecom Co.\\, Ltd.,OU=Financial User Certification Authority,OU=8220901-CTCB,OU=FXML,CN=80027966--80027966993</CADN><CASN>7D7EDBB99C87E6A5BE44C66CCB60A4DD</CASN><CANotBeforeDate>20210305222446</CANotBeforeDate><CANotAfterDate>20220405222446</CANotAfterDate><UPLOADTIME>20210323163029</UPLOADTIME><DATA>UDQ9UDQmdHhEYXRlPTIwMjEwMzI0JlAzPVAzJlA1PVA1Jm1lbnVJZD1TSzAwMSZDQUlEPTgwMDI3OTY2OTkzJlNQX0FjdGl2ZVhfUlE9U1BfQWN0aXZlWF9SUSZDVVNUT01FUklEPTgwMDI3OTY2JmJ0bk9rPSZVUkwxPWh0dHAlM0ElMkYlMkYxNzIuMjQuOS4xMzklM0E3MDAzJTJGZWMtc2std2ViJTJGbXZjJTJGY2ElMkZ1cGRhdGVTZXNzaW9uJlAyPVAyJlB1bGxEZXZpY2VPdXQ9MSZVc2VFWENPTT0xJmNhVHhuU2VxSWQ9JlAxPVAxJnRva2VuRm9ybU5hbWU9cXVlcnlGb3JtJlZFUklGWVVTRVI9MCZVU0VSSUQ9MzY1Mjgmb3BlcmF0aW9uSWQ9VTAwMDImZnR4RGF0ZT0yMDIxJTJGMDMlMkYyNCZjb2x1bW5Gb3JTb3J0PSZTSUdOREFUQT0mdG9rZW5PdHA9JkNoaXBTZXJpYWxOdW09JmxvZ2luVGltZT1DTFNERExERkRDRkZTRkxTVUZQRFVERkFEQUREQUZERERMQ0RERERERERTRExERERQRERBRFpERFpQRkRERFVERERERERBTEREREREREZEREFEREQmQ09VTlRSWUNPREU9VFcmQWxsb3dVcGxvYWRGaWxlTnVtPTEmU0VTU0lPTklEPU1MQkJSTUROTk9PQ0hFSllSTyU0MFl2OXRnWm1HMjRjR3ZoMnJSMm1SWG5aZmpyVnFjN1pCTDRxbGhESGNYUUoyNlFMTkRRd1YlMjExOTIwNjYxMDA4JTIxMTYxNjQ4ODEzNDI1NSZwYWdlQ250PTAmbGFuZ3VhZ2U9MSZQaW5QYWQ9dHJ1ZSZzb3J0ZXI9QVNDJlRyYW5zVGltZT0yMDIxMDMyMzE2MzAyNCZVUkwzPWh0dHAlM0ElMkYlMkYxNzIuMjQuMzUuNDYlM0E5MDAzJTJGSHR0cE1lc3NhZ2VQcm9jZXNzb3IlMkZjYXNlcnZlciZwcmVDb2x1bW5Gb3JTb3J0PSZJc1VwbG9hZEZpbGU9MCZUeENvZGU9SW5za0NhJklOWExFeD1JTlNLJnRva2VuSWQ9ODAwMjc5NjY5OTMmZXh0c2Vzc2lvbmlkPU1MQkJSTUROTk9PQ0hFSllSTyU3Q1l2OXRnWm1HMjRjR3ZoMnJSMm1SWG5aZmpyVnFjN1pCTDRxbGhESGNYUUoyNlFMTkRRd1YlMjExOTIwNjYxMDA4JTIxMTYxNjQ4ODEzNDI1NSZVUkwyPWh0dHAlM0ElMkYlMkYxNzIuMjQuOS4xMzklM0E3MDAzJTJGZWMtc2std2ViJTJGbXZjJTJGY2ElMkZ1cGRhdGVUcmFkZVJlc3VsdCZDYVJhV2ViVXJsPWh0dHBzJTNBJTJGJTJGMTkyLjE2OC40MC4yNiUzQTgwMDQmY2hlY2tDYT1ZJnxTO2U7UDthO1I7YTtUO2V8ISF1cGxvYWRGaWxlLy89JkZJTEVOQU1FMTIzPSZJU0ZUUD10cnVlJm9wZXJhdGlvbklkVmFsdWU9</DATA><COUNTRYCODE>TW</COUNTRYCODE><CUSTOMERID>80027966</CUSTOMERID><USERID>36528</USERID><SESSIONID>MLBBRMDNNOOCHEJYRO@Yv9tgZmG24cGvh2rR2mRXnZfjrVqc7ZBL4qlhDHcXQJ26QLNDQwV!1920661008!1616488134255</SESSIONID><VERIFYUSER>0</VERIFYUSER><CAID>80027966993</CAID><P1>P1</P1><P2>P2</P2><P3>P3</P3><P4>P4</P4><P5>P5</P5><P6/><P7/><P8/><P9/><P10/><FILECOUNT>1</FILECOUNT><TRANSFILE FILENAME=\"INSKU0001_1_20201019.txt\" FILESEQ=\"1\" FILESIZE=\"0\" HASHFILENAME=\"2db2a06f4236382296a047368d9874565ddbaabd20210323163029_0.txt\" HASHVALUE=\"929d330f9aa4bb6ddc275e55ab56346982792328\"/></Request>");
//		urlCon.connect();
//		Map<String, List<String>> aa = urlCon.getHeaderFields();
//		System.out.println(ToStringBuilder.reflectionToString(aa));
//		download("https://www.taifex.com.tw/cht/3/totalTableDateDown?firstDate=2018/03/31 00:00&lastDate=2021/03/31 00:00&queryStartDate=2018/04/02&queryEndDate=2018/04/02","D:/aa.txt");
	}

	public static void addHeader(HashMap<String, String> headers) {
		headerMap.putAll(headers);
	}

	public static void addHeader(String key, String value) {
		headerMap.put(key, value);
	}

	public static void addData(HashMap<String, String> datas) {
		dataMap.putAll(datas);
	}

	public static void addData(String key, String value) {
		dataMap.put(key, value);
	}

	public static void download(String url, String toPath) throws IOException {

		BufferedInputStream is = null;
		FileOutputStream fos = null;
		try {
			if (!new File(toPath).exists()) {
				urlCon = (HttpURLConnection) new URL(url).openConnection();
				urlCon.setDoOutput(true);
				urlCon.setUseCaches(false);
				urlCon.setRequestMethod("GET");
				if ((dataMap.size() + headerMap.size()) == 0) {
					putDefaultHeader();
				}
				setHeaderAndData(urlCon);
				urlCon.connect();
				is = new BufferedInputStream(urlCon.getInputStream());
				WDictionary.mergeDir(toPath);
				fos = new FileOutputStream(new File(toPath));
				byte[] b = new byte[8192];
				int l = 0;
				while ((l = is.read(b)) != -1) {
					fos.write(b, 0, l);
				}
			}
		} catch (IOException e) {
			throw e;
		} finally {
			try {
				if (fos != null)
					fos.close();
				if (is != null)
					is.close();
				if (urlCon != null) {
					urlCon.disconnect();
				}
				headerMap.clear();
				dataMap.clear();
			} catch (Exception e) {
				throw e;
			}
		}
	}

	private static void setHeaderAndData(Connection conn) {
		con.headers(headerMap);
		con.data(dataMap);
	}

	private static void setHeaderAndData(HttpURLConnection conn) {
		String key;
		Iterator<String> keyIt = headerMap.keySet().iterator();
		try {
			while (keyIt.hasNext()) {
				key = keyIt.next();
				conn.setRequestProperty(key, headerMap.get(key));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 設定連線Header
	/**
	 * 設定預設header
	 * 
	 */
	public static void putDefaultHeader() {
		headerMap.put("Accept",
				"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
		headerMap.put("Accept-Encoding", "gzip, deflate");
		headerMap.put("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6");
		headerMap.put("Cache-Control", "max-age=0");
		headerMap.put("Connection", "keep-alive");
		headerMap.put("Cookie",
				"stock_user_uuid=04276be0-30d8-4c98-812d-a94e3bf9897d; stock_config=YTozOntzOjc6IlN0b2NrSWQiO047czozOiJ0YWciO2k6MztzOjQ6InR5cGUiO2k6Mjt9; stock_popup_personalnews=1");
		headerMap.put("Host", "pchome.megatime.com.tw");
		headerMap.put("Referer", "http://pchome.megatime.com.tw/group/");
		headerMap.put("Upgrade-Insecure-Requests", "1");
		headerMap.put("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36");
		dataMap.put("IsCheck", "1");
	}

}
