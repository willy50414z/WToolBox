package com.willy.net.web;

import com.willy.util.cache.WCache;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import lombok.Data;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;
@Service
@Data
public class WJsoup {
	private Method actionMode = Method.POST;
	private Document doc;
	private Connection con;
	private int timeoutMillionSeconds = 30000;
	private HashMap<String, String> extraHeaderMap = new HashMap<String, String>();
	private HashMap<String, String> formDataMap = new HashMap<String, String>();
	
	public Element getElement(String url) throws IOException {
		return Jsoup.connect(url).get().body();
	}
	
	public static String download(String url, String filePath) {
		FileOutputStream out = null;
		try {
			Response resultImageResponse = Jsoup.connect(url).ignoreContentType(true).execute();
			out = (new FileOutputStream(new File(filePath)));
			out.write(resultImageResponse.bodyAsBytes());
			return filePath;
		}catch(Exception e) {
			WLog.error(e);
			return "";
		}finally {
			if(out != null) {
				try {
					out.close();
				} catch (IOException e) {
					WLog.error(e);
					return "";
				}
			}
		}
	}
	
	public Document getBody(String url) throws IOException, InterruptedException, ExecutionException {
		//同網域連線間格5秒
		WCache.waitBetweenAccess(WString.getDomainNameByUrl(url), 5000);
		this.con = Jsoup.connect(url).timeout(timeoutMillionSeconds);
		setHeader();
		setFormDate();
		Response rs = this.con.method(actionMode).execute();
		doc = Jsoup.parse(rs.body());
		timeoutMillionSeconds=30000;
		extraHeaderMap.clear();
		formDataMap.clear();
		return doc;
	}
	public Document getBody(String url, String encoding) throws IOException, InterruptedException, ExecutionException {
		//同網域連線間格5秒
		WCache.waitBetweenAccess(WString.getDomainNameByUrl(url), 5000);
		this.con = Jsoup.connect(url).timeout(timeoutMillionSeconds);
		setHeader();
		setFormDate();
		Response rs = this.con.method(actionMode).execute();
		doc = Jsoup.parse(new String(rs.bodyAsBytes(), encoding));
		timeoutMillionSeconds=30000;
		extraHeaderMap.clear();
		formDataMap.clear();
		return doc;
	}
	public String getString(String url, String encoding) throws IOException, InterruptedException, ExecutionException {
		//同網域連線間格5秒
		WCache.waitBetweenAccess(WString.getDomainNameByUrl(url), 5000);
		this.con = Jsoup.connect(url).requestBody("JSON").ignoreContentType(true).timeout(timeoutMillionSeconds);
		setHeader();
		setFormDate();
		Response rs = this.con.method(actionMode).execute();
		timeoutMillionSeconds=30000;
		extraHeaderMap.clear();
		formDataMap.clear();
		return new String(rs.bodyAsBytes(), encoding);
	}
	public Document get(String url, String encoding) throws IOException, InterruptedException, ExecutionException {
		//同網域連線間格5秒
		WCache.waitBetweenAccess(WString.getDomainNameByUrl(url), 5000);
		doc = Jsoup.parse(new URL(url).openStream(), encoding, url);
		timeoutMillionSeconds=30000;
		extraHeaderMap.clear();
		formDataMap.clear();
		return doc;
	}

	public void putHeader(String key, String value) {
		extraHeaderMap.put(key, value);
	}

	public void putFormData(String key, String value) {
		formDataMap.put(key, value);
	}

	private void setHeader() {
		HashMap<String, String> headerMap = new HashMap<String, String>();
		headerMap.putAll(getDefaultHeader());
		headerMap.putAll(extraHeaderMap);
		this.con.headers(headerMap);
	}

	private void setFormDate() {
		this.con.data(formDataMap);
	}

	private HashMap<String, String> getDefaultHeader() {
		HashMap<String, String> defaultHeader = new HashMap<String, String>();
		defaultHeader.put("Accept",
				"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
		defaultHeader.put("Accept-Encoding", "gzip, deflate");
		defaultHeader.put("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6");
		defaultHeader.put("Cache-Control", "max-age=0");
		defaultHeader.put("Connection", "keep-alive");
		defaultHeader.put("Cookie",
				"stock_user_uuid=04276be0-30d8-4c98-812d-a94e3bf9897d; stock_config=YTozOntzOjc6IlN0b2NrSWQiO047czozOiJ0YWciO2k6MztzOjQ6InR5cGUiO2k6Mjt9; stock_popup_personalnews=1");
		defaultHeader.put("Host", "pchome.megatime.com.tw");
		defaultHeader.put("Referer", "http://pchome.megatime.com.tw/group/");
		defaultHeader.put("Upgrade-Insecure-Requests", "1");
		defaultHeader.put("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36");
		return defaultHeader;
	}
}
