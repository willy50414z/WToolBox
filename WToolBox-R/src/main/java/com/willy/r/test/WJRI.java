package com.willy.r.test;

import org.rosuda.JRI.Rengine;
import org.springframework.beans.factory.annotation.Autowired;

public class WJRI {
	@Autowired
	private Rengine re;
	public String executeStr(String script) {
		return re.eval(script).asString();
	}
}
