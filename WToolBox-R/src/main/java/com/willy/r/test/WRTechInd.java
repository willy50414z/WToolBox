package com.willy.r.test;

import com.willy.r.util.WRCmd;
import com.willy.r.util.WRPkgUtil;
import com.willy.r.util.WRScriptBuilder;
import java.util.List;
import org.rosuda.JRI.REXP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//引用TTR套件
@Service
public class WRTechInd {
	@Autowired
	private WRScriptBuilder scBuilder;
	@Autowired
	private WRCmd rCmd;
	public double[][] getMACD(List<String> closePriceList){
		return getMACD(10, 20, closePriceList);
	}
	public double[][] getMACD(int nFast, int nSlow, List<String> closePriceList){
		WRPkgUtil.mergePackages(rCmd, "zoo", "TTR");
		String[][] dataAr = new String[1][closePriceList.size()];
		closePriceList.toArray(dataAr[0]);
		String createVector = scBuilder.createDigitVector("close", dataAr[0]);
		rCmd.exec(createVector);
		rCmd.exec("macd<-MACD(close, percent=F, nFast = "+nFast+", nSlow = "+nSlow+", nSig =9)");
		rCmd.exec("osc<-(macd[,\"macd\"]-macd[,\"signal\"])*2");
		REXP rexp = rCmd.exec("cbind(macd, osc)");
		return rexp == null ? null : rexp.asMatrix();
	}
 }
