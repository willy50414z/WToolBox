package com.willy.r.test;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.rosuda.JRI.Rengine;
import org.springframework.beans.factory.annotation.Autowired;

import com.willy.r.util.WRTypeUtil;
import com.willy.util.log.WLog;
import com.willy.util.reflect.WReflect;
import com.willy.util.string.WRegex;
import com.willy.util.string.WString;

//@Repository
public class WEvaluate {
	@Autowired
	private Rengine re;
	
	/**
	 *	 包含故各種運算子的計算 加減乘除 回傳
	 * @param formula
	 * @return
	 */
	public Double evalNumeric(String formula) {
		WLog.debug("formula : "+formula);
		return re.eval(formula).asDouble();
	}
	
	/**
	 *	 包含故各種運算子的計算 加減乘除><= True False
	 * @param formula
	 * @return 
	 */
	public Boolean evalBoolean(String formula) {
		formula = formula.toUpperCase();//將TF轉為大寫
		WLog.debug("formula : "+formula);
		return Boolean.valueOf(re.eval(formula).asBool().toString());
	}
	
	/**
	 * 
	 * @param formula 公式  :  參數key以${key}表示   呼叫方法以[method(..)]表示
	 * @param paramMap 參數Key與其對應的結果值，ex. ${key}
	 * @param funcObj 含有各種method供formula調用，不可有多載方法
	 * @return
	 * @throws ExecutionException 
	 */
	public Boolean evalBoolean(String formula, Map<String, Object> paramMap, Object... funcObjs) throws ExecutionException {
				
				// 宣告
				String methodName;
				Object[] args;
				String[] argsStr;
				Object result;
				String tempFunc;

				// 獲得可調用方法
				Iterator<String> methodKeyIt;
				String methodKey;
				HashMap<String, Object> methodObjMap = new HashMap<String, Object>();//方法名稱和所屬物件對應
				HashMap<String, Method> methodsMap = new HashMap<String, Method>();//方法名稱和方法對應
				HashMap<String, Method> tempMethodsMap = new HashMap<String, Method>();
				for(Object funcObj : funcObjs) {
					//暫存該物件所有Method
					tempMethodsMap = WReflect.getAllMethodMap(funcObj.getClass());
					
					//建立方法名稱與物件mapping
					methodKeyIt = tempMethodsMap.keySet().iterator();
					while(methodKeyIt.hasNext()) {
						methodKey = methodKeyIt.next();
						methodObjMap.put(methodKey, funcObj);
					}
					
					//將方法名稱與方法對應放入MAP
					methodsMap.putAll(WReflect.getAllMethodMap(funcObj.getClass()));
					
					tempMethodsMap.clear();
				}

				// 函式執行結果
				Iterator<String> funcResultIt;
				String funcResultKey;
				HashMap<String, String> resultMap = new HashMap<String, String>();

				// 拆分函式
				List<String> funcList = getFormulaList(formula);

				// 遍覽Function
				for (String func : funcList) {
					try {
						//暫存函式進tempFunc，避免原函式被取代掉
						tempFunc = func;
						
						// 取代掉已執行過的func成Key
						funcResultIt = resultMap.keySet().iterator();
						while (funcResultIt.hasNext()) {
							funcResultKey = funcResultIt.next();
							tempFunc = tempFunc.replace(funcResultKey, resultMap.get(funcResultKey));
						}

						// 取得Method名稱
						methodName = tempFunc.substring(1, func.indexOf("(")).toLowerCase();

						// 配置參數
						argsStr = tempFunc.substring((tempFunc.indexOf("(") + 1), tempFunc.indexOf(")")).split(",");
						args = new Object[argsStr.length];
						for (int argIndex = 0; argIndex < args.length; argIndex++) {
							if (WRegex.match(argsStr[argIndex].toString(), "[${][^}]*[}]")) {
								args[argIndex] = paramMap.get(argsStr[argIndex]);
							}else {
								args[argIndex] = argsStr[argIndex];
							}
						}

						// 執行方法
						Method m = methodsMap.get(methodName);
						result = m.invoke(methodObjMap.get(methodName), (Object) args);

						// 將執行結果塞入cache
						if (result instanceof Boolean) {
							resultMap.put(func, result.toString());
						} else {
							// 回傳值不是String表示回傳資料集，後面還會用到
							resultMap.put(func, "${resultOfMethod_" + paramMap.size() + "}");
							paramMap.put("${resultOfMethod_" + paramMap.size() + "}", result);
						}

					} catch (Exception e) {
						WLog.error("將公式中的函式轉換為TF發生錯誤\r\n Func : "+func, e);
					}
				}
				
				//取出最外圈的函式(不為其他函式參數的函式)
				funcList = WRegex.getMatchedStrList(formula, "\\[[^&|]*\\]");
				//將公式中函式取代為T/F
				for(String func : funcList) {
					formula = formula.replace(func, resultMap.get(func));
				}
		return this.evalBoolean(formula);
	}

	// 將公式依序塞入List，公式中可能含${xx}變數，不可直接丟給R
	public static List<String> getFormulaList(String formula) {
		List<String> funcList = new LinkedList<String>();

		// 以&和|拆分公式
		String[] funcAr = formula.split("&|\\|");

		// 逐個function拆解成更細的function
		for (String func : funcAr) {
			// 取出呼叫的方法
			funcList.addAll(WString.getIncludedStrList(func, "[", "]"));
		}
		return funcList;
	}
	/**
	 *   取得皮爾森相關係數
	 * @param arr1
	 * @param arr2
	 * @return
	 */
	public Double getPCC(String[] arr1,String[] arr2) {
		re.eval(WRTypeUtil.arToVector("vector1", arr1));
		re.eval(WRTypeUtil.arToVector("vector2", arr2));
		double pcc=re.eval("cor(vector1,vector2)").asDouble();
		re.end();
		return pcc;
	}
}
