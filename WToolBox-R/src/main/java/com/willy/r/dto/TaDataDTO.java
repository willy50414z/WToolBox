package com.willy.r.dto;

import com.willy.r.constant.TaType;
import java.util.HashMap;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TaDataDTO {
  private TaType taType;
  private HashMap<String, Object> taParamMap;
}
