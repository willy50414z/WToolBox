package com.willy.r.util;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.willy.util.type.WType;

/**
 * 
 * @author Willy 產生將Java物件轉換成R語法
 */
@Repository
public class WRTypeUtil {
	private static StringBuffer script;

	/**
	 * 生成將Array轉換成R物件語法
	 * 
	 * @param beanName
	 * @param dataAr
	 * @return
	 */
	public static String createMatrix(String beanName, String[][] dataAr) {
		script = new StringBuffer();
		script.append(beanName).append("<-matrix(c(");
		for (String[] rowAr : dataAr) {
			for (String cell : rowAr) {
				script.append(cell).append(",");
			}
		}
		script.replace(script.length() - 1, script.length(), ")");
		script.append(",").append(dataAr.length);
		script.append(",").append(dataAr[0].length);
		script.append(",byrow = TRUE)");
		return script.toString();
	}

	/**
	 * 建立向量Vector
	 * 
	 * @param varName
	 * @param dataAr
	 * @return
	 */
	public static String arToVector(String varName, String[] dataAr) {
		script = new StringBuffer();
		script.append(varName).append("<-c(");
		for (Object data : dataAr) {
			script.append(data.toString()).append(",");
		}
		script.replace(script.length() - 1, script.length(), ")");
		return script.toString();
	}
	
	public static String listToStringVector(String varName, List<?> dataAr) {
		script = new StringBuffer();
		script.append(varName).append("<-c(");
		for (Object data : dataAr) {
			script.append(data.toString()).append(",");
		}
		script.replace(script.length() - 1, script.length(), ")");
		return script.toString();
	}

	/**
	 * 建立日期向量Vector
	 * 
	 * @param var
	 * @param dataList
	 * @return
	 * @throws ParseException
	 */
	public static String createDateVector(String var, List<Date> dataList) throws ParseException {
		script = new StringBuffer();
		script.append(var).append("<-c(");
		for (Date data : dataList) {
			script.append("as.Date('" + WType.dateToStr(data, "yyyy-MM-dd") + "')").append(",");
		}
		script.replace(script.length() - 1, script.length(), ")");
		return script.toString();
	}
}
