package com.willy.r.util;

import org.rosuda.JRI.REXP;
import org.rosuda.JRI.Rengine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.willy.util.log.WLog;

import lombok.Getter;

@Service
@Getter
public class WRCmd {
	@Autowired
	private Rengine re;
	public REXP exec(String script) {
		WLog.debug("R EXE Script["+script+"]");
		REXP rexp = re.eval(script);
		WLog.debug("Response["+((rexp == null) ? "" : rexp.toString())+"]");
		return re.eval(script);
	}
}
