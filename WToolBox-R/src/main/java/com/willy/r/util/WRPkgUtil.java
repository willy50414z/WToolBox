package com.willy.r.util;

import java.util.Arrays;
import org.rosuda.JRI.REXP;
import org.springframework.stereotype.Service;

@Service
public class WRPkgUtil {
	public static void mergePackages(WRCmd rCmd, String... packageNames) {
		for (String packageName : packageNames) {
			if (!WRPkgUtil.isPackageInstall(rCmd, packageName)) {
				rCmd.exec("install.packages(\""+packageName+"\")");
			}
			if (!WRPkgUtil.isPackageLib(rCmd, packageName)) {
				rCmd.exec("library("+packageName+")");
			}
		}
	}

	public static boolean isPackageInstall(WRCmd rCmd, String packageName) {
		REXP rexp = rCmd.exec("installed.packages()[,c('Package')]");
		String[] installedPackages = rexp == null ? new String[0] : rexp.asStringArray();
		if(installedPackages == null) {
			return false;
		} else {
			return Arrays.asList(installedPackages).contains(packageName);
		}
	}

	public static boolean isPackageLib(WRCmd rCmd, String libraryName) {
		String[] libPackages = rCmd.exec("(.packages())").asStringArray();
		return Arrays.asList(libPackages).contains(libraryName);
	}
}
