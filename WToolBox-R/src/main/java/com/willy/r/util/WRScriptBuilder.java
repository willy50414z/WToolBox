package com.willy.r.util;

import com.willy.file.dictionary.WDictionary;
import com.willy.r.constant.TaType;
import com.willy.r.dto.TaDataDTO;
import com.willy.util.date.WDate;
import com.willy.util.type.WType;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Willy
 *   產生將Java物件轉換成R語法
 */
@Repository
public class WRScriptBuilder {
	private StringBuilder script;

	private final String mirrorUrl = "https://cran.csie.ntu.edu.tw/";
	private final String[] maColors = new String[] {"yellow", "sandybrown", "paleturquoise1", "plum1", "slateblue1"};
	
	public String createDigitVector(String beanName,String[] dataAr) {
		script=new StringBuilder();
		script.append(beanName).append("<-c(");
		for(String cell : dataAr) {
			script.append(cell).append(",");
		}
		script.setLength(script.length() - 1);
		script.append(")");
		return script.toString();
	}
	public String createDigitVector(String beanName,List<String> dataList) {
		script=new StringBuilder();
		script.append(beanName).append("<-c(");
		dataList.forEach(data -> script.append(data).append(","));
		script.setLength(script.length() - 1);
		script.append(")");
		return script.toString();
	}
	/**
	 *  生成將Array轉換成R物件語法
	 * @param beanName
	 * @param dataAr
	 * @return
	 */
	public String createMatrix(String beanName,String[][] dataAr) {
		script=new StringBuilder();
		script.append(beanName).append("<-matrix(c(");
		for(String[] rowAr : dataAr) {
			for(String cell : rowAr) {
				script.append("\"").append(cell).append("\"").append(",");
			}
		}
		script.setLength(script.length() - 1);
		script.append(")");
		script.append(",").append(dataAr.length);
		script.append(",").append(dataAr[0].length);
		script.append(",byrow = TRUE)");
		return script.toString();
	}
	public String createMatrix(String beanName,List<String[]> dataList) {
		script=new StringBuilder();
		script.append(beanName).append("<-matrix(c(");
		for(String[] rowAr : dataList) {
			for(String cell : rowAr) {
				script.append("\"").append(cell).append("\"").append(",");
			}
		}
		script.setLength(script.length() - 1);
		script.append(")");
		script.append(",").append(dataList.size());
		script.append(",").append(dataList.get(0).length);
		script.append(",byrow = TRUE)");
		return script.toString();
	}
	/**
	 * 建立向量Vector
	 * @param varName
	 * @param dataAr
	 * @return
	 */
	public String arToVector(String varName,String[] dataAr) {
		script=new StringBuilder();
		script.append(varName).append("<-c(");
		for(Object data : dataAr) {
			script.append(data.toString()).append(",");
		}
		script.replace(script.length()-1, script.length(), ")");
		return script.toString();
	}
	
	public String arToVector(String varName,List<String> dataAr) {
		script=new StringBuilder();
		script.append(varName).append("<-c(");
		for(Object data : dataAr) {
			script.append(data.toString()).append(",");
		}
		script.replace(script.length()-1, script.length(), ")");
		return script.toString();
	}

	public String setWorkspace(String workspace) {
		return "setwd('"+workspace.replace("\\", "/")+"')";
	}

	public String importPackage(String... pkgNames) {
		StringBuffer script = new StringBuffer();
		for (String pkgName : pkgNames) {
			script.append("library(").append(pkgName).append(")\r\n");
		}
		return script.toString();
	}

	public String generateCandleChart(String chartName, Date startDate, Date endDate, String filePath, List<String[]> priceList, List<TaDataDTO> taDataList) throws IllegalArgumentException{
		//去認資料來源沒問題
		this.checkCandleChartDataSource(priceList);

		String strStartDate = WType.dateToStr(startDate, WDate.dateFormat_yyyyMMdd_Dash);
		String strEndDate = WType.dateToStr(endDate, WDate.dateFormat_yyyyMMdd_Dash);

		//確認產檔路徑
		String workspace = WDictionary.getFileDir(filePath);

		WDictionary.mergeDir(workspace);

		StringBuilder script = new StringBuilder();
		script.append(this.setWorkspace(workspace)).append("\r\n");

		//確認載入所需Package
		script.append(this.importPackage("quantmod", "xts")).append("\r\n");

		//取得所需物件
		script.append(this.createMatrix("stockPrice",priceList)).append("\r\n");
		script.append("date<-stockPrice[,1]\r\n");
		script.append("as.Date(date)->date\r\n");
		script.append("sp<-xts(apply(stockPrice[,2:ncol(stockPrice)], 2, as.numeric), order.by=date)\r\n");
		script.append("colnames(sp)<-c(\"Open\",\"High\",\"Low\",\"Close\",\"Volume\")\r\n");

		// addXX
		StringBuilder taScript = new StringBuilder();
		if(CollectionUtils.isNotEmpty(taDataList)) {
			for(TaDataDTO taData : taDataList) {
				if(!taData.getTaType().equals(TaType.TA)) {
					HashMap<String, Object> paramData = taData.getTaParamMap();
					if(paramData == null || paramData.size() == 0) {
						taScript.append("add").append(taData.getTaType()).append("();");
					} else {
						taScript.append("add").append(taData.getTaType()).append("(");
						paramData.forEach((key, value) -> {taScript.append(key).append("=").append(value).append(", ");});
						taScript.setLength(taScript.length()-2);
						taScript.append(");");
					}
				}
			}
		}

		// addTA
		StringBuilder addTaScript = new StringBuilder();
		if(CollectionUtils.isNotEmpty(taDataList)) {
			for (TaDataDTO taData : taDataList) {
				HashMap<String, Object> paramData = taData.getTaParamMap();
				if (taData.getTaType().equals(TaType.TA)) {
					addTaScript.append("taXtsData<-xts(as.numeric(c(" + paramData.get("data") + ")), order.by=subset(date, date >= as.Date(\"").append(strStartDate).append("\") & date <= as.Date(\"").append(strEndDate).append("\") ))\r\n ");
					addTaScript.append("addTA(taXtsData,");
					paramData.forEach((key, value) -> {
						if(!key.equals("data")) {
							addTaScript.append(key).append("=").append(value).append(", ");
						}
					});
					addTaScript.setLength(addTaScript.length()-2);
					addTaScript.append(")\r\n");
				}
			}

		}

		script.append("png(\"").append(filePath.replace("\\", "/")).append("\", width = 1000, height = 600)\r\n");
		script.append("chartSeries(sp,theme = chartTheme(\"black\"),up.col='red',dn.col='green', name=\"").append(chartName);
		script.append("\", TA=\""+taScript+"\", subset='").append(strStartDate).append("::").append(strEndDate);
		script.append("')\r\n");
		script.append(addTaScript);
		script.append("dev.off()\n");
		return script.toString();
	}

	private void checkCandleChartDataSource(List<String[]> priceAr) throws IllegalArgumentException {
		if(!(priceAr.size()>0 && priceAr.get(0).length==6)) {
			throw new IllegalArgumentException("來源資料格式有誤 正確格式為 : [Date,Open,High,Low,Close,Vol]");
		}
	}
}
