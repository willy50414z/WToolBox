package com.willy.r.constant;

public enum TaType {
  SMA, CCI, EMA, Lines, MACD, Points, RSI, Vo, TA;
}
