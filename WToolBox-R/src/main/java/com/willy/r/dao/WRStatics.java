package com.willy.r.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.rosuda.JRI.REXP;
import org.rosuda.JRI.Rengine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.willy.r.util.WRScriptBuilder;
import com.willy.util.log.WLog;

@Component
public class WRStatics {
	@Autowired
	private Rengine re;
	@Autowired
	private WRScriptBuilder scriptBuilder;
	/**
	 *   取得皮爾森相關係數
	 * @param arr1
	 * @param arr2
	 * @return
	 */
	public Double getPCC(String[] arr1,String[] arr2) {
		re.eval(scriptBuilder.arToVector("vector1", arr1));
		re.eval(scriptBuilder.arToVector("vector2", arr2));
		double pcc=re.eval("cor(vector1,vector2)").asDouble();
		re.end();
		return pcc;
	}
	public Double getPCC(List<BigDecimal> arr1,List<BigDecimal> arr2) {
		re.eval(scriptBuilder.arToVector("vector1", arr1.stream().map(a-> a.toString()).collect(Collectors.toList())));
		re.eval(scriptBuilder.arToVector("vector2", arr2.stream().map(a-> a.toString()).collect(Collectors.toList())));
		WLog.info(scriptBuilder.arToVector("vector1", arr1.stream().map(a-> a.toString()).collect(Collectors.toList())));
		WLog.info(scriptBuilder.arToVector("vector2", arr2.stream().map(a-> a.toString()).collect(Collectors.toList())));
		org.rosuda.JRI.REXP dd = re.eval("cor(vector1,vector2)");
		re.end();
		if(dd == null) {
			return -1.0;
		} else {
			return dd.asDouble();
		}
	}
	/**
	 * 	取得標準差
	 * @param arr1
	 * @return
	 */
	public Double getSd(String[] arr1) {
		re.eval(scriptBuilder.arToVector("vector1", arr1));
		double sd=re.eval("sd(vector1)").asDouble();
		re.end();
		return sd;
	}
	public Double getSd(List<String> arr1) {
		re.eval(scriptBuilder.arToVector("vector1", arr1));
		REXP rexp = re.eval("sd(vector1)");
		if(rexp == null) {
			throw new NullPointerException("can't get sd");
		}
		double sd=rexp.asDouble();
		re.end();
		return sd;
	}
	public Double getMean(List<String> arr1) {
		re.eval(scriptBuilder.arToVector("vector1", arr1));
		double sd=re.eval("mean(vector1)").asDouble();
		re.end();
		return sd;
	}
}
