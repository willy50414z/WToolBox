package com.willy.r.dao;

import com.willy.file.dictionary.WDictionary;
import com.willy.file.txt.WFile_txt;
import com.willy.r.dto.TaDataDTO;
import com.willy.r.util.WRCmd;
import com.willy.r.util.WRPkgUtil;
import com.willy.r.util.WRScriptBuilder;
import com.willy.r.util.WRTypeUtil;
import com.willy.util.cmd.CMDUtil;
import com.willy.util.log.WLog;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class WRChart {
	@Autowired
	private WRCmd rCmd;
	@Autowired
	private WRScriptBuilder scriptBuilder;
	@Autowired
	private WFile_txt fileTxt;
	@Autowired
	private CMDUtil cmd;

	private final String[] colorsNameAr = new String[]{"red","goldenrod1","chartreuse2","blue3","blueviolet"};

	public boolean genCandleChart(String chartName, Date startDate, Date endDate,
			String filePath, List<String[]> priceAr, List<TaDataDTO> taDataDtoList) {
		String script = "";
		try {
			String rScriptFilePath = WDictionary.getFileDir(filePath) + "generateCandleChart.R";

			//get R script
			script = scriptBuilder.generateCandleChart(chartName, startDate, endDate,
					filePath, priceAr, taDataDtoList);

			//export to R file
			fileTxt.writeByStr(rScriptFilePath, script, false);

			//execute R file
			cmd.exec("RScript " + rScriptFilePath);
		} catch (Exception e) {
			WLog.error("generate candle chart failed!!, script[" + script + "]");
			return false;
		}
		return true;
	}
	
	public void tsPlot(String filePath, List<Date> dateList, List<?> valueList) {
		try {
			WLog.debug(rCmd.exec(WRTypeUtil.createDateVector("dateVector", dateList)).asString());
			WLog.debug(rCmd.exec(WRTypeUtil.listToStringVector("valueVector", valueList)).asString());
			WLog.debug(rCmd.exec("df <- data.frame(Year = dateVector, Weight = valueVector)").asString());
			WLog.debug(rCmd.exec("png(\"" + filePath.replace("\\", "/") + "\", width = 1000, height = 600)").asString());
			WLog.debug(rCmd.exec("plot(df$Year, df$Weight, type = \"l\", ylab = \"Data1\", main = \"plot\", xlab = \"Date\", col = \"blue\", xaxt = \"n\",lwd = 2)").asString());
			WLog.debug(rCmd.exec("axis.Date(side = 1, df$Year, format = \"%Y/%m/%d\")").asString());
			WLog.debug(rCmd.exec("dev.off()").asString());
		} catch (ParseException e) {
			WLog.error(e);
		}
	}
	
	public void mutiTsPlot(String filePath, List<Date> dateList, List<?>... valueList) {
		try {
			WRPkgUtil.mergePackages(rCmd, "reshape2");
			WLog.debug(rCmd.exec(WRTypeUtil.createDateVector("date", dateList)).asString());
			
			StringBuffer datasSb = new StringBuffer();
			StringBuffer legendNameSb = new StringBuffer();
			StringBuffer legendColorSb = new StringBuffer();
			StringBuffer legendPchSb = new StringBuffer();
			for(int i=0;i<valueList.length;i++) {
				WLog.debug(rCmd.exec(WRTypeUtil.listToStringVector("data" + i, valueList[i])).asString());
				datasSb.append("data").append(i).append(",");
				legendNameSb.append("\"").append("data").append(i).append("\",");
				legendColorSb.append("\"").append(colorsNameAr[i]).append("\",");
				legendPchSb.append(17+i*2).append(",");
			}
			datasSb.setLength(datasSb.length()-1);
			legendNameSb.setLength(legendNameSb.length()-1);
			legendColorSb.setLength(legendColorSb.length()-1);
			legendPchSb.setLength(legendPchSb.length()-1);
			
			WLog.debug(rCmd.exec("df <- data.frame(date, "+datasSb.toString()+")").asString());
			
			WLog.debug(rCmd.exec("png(\"" + filePath.replace("\\", "/") + "\", width = 1000, height = 600)").asString());
			
			WLog.debug(rCmd.exec("plot(df$date, df$data0, type = \"l\", ylab = \"Data1\", main = \"plot\", xlab = \"Date\", col = \""+colorsNameAr[0]+"\", xaxt = \"n\",lwd = 2)").asString());
			WLog.debug(rCmd.exec("axis.Date(side = 1, df$date, format = \"%Y/%m/%d\")").asString());
			for(int i=1;i<valueList.length;i++) {
				WLog.debug(rCmd.exec("par(new = TRUE)").asString());
				WLog.debug(rCmd.exec("plot(df$date, df$data"+i+", type = \"l\", xaxt = 'n', yaxt='n', ylab = '', xlab = '', col = \""+colorsNameAr[i]+"\",lwd = 2)").asString());
				if(i==1) {
					WLog.debug(rCmd.exec("axis(side = 4)").asString());
					WLog.debug(rCmd.exec("mtext(\"Data2\", side = 4, line = 3) ").asString());
				}
			}
			
			
			WLog.debug(rCmd.exec("legend( x = \"bottomright\", legend = c("+legendNameSb+"), col = c("+legendColorSb+"), lwd = 2, pch = c("+legendPchSb+") )").asString());
				 
			WLog.debug(rCmd.exec("dev.off()").asString());
		} catch (ParseException e) {
			WLog.error(e);
		}
	}

}
