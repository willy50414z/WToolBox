package com.willy.r.spring.config;

import org.rosuda.JRI.Rengine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RConfig {
	static {
	    System.loadLibrary("jri");
	}
	@Bean(destroyMethod="end")
	public Rengine getRengine() {
		return new Rengine(new String[] {}, false, null);
	}
}
