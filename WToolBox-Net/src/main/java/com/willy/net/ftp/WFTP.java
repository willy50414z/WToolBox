package com.willy.net.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTPClient;
import org.springframework.stereotype.Service;

//FTPUtil ftp=new FTPUtil("172.24.15.19","AP_CSFDS_PUT","pr4jr6kb");
//ftp.put("D:/cc.txt","/aptoap/mof/ddd.txt");
//
//ftp=new FTPUtil("172.24.15.19","AP_CSFDS_GET","kw3gt7px");
//ftp.get("/aptoap/mof/ddd.txt", "D:/eee.txt");
//@Service
public class WFTP {
	private String hostIp = "";
	private String userName = "";
	private String password = "";
	private FTPClient fc;
	private InputStream in = null;
	private OutputStream os = null;
	private FileInputStream fis=null;
	private File f=null;
	private boolean isAutoClose=true;
	public WFTP(String hostIp, String userName, String password) {
		super();
		this.hostIp = hostIp;
		this.userName = userName;
		this.password = password;
		fc = new FTPClient();
	}

	

	public boolean get(String fromPath, String toPath) throws IOException {
		try {
			fc.connect(this.hostIp);
			fc.login(this.userName, this.password);
			in = fc.retrieveFileStream(fromPath);
			if (in != null) {
				byte[] buffer = new byte[4096];
				f = new File(toPath);
				os = new FileOutputStream(f);
				int b;
				while ((b = in.read(buffer)) != -1) {
					os.write(buffer, 0, b);
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if(isAutoClose) {
				fc.disconnect();
			}
			in.close();
			os.flush();
			os.close();
		}

		return true;
	}

	public boolean put(String fromPath, String toPath) throws IOException {
		try {
			fc.connect(this.hostIp);
			fc.login(this.userName, this.password);
			os = fc.appendFileStream(toPath);
			byte[] buffer = new byte[4096];
			f = new File(fromPath);
			fis = new FileInputStream(f);
			int b;
			while ((b = fis.read(buffer)) != -1) {
				os.write(buffer, 0, b);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			if(isAutoClose) {
				fc.disconnect();
			}
			fis.close();
			os.flush();
			os.close();
		}
		return true;

	}
	public void setAutoClose(boolean isAutoClose) {
		this.isAutoClose = isAutoClose;
	}
}
