package com.willy.net.video;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Security;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import com.willy.file.dictionary.WDictionary;
import com.willy.net.utils.MediaFormatUtil;
import com.willy.net.utils.StringUtils;
import com.willy.util.constant.WConst;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;

/**
 * @author liyaling
 * @email ts_liyaling@qq.com
 * @date 2019/12/14 16:02
 */
public class WM3u8Downloader {

	@Override
	public String toString() {
		return "WM3u8Downloader [m3u8Url=" + m3u8Url + ", downloadDir=" + downloadDir + ", videoName=" + videoName
				+ "]";
	}

	private String m3u8Url, downloadDir, videoName;

	public WM3u8Downloader(String m3u8Url, String downloadDir, String videoName) {
		super();
		this.m3u8Url = m3u8Url;
		this.downloadDir = downloadDir;
		this.videoName = videoName;
	}

	public static void main(String[] args) {
//		new WM3u8Downloader(args[0], args[1], args[2]).download();
		
		new WM3u8Downloader("https://aut-bask-kon.mushroomtrack.com/hls/aFIUU-li-CIs-Wos4ZMIOg/1624127780/16000/16568/16568.m3u8","D:\\","TEST1.mp4").download();
	}

	public void download() {
		M3u8DownloadFactory.M3u8Download m3u8Download = M3u8DownloadFactory.getInstance(m3u8Url);
		// 設置生成目錄
		m3u8Download.setDir(downloadDir);
		// 設置視頻名稱
		m3u8Download.setFileName(videoName);
		// 設置執行緒數
		m3u8Download.setThreadCount(500);
		// 設置重試次數
		m3u8Download.setRetryCount(100);
		// 設置連接逾時時間（單位：毫秒）
		m3u8Download.setTimeoutMillisecond(10000L);
		// 設置監聽器間隔（單位：毫秒）
		m3u8Download.setInterval(500L);
		// 添加監聽器
		m3u8Download.addListener(new DownloadListener() {
			@Override
			public void start() {
				WLog.info("開始下載！");
			}

			@Override
			public void process(String downloadUrl, int finished, int sum, float percent) {
				if (sum % 100 == 0) {
//					System.out.println(
//							"下載網址：" + downloadUrl + "\t已下載" + finished + "個\t一共" + sum + "個\t已完成" + percent + "%");
					WLog.info("下載網址：" + downloadUrl + "\t已下載" + finished + "個\t一共" + sum + "個\t已完成" + percent + "%");
				}
			}

			@Override
			public void speed(String speedPerSecond) {
//				System.out.println("下載速度：" + speedPerSecond);
			}

			@Override
			public void end() {
				WLog.info("下載完畢");
			}
		});
		// 開始下載
		m3u8Download.start();
	}
}

class M3u8DownloadFactory {

	private static M3u8Download m3u8Download;

	/**
	 *
	 * 解決java不支援AES/CBC/PKCS7Padding模式解密
	 *
	 */
	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	public static class M3u8Download {
		// 要下載的m3u8連結
		private final String DOWNLOADURL;
		// 優化記憶體佔用
		private static final BlockingQueue<byte[]> BLOCKING_QUEUE = new LinkedBlockingQueue<>();
		// 執行緒數
		private int threadCount = 1;
		// 重試次數
		private int retryCount = 30;
		// 連結連接逾時時間（單位：毫秒）
		private long timeoutMillisecond = 1000L;
		// 合併後的檔存儲目錄
		private String dir;
		// 下載檔暫存路徑
		private String tempDir;
		// 合併後的視頻檔案名稱
		private String fileName;
		// 已完成ts片段個數
		private int finishedCount = 0;
		// 解密演算法名稱
		private String method;
		// 金鑰
		private String key = "";
		// 金鑰位元組
		private byte[] keyBytes = new byte[16];
		// key是否為位元組
		private boolean isByte = false;
		// IV
		private String iv = "";
		// 所有ts片段下載連結
		private Set<String> tsSet = new LinkedHashSet<>();
		// 解密後的片段
		private Set<File> finishedFiles = new ConcurrentSkipListSet<>(
				Comparator.comparingInt(o -> Integer.parseInt(o.getName().replace(".xyz", "").replace(".xy", ""))));
		// 已經下載的檔大小
		private BigDecimal downloadBytes = new BigDecimal(0);
		// 監聽間隔
		private volatile long interval = 0L;
		// 自訂請求頭
		private Map<String, String> requestHeaderMap = new HashMap<>();
		// 監聽事件
		private Set<DownloadListener> listenerSet = new HashSet<>(5);

		// 開始下載視頻
		public void start() {
			setThreadCount(30);
			checkField();
			String tsUrl = getTsUrl();
			if (StringUtils.isEmpty(tsUrl))
				WLog.info("不需要解密");
			WLog.info("tsUrl : " + tsUrl);
			startDownload();
		}

		// 下載視頻
		private void startDownload() {
			// 執行緒池
			final ExecutorService fixedThreadPool = Executors.newFixedThreadPool(threadCount);
			int i = 0;
			// 將檔案下載至暫存目錄
			tempDir += File.separator + UUID.randomUUID();
			WDictionary.mergeDir(tempDir);
			WLog.info("暫存下載路徑 : " + tempDir);

			// 執行多執行緒下載
			for (String s : tsSet) {
				i++;
				fixedThreadPool.execute(downloadTsFile(s, i));
//				if(finishedFiles.size()%10==0) {
//					System.out.println(
//							"已完成" + finishedFiles.size() + "個檔案，總共" + tsSet.size() 
//							+ "個，進度" + (finishedFiles.size()/tsSet.size())*100+"%");
//				}

			}
			fixedThreadPool.shutdown();
//			startListener(fixedThreadPool);
//			int consume = 0;
//			// 輪詢是否下載成功
			while (!fixedThreadPool.isTerminated()) {
//				try {
//					consume++;
//					BigDecimal bigDecimal = new BigDecimal(downloadBytes.toString());
//					Thread.sleep(5000L);
//					System.out
//							.println(
//									"已用時" + consume + "秒！\t下載速度："
//											+ StringUtils.convertToDownloadSpeed(
//													new BigDecimal(downloadBytes.toString()).subtract(bigDecimal), 3)
//											+ "/s");
//					WLog.debug("\t已完成" + finishedCount + "個，還剩" + (tsSet.size() - finishedCount) + "個！");
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
			}
			WLog.info("下載完成，正在合併檔！共" + finishedFiles.size() + "個！"
					+ StringUtils.convertToDownloadSpeed(downloadBytes, 3));
			// 開始合併視頻
			mergeTs();
			// 將檔案由暫存路徑移至下載目標路徑
			WDictionary.move(this.tempDir + File.separator + this.fileName, this.dir + File.separator + this.fileName);
			// 刪除暫存目錄
			WDictionary.delete(this.tempDir);
			WLog.info("視頻合併完成，歡迎使用!");
		}

		private void startListener(ExecutorService fixedThreadPool) {
			new Thread(() -> {
				for (DownloadListener downloadListener : listenerSet)
					downloadListener.start();
				// 輪詢是否下載成功
				while (!fixedThreadPool.isTerminated()) {
					try {
						Thread.sleep(interval);
						for (DownloadListener downloadListener : listenerSet)
							downloadListener.process(DOWNLOADURL, finishedCount, tsSet.size(),
									new BigDecimal(finishedCount)
											.divide(new BigDecimal(tsSet.size()), 4, BigDecimal.ROUND_HALF_UP)
											.multiply(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP)
											.floatValue());
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				for (DownloadListener downloadListener : listenerSet)
					downloadListener.end();
			}).start();
			new Thread(() -> {
				while (!fixedThreadPool.isTerminated()) {
					try {
						BigDecimal bigDecimal = new BigDecimal(downloadBytes.toString());
						Thread.sleep(1000L);
						for (DownloadListener downloadListener : listenerSet)
							downloadListener.speed(StringUtils.convertToDownloadSpeed(
									new BigDecimal(downloadBytes.toString()).subtract(bigDecimal), 3) + "/s");
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}).start();
		}

		/**
		 * 合併下載好的ts片段
		 */
		private void mergeTs() {
			try {
				File[] finishedFiles = new File(tempDir).listFiles(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						// TODO Auto-generated method stub
						return name.contains("xyz");
					}
				});
				List<File> finishFileList = Arrays.asList(finishedFiles);
				finishFileList.sort(new Comparator<File>() {

					@Override
					public int compare(File f1, File f2) {
						// TODO Auto-generated method stub
						return Integer.valueOf(f1.getName().replace(".xyz", ""))
								.compareTo(Integer.valueOf(f2.getName().replace(".xyz", "")));
					}
				});
				File file = new File(tempDir + File.separator + fileName);
				System.gc();
				if (file.exists())
					file.delete();
				else
					file.createNewFile();
				FileOutputStream fileOutputStream = new FileOutputStream(file);
				byte[] b = new byte[4096];
				for (File f : finishFileList) {
					FileInputStream fileInputStream = new FileInputStream(f);
					int len;
					while ((len = fileInputStream.read(b)) != -1) {
						fileOutputStream.write(b, 0, len);
					}
					fileInputStream.close();
					fileOutputStream.flush();
				}
				fileOutputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		/**
		 * 刪除下載好的片段
		 */
		private void deleteFiles() {
			File file = new File(tempDir);
			for (File f : file.listFiles()) {
				if (f.getName().endsWith(".xy") || f.getName().endsWith(".xyz"))
					f.delete();
			}
		}

		/**
		 * 開啟下載執行緒
		 *
		 * @param urls ts片段連結
		 * @param i    ts片段序號
		 * @return 執行緒
		 */
		private Thread downloadTsFile(String urls, int i) {
			return new Thread(() -> {
				int count = 1;
				HttpURLConnection httpURLConnection = null;
				// xy為未解密的ts片段，如果存在，則刪除
				File file2 = new File(tempDir + System.getProperty("file.separator") + i + ".xy");
				if (file2.exists())
					file2.delete();
				OutputStream outputStream = null;
				InputStream inputStream1 = null;
				FileOutputStream outputStream1 = null;
				byte[] bytes;
				try {
					bytes = BLOCKING_QUEUE.take();
				} catch (InterruptedException e) {
					bytes = new byte[40960];
				}
				// 重試次數判斷
				while (count <= retryCount) {
					try {
						// 模擬http請求獲取ts片段檔
						URL url = new URL(urls);
						httpURLConnection = (HttpURLConnection) url.openConnection();
						httpURLConnection.setConnectTimeout((int) timeoutMillisecond);
						for (Map.Entry<String, String> entry : requestHeaderMap.entrySet())
							httpURLConnection.addRequestProperty(entry.getKey(), entry.getValue());
						httpURLConnection.setUseCaches(false);
						httpURLConnection.setReadTimeout((int) timeoutMillisecond);
						httpURLConnection.setDoInput(true);
						InputStream inputStream = httpURLConnection.getInputStream();
						try {
							outputStream = new FileOutputStream(file2);
						} catch (FileNotFoundException e) {
							e.printStackTrace();
							continue;
						}
						int len;
						// 將未解密的ts片段寫入文件
						while ((len = inputStream.read(bytes)) != -1) {
							outputStream.write(bytes, 0, len);
							synchronized (this) {
								downloadBytes = downloadBytes.add(new BigDecimal(len));
							}
						}
						outputStream.flush();
						inputStream.close();
						finishedFiles.add(file2);
						inputStream1 = new FileInputStream(file2);
						int available = inputStream1.available();
						if (bytes.length < available)
							bytes = new byte[available];
						inputStream1.read(bytes);
						File file = new File(tempDir + System.getProperty("file.separator") + i + ".xyz");
						outputStream1 = new FileOutputStream(file);
						// 開始解密ts片段，這裡我們把ts尾碼改為了xyz，改不改都一樣
						byte[] decrypt = decrypt(bytes, available, key, iv, method);
						if (decrypt == null)
							outputStream1.write(bytes, 0, available);
						else
							outputStream1.write(decrypt);
						finishedFiles.add(file);
						break;
					} catch (Exception e) {
						if (e instanceof InvalidKeyException || e instanceof InvalidAlgorithmParameterException) {
							WLog.info("解密失敗！");
							break;
						}
						WLog.debug("第" + count + "獲取連結重試！\t" + urls);
						count++;
//                        e.printStackTrace();
					} finally {
						try {
							if (inputStream1 != null)
								inputStream1.close();
							if (outputStream1 != null)
								outputStream1.close();
							if (outputStream != null)
								outputStream.close();
							BLOCKING_QUEUE.put(bytes);
						} catch (IOException | InterruptedException e) {
							e.printStackTrace();
						}
						if (httpURLConnection != null) {
							httpURLConnection.disconnect();
						}
					}
				}
				if (count > retryCount)
					// 自訂異常
					throw new IllegalArgumentException("連接逾時！");
				finishedCount++;
				WLog.debug(urls + "下載完畢！\t已完成" + finishedCount + "個，還剩" + (tsSet.size() - finishedCount) + "個！");
			});
		}

		// 獲取m3u8下載路徑
		private String getTsUrl() {
			StringBuilder m3u8Content = getUrlContent(DOWNLOADURL, false);

			// 判斷是否是m3u8連結
			if (!m3u8Content.toString().contains("#EXTM3U"))
				throw new IllegalArgumentException(DOWNLOADURL + "不是m3u8連結！");

			String[] m3u8ContentAr = m3u8Content.toString().split("\\n");
			String keyUrl = "";
			boolean isKey = false;
			for (String m3u8RowContent : m3u8ContentAr) {
				// 如果含有此欄位，則說明只有一層m3u8連結
				if (m3u8RowContent.contains("#EXT-X-KEY") || m3u8RowContent.contains("#EXTINF")) {
					isKey = true;
					keyUrl = DOWNLOADURL;
					break;
				}
				// 如果含有此欄位，則說明ts片段連結需要從第二個m3u8連結獲取
				if (m3u8RowContent.contains(".m3u8")) {
					if (StringUtils.isUrl(m3u8RowContent))
						return m3u8RowContent;
					String relativeUrl = DOWNLOADURL.substring(0, DOWNLOADURL.lastIndexOf("/") + 1);
					keyUrl = relativeUrl + m3u8RowContent;
					break;
				}
			}
			if (StringUtils.isEmpty(keyUrl))
				throw new IllegalArgumentException("未發現key連結！");
			// 獲取金鑰
			String key1 = isKey ? getKey(keyUrl, m3u8Content) : getKey(keyUrl, null);
			if (StringUtils.isNotEmpty(key1)) {
				key = key1;
			} else {
				key = null;
			}
			return key;
		}

		/**
		 * 獲取ts解密的金鑰，並把ts片段加入set集合
		 *
		 * @param url     金鑰連結，如果無金鑰的m3u8，則此欄位可為空
		 * @param content 內容，如果有金鑰，則此欄位可以為空
		 * @return ts是否需要解密，null為不解密
		 */
		private String getKey(String url, StringBuilder content) {
			StringBuilder urlContent;
			if (content == null || StringUtils.isEmpty(content.toString()))
				urlContent = getUrlContent(url, false);
			else
				urlContent = content;
			if (!urlContent.toString().contains("#EXTM3U"))
				throw new IllegalArgumentException(DOWNLOADURL + "不是m3u8連結！");
			String[] split = urlContent.toString().split("\\n");
			for (String s : split) {
				// 如果含有此欄位，則獲取加密演算法以及獲取金鑰的連結
				if (s.contains("EXT-X-KEY")) {
					String[] split1 = s.split(",");
					for (String s1 : split1) {
						if (s1.contains("METHOD")) {
							method = s1.split("=", 2)[1];
							continue;
						}
						if (s1.contains("URI")) {
							key = s1.split("=", 2)[1];
							continue;
						}
						if (s1.contains("IV"))
							iv = s1.split("=", 2)[1];
					}
				}
			}
			String relativeUrl = url.substring(0, url.lastIndexOf("/") + 1);
			// 將ts片段連結加入set集合
			for (int i = 0; i < split.length; i++) {
				String s = split[i];
				if (s.contains("#EXTINF")) {
					String s1 = split[++i];
					tsSet.add(StringUtils.isUrl(s1) ? s1 : relativeUrl + s1);
				}
			}
			if (!StringUtils.isEmpty(key)) {
				key = key.replace("\"", "");
				return getUrlContent(StringUtils.isUrl(key) ? key : relativeUrl + key, true).toString()
						.replaceAll("\\s+", "");
			}
			return null;
		}

		/**
		 * 類比http請求獲取內容
		 *
		 * @param urls  http連結
		 * @param isKey 這個url連結是否用於獲取key
		 * @return 內容
		 */
		private StringBuilder getUrlContent(String urls, boolean isKey) {
			int count = 1;
			HttpURLConnection httpURLConnection = null;
			StringBuilder content = new StringBuilder();
			while (count <= retryCount) {
				try {
					URL url = new URL(urls);
					httpURLConnection = (HttpURLConnection) url.openConnection();
					httpURLConnection.setConnectTimeout((int) timeoutMillisecond);
					httpURLConnection.setReadTimeout((int) timeoutMillisecond);
					httpURLConnection.setUseCaches(false);
					httpURLConnection.setDoInput(true);
					for (Map.Entry<String, String> entry : requestHeaderMap.entrySet())
						httpURLConnection.addRequestProperty(entry.getKey(), entry.getValue());
					String line;
					InputStream inputStream = httpURLConnection.getInputStream();
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
					if (isKey) {
						byte[] bytes = new byte[128];
						int len;
						len = inputStream.read(bytes);
						isByte = true;
						if (len == 1 << 4) {
							keyBytes = Arrays.copyOf(bytes, 16);
							content.append("isByte");
						} else
							content.append(new String(Arrays.copyOf(bytes, len)));
						return content;
					}
					while ((line = bufferedReader.readLine()) != null)
						content.append(line).append("\n");
					bufferedReader.close();
					inputStream.close();
//					System.out.println(content);
					break;
				} catch (Exception e) {
//					System.out.println("第" + count + "獲取連結重試！\t" + urls);
					e.printStackTrace();
					WLog.debug("第" + count + "獲取連結重試！\t" + urls);
					count++;
//                    e.printStackTrace();
				} finally {
					if (httpURLConnection != null) {
						httpURLConnection.disconnect();
					}
				}
			}
			if (count > retryCount)
				throw new IllegalArgumentException("連接逾時！");
			return content;
		}

		/**
		 * 解密ts
		 *
		 * @param sSrc   ts檔位元組陣列
		 * @param length
		 * @param sKey   金鑰
		 * @return 解密後的位元組陣列
		 */
		private byte[] decrypt(byte[] sSrc, int length, String sKey, String iv, String method) throws Exception {
			if (StringUtils.isNotEmpty(method) && !method.contains("AES"))
				throw new IllegalArgumentException("未知的演算法！");
			// 判斷Key是否正確
			if (StringUtils.isEmpty(sKey))
				return null;
			// 判斷Key是否為16位
			if (sKey.length() != 16 && !isByte) {
				throw new IllegalArgumentException("Key長度不是16位！");
			}
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
			SecretKeySpec keySpec = new SecretKeySpec(isByte ? keyBytes : sKey.getBytes(StandardCharsets.UTF_8), "AES");
			byte[] ivByte;
			if (iv.startsWith("0x"))
				ivByte = StringUtils.hexStringToByteArray(iv.substring(2));
			else
				ivByte = iv.getBytes();
			if (ivByte.length != 16)
				ivByte = new byte[16];
			// 如果m3u8有IV標籤，那麼IvParameterSpec構造函數就把IV標籤後的內容轉成位元組陣列傳進去
			AlgorithmParameterSpec paramSpec = new IvParameterSpec(ivByte);
			cipher.init(Cipher.DECRYPT_MODE, keySpec, paramSpec);
			return cipher.doFinal(sSrc, 0, length);
		}

		/**
		 * 欄位校驗
		 */
		private void checkField() {
			if ("m3u8".compareTo(MediaFormatUtil.getMediaFormat(DOWNLOADURL)) != 0)
				throw new IllegalArgumentException(DOWNLOADURL + "不是一個完整m3u8連結！");
			if (threadCount <= 0)
				throw new IllegalArgumentException("同時下載執行緒數只能大於0！");
			if (retryCount < 0)
				throw new IllegalArgumentException("重試次數不能小於0！");
			if (timeoutMillisecond < 0)
				throw new IllegalArgumentException("超時時間不能小於0！");
			if (StringUtils.isEmpty(this.dir))
				throw new IllegalArgumentException("視頻存儲目錄不能為空！");
			if (StringUtils.isEmpty(fileName))
				throw new IllegalArgumentException("視頻名稱不能為空！");
			finishedCount = 0;
			method = "";
			key = "";
			isByte = false;
			iv = "";
			tsSet.clear();
			finishedFiles.clear();
			downloadBytes = new BigDecimal(0);
			this.tempDir = this.dir;
			this.fileName = WString.rmStr(this.fileName, " ", "\t");
		}

		public String getDOWNLOADURL() {
			return DOWNLOADURL;
		}

		public int getThreadCount() {
			return threadCount;
		}

		public void setThreadCount(int threadCount) {
			if (BLOCKING_QUEUE.size() < threadCount) {
				for (int i = BLOCKING_QUEUE.size(); i < threadCount * 1.15F; i++) {
					try {
						BLOCKING_QUEUE.put(new byte[40960]);
					} catch (InterruptedException ignored) {
					}
				}
			}
			this.threadCount = threadCount;
		}

		public int getRetryCount() {
			return retryCount;
		}

		public void setRetryCount(int retryCount) {
			this.retryCount = retryCount;
		}

		public long getTimeoutMillisecond() {
			return timeoutMillisecond;
		}

		public void setTimeoutMillisecond(long timeoutMillisecond) {
			this.timeoutMillisecond = timeoutMillisecond;
		}

		public String getDir() {
			return tempDir;
		}

		public void setDir(String dir) {
			this.dir = dir;
		}

		public String getFileName() {
			return fileName;
		}

		public void setFileName(String fileName) {
			this.fileName = fileName;
		}

		public int getFinishedCount() {
			return finishedCount;
		}

		public Map<String, String> getRequestHeaderMap() {
			return requestHeaderMap;
		}

		public void addRequestHeaderMap(Map<String, String> requestHeaderMap) {
			this.requestHeaderMap.putAll(requestHeaderMap);
		}

		public void setInterval(long interval) {
			this.interval = interval;
		}

		public void addListener(DownloadListener downloadListener) {
			listenerSet.add(downloadListener);
		}

		private M3u8Download(String DOWNLOADURL) {
			this.DOWNLOADURL = DOWNLOADURL;
			requestHeaderMap.put("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36");
		}

		public String getTempDir() {
			return tempDir;
		}

		public void setTempDir(String tempDir) {
			this.tempDir = tempDir;
		}
	}

	/**
	 * 獲取實例
	 *
	 * @param downloadUrl 要下載的連結
	 * @return 返回m3u8下載實例
	 */
	public static M3u8Download getInstance(String downloadUrl) {
		if (m3u8Download == null) {
			synchronized (M3u8Download.class) {
				if (m3u8Download == null)
					m3u8Download = new M3u8Download(downloadUrl);
			}
		}
		return m3u8Download;
	}

	public static void destroied() {
		m3u8Download = null;
	}

}