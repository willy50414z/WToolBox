package com.willy.net.mail;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.activation.FileDataSource;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.plexus.util.FileUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresolver.StringTemplateResolver;

import com.willy.file.dictionary.WDictionary;
import com.willy.util.string.WString;

@Service
@ConditionalOnProperty(name = "spring.mail.host")
public class WMail {
	private String sender;
	private String sendFrom;
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private TemplateEngine mailTemplEngine;
	private HashMap<String, Object> templateContextMap;
	private Date sendDate;// 寄件日期
	private List<String> toMailAddrList = new ArrayList<String>();
	private List<String> ccMailAddrList = new ArrayList<String>();
	private List<String> bccMailAddrList = new ArrayList<String>();
	private HashMap<MsgType, String> content = new HashMap<MsgType, String>();// 信件內容
	private List<FileDataSource> attachmentList = new ArrayList<FileDataSource>();

	public WMail() {
		super();
	}

	public void send(String subject, String toMailAddr) throws UnsupportedEncodingException, MessagingException {
		this.send(subject, Arrays.asList(toMailAddr));
	}
	public void send(String subject, List<String> toMailAddrList)
			throws MessagingException, UnsupportedEncodingException {
		clearRecver();

		this.toMailAddrList = toMailAddrList;

		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
		setMailHeader(helper);
		helper.setSubject(subject);
		addReceiver(helper);
		setMailContent(helper);
		mailSender.send(mimeMessage);
	}

	private void clearRecver() {
		if(this.toMailAddrList != null) {
			this.toMailAddrList.clear();
		}
		if(this.ccMailAddrList != null) {
			this.ccMailAddrList.clear();
		}
		if(this.toMailAddrList != null) {
			this.bccMailAddrList.clear();
		}
	}

	private void setMailHeader(MimeMessageHelper helper) throws MessagingException, UnsupportedEncodingException {
		if (StringUtils.isEmpty(sender)) {
			throw new IllegalArgumentException("未設定寄件人,spring.mail.sender");
		}
		if (StringUtils.isEmpty(sendFrom)) {
			throw new IllegalArgumentException("未設定寄件人郵件地址,spring.mail.sendfrom");
		}

		helper.setFrom(new InternetAddress(sendFrom, sender)); // 發件人

		helper.setSentDate(this.getSendDate() == null ? new Date() : this.getSendDate());
	}

	private MimeMessageHelper addReceiver(MimeMessageHelper helper) throws AddressException, MessagingException {
		for (String mailAddr : toMailAddrList) {
			helper.setTo(InternetAddress.parse(mailAddr));
		}
		for (String mailAddr : ccMailAddrList) {
			helper.setCc(InternetAddress.parse(mailAddr));
		}
		for (String mailAddr : bccMailAddrList) {
			helper.setBcc(InternetAddress.parse(mailAddr));
		}
		return helper;
	}

	private void setMailContent(MimeMessageHelper helper) throws MessagingException {
		// 信件內容
		if (this.content.size() > 0) {
			MsgType msgType = this.content.keySet().iterator().next();
			if (MsgType.Html.equals(msgType)) {
				helper.setText(this.content.get(msgType), true);
			} else if (MsgType.Template.equals(msgType)) {
				Context context = new Context();
				context.setVariables(this.templateContextMap);
				helper.setText(mailTemplEngine.process(this.content.get(MsgType.Template), context));
			} else {
				// String
				helper.setText(this.content.get(msgType), false);
			}

		}

		// 附件
		for (FileDataSource attachment : attachmentList) {
			helper.addAttachment(attachment.getName(), attachment);
		}
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public void addTo(String mailAddr) {
		toMailAddrList.add(mailAddr);
	}

	public void addCC(String mailAddr) {
		ccMailAddrList.add(mailAddr);
	}

	public void addBCC(String mailAddr) {
		bccMailAddrList.add(mailAddr);
	}

	public void addAttachment(String filePath) {
		attachmentList.add(new FileDataSource(filePath));
	}

	public void addAttachment(File file) {
		attachmentList.add(new FileDataSource(file.getAbsolutePath()));
	}

	public void setHtmlListContent(String msgTplID, List<List<String>> listData, HashMap<String, String> mapData)
			throws IOException {
		StringBuffer listHtml = new StringBuffer();
		listData.forEach(ldata -> {
			listHtml.append("<tr>");
			ldata.forEach(data -> listHtml.append("<td>").append(data).append("</td>"));
			listHtml.append("</tr>");
		});
		StringBuffer htmlTpl = new StringBuffer();
		
		try (InputStream in = getClass().getResourceAsStream("/mail/" + msgTplID + ".html");
				BufferedReader br = new BufferedReader(new InputStreamReader(in))) {
			String strLine;
			while ((strLine = br.readLine()) != null) {// 將CSV檔字串一列一列讀入並存起來直到沒有列為止
				htmlTpl.append(strLine);
			}
		}
		JSONObject json = new JSONObject();
		json.put("listHtml", listHtml.toString());
		if(mapData != null) {
			mapData.forEach((k, v) -> {
				json.put(k, v);
			});
		}
		this.content.put(MsgType.Html, WString.parseParamsToStrByJson(htmlTpl.toString(), json));
	}

	public void setContent(MsgType msgType, String content) {
		this.content.put(msgType, content);
	}

	public HashMap<String, Object> getTemplateContextMap() {
		return templateContextMap;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getSendFrom() {
		return sendFrom;
	}

	public void setSendFrom(String sendFrom) {
		this.sendFrom = sendFrom;
	}

	// Thymeleaf 設定
	@Bean
	@Primary
	public TemplateEngine emailTemplateEngine() {
		final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.addTemplateResolver(textTemplateResolver());
		templateEngine.addTemplateResolver(htmlTemplateResolver());
		templateEngine.addTemplateResolver(stringTemplateResolver());
		templateEngine.setTemplateEngineMessageSource(emailMessageSource());
		return templateEngine;
	}

	public ResourceBundleMessageSource emailMessageSource() {
		final ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("mail/MailMessages");
		return messageSource;
	}

	private ITemplateResolver textTemplateResolver() {
		final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setOrder(Integer.valueOf(1));
		templateResolver.setResolvablePatterns(Collections.singleton("text/*"));
		templateResolver.setPrefix("/mailTempl/");
		templateResolver.setSuffix(".txt");
		templateResolver.setTemplateMode(TemplateMode.TEXT);
		templateResolver.setCharacterEncoding("UTF-8");
		templateResolver.setCacheable(false);
		return templateResolver;
	}

	private ITemplateResolver htmlTemplateResolver() {
		final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setOrder(Integer.valueOf(2));
		templateResolver.setResolvablePatterns(Collections.singleton("html/*"));
		templateResolver.setPrefix("/mailTempl/");
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode(TemplateMode.HTML);
		templateResolver.setCharacterEncoding("UTF-8");
		templateResolver.setCacheable(false);
		return templateResolver;
	}

	private ITemplateResolver stringTemplateResolver() {
		final StringTemplateResolver templateResolver = new StringTemplateResolver();
		templateResolver.setOrder(Integer.valueOf(3));
		templateResolver.setTemplateMode("HTML5");
		templateResolver.setCacheable(false);
		return templateResolver;
	}
}
