package com.willy.net.web;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WHtmlParser {
	private static String separator="@@";
	public static List<List<String[]>> parseAllTablesToList(String html){
		List<List<String[]>> tablesInfoList;
		tablesInfoList = new ArrayList<List<String[]>>();
		Document doc = Jsoup.parse(html);
		Elements elements = doc.getElementsByTag("table");
		for(Element e: elements) {
			tablesInfoList.add(parseTableToArrayList(e));
		}
		return tablesInfoList;
	}
	public static List<String[]> parseTableToArrayList(Element e){
		//因不確定array範圍，先組成List<String>
		StringBuffer rowStrBuf=new StringBuffer();
		List<String[]> rowStrList=new ArrayList<String[]>();
		//逐列抓取
		Elements trElements=e.getElementsByTag("tr");
		//抓取th
		for(Element tr :trElements) {
			Elements thElements=tr.getElementsByTag("th");
			for(Element thElement : thElements) {
				rowStrBuf.append(thElement.text()).append(separator);
			}
			rowStrBuf.delete(Math.max(0,(rowStrBuf.length()-separator.length())), rowStrBuf.length());
			if(rowStrBuf.length()>0) {
				rowStrList.add(rowStrBuf.toString().split(separator));
			}
			rowStrBuf.delete(0, rowStrBuf.length());
		}
		//抓取td
		for(Element tr :trElements) {
			Elements tdElements=tr.getElementsByTag("td");
			for(Element tdElement : tdElements) {
				rowStrBuf.append(tdElement.text()).append(separator);
			}
			rowStrBuf.delete(Math.max(0,(rowStrBuf.length()-separator.length())), rowStrBuf.length());
			if(rowStrBuf.length()>0) {
				rowStrList.add(rowStrBuf.toString().split(separator));
			}
			rowStrBuf.delete(0, rowStrBuf.length());
		}
		return rowStrList;
	}
}
