package com.willy.net.web;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.willy.util.config.WConfig;
import com.willy.util.constant.WConstant;

/**
 * @author Willy
 * @version 1.0
 * @created 17-六月-2018 下午 03:01:19
 */
@Service
public class WSelenium {
	private WebDriver tempBrowser;
	private int waitElementTimeoutSecond=30;
	@Value("${selenium.driver.dir}")
	private String webDriverFolder;
	public static final String driver_IE = "IE";
	public static final String driver_Chrome = "Chrome";
	public static final String driver_PhantomJS = "PhantomJS";
	public enum WebDriverType{
		IE,Chrome,PhantomJS;
	}
	public WSelenium() {
		super();
	}
	public WebDriver getBrowser(WebDriverType webDriverType) throws FileNotFoundException, InterruptedException {
		checkDriverExists();
		switch (webDriverType) {
		case IE:
			System.setProperty("webdriver.ie.driver", webDriverFolder + "IEDriverServer.exe");
			tempBrowser=new InternetExplorerDriver();
			break;
		case Chrome:
			System.setProperty("webdriver.chrome.driver", webDriverFolder + "chromedriver.exe");
			ChromeOptions option = new ChromeOptions();
			option.setBinary("C:/Program Files/Google/Chrome/Application/chrome.exe");
			tempBrowser=new ChromeDriver();
			break;
		case PhantomJS:
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setJavascriptEnabled(true);
			caps.setCapability("takesScreenshot", true);
			caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
					webDriverFolder + "phantomjs.exe");
			tempBrowser=new PhantomJSDriver(caps);
			break;
		default:
			System.setProperty("webdriver.chrome.driver", webDriverFolder + "chromedriver.exe");
			tempBrowser=new ChromeDriver();
			break;
		}
		tempBrowser.manage().timeouts().implicitlyWait(waitElementTimeoutSecond, TimeUnit.SECONDS);
		Thread.sleep(3000);
		return tempBrowser;
	}
	
	public void get(ChromeDriver chromeBrowser, String url) {
		StringBuffer postJS = new StringBuffer();

		postJS.append(
				"var form = document.createElement(\"form\");\r\n" + "    form.setAttribute(\"method\", \"get\");\r\n"
						+ "    form.setAttribute(\"action\", \"" + url + "\");\r\n");
		postJS.append("document.body.appendChild(form);\r\n" + "form.submit();");
		System.out.println(postJS.toString());
		chromeBrowser.executeScript(postJS.toString(), "");
	}
	public void post(ChromeDriver chromeBrowser, String action, Map<String, String> postParaMap) {
		StringBuffer postJS = new StringBuffer();

		postJS.append(
				"var form = document.createElement(\"form\");\r\n" + "    form.setAttribute(\"method\", \"post\");\r\n"
						+ "    form.setAttribute(\"action\", \"" + action + "\");\r\n");

		postJS.append(this.getPostParJS(postParaMap));

		postJS.append("document.body.appendChild(form);\r\n" + "form.submit();");
		System.out.println(postJS.toString());
		chromeBrowser.executeScript(postJS.toString(), "");
	}

	
	 public Object exeJS(WebDriver driver,String script) {
		 JavascriptExecutor js=(JavascriptExecutor) driver;
		 Object obj = js.executeScript(script);
         return obj;
     }
	 public WebElement findElementInFrame(WebDriver driver,By by) {//NoSuchFrameException
		 WebDriver frameDriver;
		 if(this.exists(driver, by)) {
			 System.out.println("第一層就找到了");
			 return driver.findElement(by);
		 }
		for(int frameIndex=0;frameIndex<10;frameIndex++) {
			try {
				System.out.println("0. 嘗試進入frame一層"+frameIndex);
				frameDriver = driver.switchTo().frame(frameIndex);
				System.out.println("1. 進入frame一層"+frameIndex);
				if(this.exists(frameDriver, by)) {
					//第一層frame抓到就返回
					return driver.findElement(by);
				}else {
					//第一層frame抓不到就在往裡面抓
					WebElement e =findElementInFrame(frameDriver,by); 
					if(e==null) {
						continue;
					}else {
						return e;
					}
				}
			}catch(NoSuchFrameException e) {
				driver.switchTo().parentFrame();
				System.out.println("3. 跳出");
				break;//找不到frame
			}
		}
		return null;
	 }
	// 判斷元素是否存在
	public boolean isExistByID(WebDriver browser, String elementId) {
		return browser.findElements(By.id(elementId)).size() > 0;
	}
	public boolean exists(WebDriver browser, By by) {
		return browser.findElements(by).size() > 0;
	}

	private String getPostParJS(Map postParam) {
		StringBuffer postParJS = new StringBuffer();
		for (String key : (Set<String>) postParam.keySet()) {
			postParJS.append("var hiddenField = document.createElement(\"input\");\r\n");
			postParJS.append("hiddenField.setAttribute(\"type\", \"hidden\");\r\n");
			postParJS.append("hiddenField.setAttribute(\"name\", \"" + key + "\");\r\n");
			postParJS.append("hiddenField.setAttribute(\"value\", \"" + postParam.get(key) + "\");\r\n");
			postParJS.append("form.appendChild(hiddenField);\r\n");
		}

		return postParJS.toString();
	}
	public void click(WebDriver browser, By by) {
		browser.findElement(by).click();
	}
	public Object clickWithAlert(WebDriver browser, By by) {
		Object resultObj= null;
		if(browser instanceof PhantomJSDriver) {
			String pageTitle = ((PhantomJSDriver) browser).executePhantomJS("return document.title;").toString();
			((PhantomJSDriver) browser).executePhantomJS("var page = this;"
					+ "page.onAlert = function(msg){"
					+ "document.title = msg;"
					+ "console.log(msg);"
					+ "}");
			browser.findElement(by).click();
			resultObj = ((PhantomJSDriver) browser).executePhantomJS("return document.title;");
			((PhantomJSDriver) browser).executePhantomJS("document.title = '"+pageTitle+"';");
		}else {
			browser.findElement(by).click();
			Alert alert = browser.switchTo().alert();
			resultObj = alert.getText();
			alert.accept();
		}
		return resultObj;
	}
	public WebElement findElementById(WebDriver browser, String id) {
		return browser.findElement(By.id(id));
	}

	public WebElement findElementByTagName(WebDriver browser, String tagName) {
		return browser.findElement(By.tagName(tagName));
	}

	public List<WebElement> findElementsByTagName(WebDriver browser, String tagName) {
		return browser.findElements(By.tagName(tagName));
	}

	public WebElement getElementFromListByAttribute(List<WebElement> tempElement, String dataType, String value) {
		for (WebElement e : tempElement) {
			if (e.getAttribute(dataType).equals(value))
				return e;
		}
		return null;
	}
	public WebElement getElementFromListByText(List<WebElement> tempElement, String text) {
		for (WebElement e : tempElement) {
			if (e.getText().equals(text))
				return e;
		}
		return null;
	}
	public void clickRadioByValue(WebDriver browser, String radioName, String radioValue) {
//		Select select = new Select(selectElement);
//		select.selectByVisibleText(optionValue);
		List<WebElement> radioElements = browser.findElements(By.name(radioName));
		for(WebElement radioElement : radioElements) {
			if(radioElement.getAttribute("value").equals(radioValue)) {
				radioElement.click();
				break;
			}
		}
	}
	public void clickOptionByText(WebDriver browser, WebElement selectElement, String optionValue) {
		Select select = new Select(selectElement);
		select.selectByVisibleText(optionValue);
	}
	public void clickOptionByText(WebDriver browser, By by, String optionValue) {
		Select select = new Select(browser.findElement(by));
		select.selectByVisibleText(optionValue);
	}

	public void clickOptionByValue(WebDriver browser, By by, String optionValue) {
		Select select = new Select(browser.findElement(by));
		select.selectByValue(optionValue);
	}
	
	//send
	public void sendKey(WebDriver browser, By elementBy, String sentValue) {
		browser.findElement(elementBy).sendKeys(sentValue);
	}
	
	//switch
	public boolean switchToWindowByTitle(WebDriver driver,String windowTitle){  
	    boolean flag = false;  
	    try {  
	        String currentHandle = driver.getWindowHandle();  
	        Set<String> handles = driver.getWindowHandles();  
	        for (String s : handles) {  
	            if (s.equals(currentHandle))  
	                continue;  
	            else {  
	                driver.switchTo().window(s);  
	                if (driver.getTitle().contains(windowTitle)) {  
	                    flag = true;  
	                    break;  
	                } else  
	                    continue;  
	            }  
	        }  
	    } catch (NoSuchWindowException e) {  
	        System.out.printf("Window: " + windowTitle  + " cound not found!", e.fillInStackTrace());  
	        flag = false;  
	    }  
	    return flag;  
	}  
	
	public String getText(WebDriver driver,By elementIdentity) {
		return driver.findElement(elementIdentity).getText();
	}
	public String getValue(WebDriver driver,By elementIdentity) {
		return driver.findElement(elementIdentity).getAttribute("value");
	}
	private void checkDriverExists() throws FileNotFoundException {
		// 確認被執行檔位置
				File googleDriver = new File(webDriverFolder + "chromedriver.exe");
				File IEDriver = new File(webDriverFolder + "IEDriverServer.exe");
				File phantomJSDriver = new File(webDriverFolder + "phantomjs.exe");
				if (!googleDriver.exists() && !phantomJSDriver.exists() && !IEDriver.exists()) {
					throw new FileNotFoundException("please set web driver in selenium.driver.dir, " + webDriverFolder + " can't find any web driver");
				}
	}

	public void setWebDriverDir(String webDriverFolder) {
		this.webDriverFolder = webDriverFolder;
	}
	
}// end BrowserUtil