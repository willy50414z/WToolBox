package com.willy.net.web;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Set;

import org.jsoup.Connection;

import com.willy.file.dictionary.WDictionary;


public class WURLConnect {
	public void setUrl(String url) {
		this.url = url;
	}
	String url;
	Connection con;
	public void addHeader(HashMap<String, String> headers) {
		Set<String> headerKeys = headers.keySet();
		for (String key : headerKeys) {
			this.con.header(key, headers.get(key));
		}
	}

	public void addHeader(String key, String value) {
		this.con.header(key, value);
	}
	public static void download(String url, String toPath) throws IOException {
		HttpURLConnection con = null;
		BufferedInputStream is = null;
		FileOutputStream fos = null;
		try {
			if (!new File(toPath).exists()) {
				con = (HttpURLConnection) new URL(url).openConnection();
				con.setDoOutput(true);
				con.setUseCaches(false);
				con.setRequestMethod("GET");
				if (url.indexOf("taifex") > -1) {
					con = setTaifexHeader(con);
				} else {
					con = setHeader(con);
				}
				con.connect();
				is = new BufferedInputStream(con.getInputStream());
				WDictionary.mergeDir(toPath);
				fos = new FileOutputStream(new File(toPath));
				byte[] b = new byte[8192];
				int l = 0;
				while ((l = is.read(b)) != -1) {
					fos.write(b, 0, l);
				}
			}
		} catch (IOException e) {
			throw e;
		} finally {
			try {
				if (fos != null)
					fos.close();
				if (is != null)
					is.close();
				if (con != null) {
					con.disconnect();
				}
			} catch (Exception e) {
				throw e;
			}
		}
	}
//設定連線Header
	/**
	 * 設定預設header
	 * 
	 */
	public static Connection setHeader(Connection con) {
		con.header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
		con.header("Accept-Encoding", "gzip, deflate");
		con.header("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6");
		con.header("Cache-Control", "max-age=0");
		con.header("Connection", "keep-alive");
		con.header("Cookie","stock_user_uuid=04276be0-30d8-4c98-812d-a94e3bf9897d; stock_config=YTozOntzOjc6IlN0b2NrSWQiO047czozOiJ0YWciO2k6MztzOjQ6InR5cGUiO2k6Mjt9; stock_popup_personalnews=1");
		con.header("Host", "pchome.megatime.com.tw");
		con.header("Referer", "http://pchome.megatime.com.tw/group/");
		con.header("Upgrade-Insecure-Requests", "1");
		con.header("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36");
		con.data("IsCheck", "1");
		return con;
	}
	public static HttpURLConnection setHeader(HttpURLConnection con) {
		con.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
		con.setRequestProperty("Accept-Encoding", "gzip, deflate");
		con.setRequestProperty("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6");
		con.setRequestProperty("Cache-Control", "max-age=0");
		con.setRequestProperty("Connection", "keep-alive");
		con.setRequestProperty("Cookie","stock_user_uuid=04276be0-30d8-4c98-812d-a94e3bf9897d; stock_config=YTozOntzOjc6IlN0b2NrSWQiO047czozOiJ0YWciO2k6MztzOjQ6InR5cGUiO2k6Mjt9; stock_popup_personalnews=1");
		con.setRequestProperty("Host", "pchome.megatime.com.tw");
		con.setRequestProperty("Referer", "http://pchome.megatime.com.tw/group/");
		con.setRequestProperty("Upgrade-Insecure-Requests", "1");
		con.setRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36");
		return con;
	}
	/**
	 * 設定期交所專屬Header
	 * 
	 * */
	public static Connection setTaifexHeader(Connection con) {
		con.header("Accept",
				"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
		con.header("Accept-Encoding", "gzip, deflate");
		con.header("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6");
		con.header("Cache-this.control", "max-age=0");
		con.header("this.connection", "keep-alive");
		con.header("this.content-Length", "250");
		con.header("this.content-Type", "application/x-www-form-urlencoded");
		con.header("Cookie",
				"ASPSESSIONIDAADBDTQS=NHCPJKKAFEPAKNJKLKCMJFCH; AX-cookie-POOL_PORTAL=AGACBAKM; AX-cookie-POOL_PORTAL_web3=ADACBAKM");
		con.header("Host", "www.taifex.com.tw");
		con.header("Origin", "http://www.taifex.com.tw");
		con.header("Referer", "http://www.taifex.com.tw/chinese/3/3_1_2.asp");
		con.header("Upgrade-Insecure-Requests", "1");
		con.header("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");
		return con;
	}
	/**
	 * 設定期交所專屬Header
	 * 
	 * */
	public static HttpURLConnection setTaifexHeader(HttpURLConnection con) {
		con.setRequestProperty("Accept",
				"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
		con.setRequestProperty("Accept-Encoding", "gzip, deflate");
		con.setRequestProperty("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6");
		con.setRequestProperty("Cache-this.control", "max-age=0");
		con.setRequestProperty("this.connection", "keep-alive");
		con.setRequestProperty("this.content-Length", "250");
		con.setRequestProperty("this.content-Type", "application/x-www-form-urlencoded");
		con.setRequestProperty("Cookie",
				"ASPSESSIONIDAADBDTQS=NHCPJKKAFEPAKNJKLKCMJFCH; AX-cookie-POOL_PORTAL=AGACBAKM; AX-cookie-POOL_PORTAL_web3=ADACBAKM");
		con.setRequestProperty("Host", "www.taifex.com.tw");
		con.setRequestProperty("Origin", "http://www.taifex.com.tw");
		con.setRequestProperty("Referer", "http://www.taifex.com.tw/chinese/3/3_1_2.asp");
		con.setRequestProperty("Upgrade-Insecure-Requests", "1");
		con.setRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");
		return con;
	}
	/**
	 * 設定下載期貨交易資料所需data
	 * @param con
	 * @param dataDate
	 */
	public static Connection setData_FurturesDailyCSV(Connection con,String dataDate) {
		con.data("goday", "");
		con.data("DATA_DATE", "");
		con.data("DATA_DATE1", "");
		con.data("DATA_DATE_Y", "");
		con.data("DATA_DATE_M", "");
		con.data("DATA_DATE_D", "");
		con.data("DATA_DATE_Y1", "");
		con.data("DATA_DATE_M1", "");
		con.data("DATA_DATE_D1", "");
		con.data("syear", "");
		con.data("smonth", "");
		con.data("sday", "");
		con.data("syear1", "");
		con.data("smonth1", "");
		con.data("sday1", "");
		con.data("datestart", dataDate);
		con.data("dateend", dataDate);
		con.data("COMMODITY_ID", "all");
		con.data("commodity_id2t", "");
		con.data("his_year", "2017");
		return con;
	}
}
