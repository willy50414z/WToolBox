package com.willy.net.svn;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

public class WSvn {

	public static void main(String[] args) throws SVNException {
		String svnRootUrl = "https://192.168.29.92:8443/svn/bcic";
		WSvn svn = new WSvn(svnRootUrl, "Z00040868", "Z00040868");
		svn.getRecentLogInfoList(10).stream().forEach(log -> System.out.println(log));
		svn.close();
	}

	private SVNRepository repository = null;
	private SVNClientManager clientManager = null;

	public WSvn(String url, String userName, String pwd) {
		super();
		DAVRepositoryFactory.setup();
		try {
			ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(userName, pwd);
			this.repository = SVNRepositoryFactory.create(SVNURL.parseURIEncoded(url));
			this.repository.setAuthenticationManager(authManager);
			DefaultSVNOptions options = SVNWCUtil.createDefaultOptions(true);
			this.clientManager = SVNClientManager.newInstance(options, authManager);
		} catch (SVNException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public List<String> getRecentLogInfoList(long recentRecordCount) {
		List<String> logInfoList = new ArrayList<String>();
		Collection<SVNLogEntry> logEntries = null;

		// repository.info("", -1).getRevision() 最新版號
		try {
			logEntries = repository.log(new String[] { "" }, null, Math.max(1, repository.info("", -1).getRevision() - 100), -1, true,
					true);
		} catch (SVNException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
		logEntries.stream().collect(Collectors.toList());
		Iterator<SVNLogEntry> entries = logEntries.iterator();
		List<SVNLogEntry> logEntrieList = new ArrayList<>();
		while (entries.hasNext()) {
			logEntrieList.add(entries.next());
		}
		Collections.reverse(logEntrieList);
		for (SVNLogEntry logEntry : logEntrieList) {
			if (StringUtils.isNotEmpty(logEntry.getAuthor())) {
				logInfoList.add(svnLogEntryToString(logEntry));
				recentRecordCount--;
			}
			if (recentRecordCount == 0) {
				break;
			}
		}
		return logInfoList;
	}
	
	@SuppressWarnings("unchecked")
	public Map<Long, Set<String>> getChangedFilesBetweenVersions(int startVersNo, int endVersNo) {
		Collection<SVNLogEntry> logEntries = null;
		Set<String> modifiedFileList;
		try {
			logEntries = repository.log(new String[] { "" }, null, startVersNo, endVersNo, true, true);
		} catch (SVNException e) {
			throw new IllegalArgumentException(e.getMessage());
		}

		SVNLogEntry logEntry;
		Map<Long, Set<String>> changedFilesMap = new HashMap<>();
		Iterator<SVNLogEntry> entries = logEntries.iterator();
		while (entries.hasNext()) {
			modifiedFileList = new HashSet<String>();
			logEntry = (SVNLogEntry) entries.next();
			// Modified File Paths
			if (logEntry.getChangedPaths().size() > 0) {
				SVNLogEntryPath entryPath;
				Map<String, SVNLogEntryPath> svnLogMap = logEntry.getChangedPaths();
				Set<String> changedPathsSet = logEntry.getChangedPaths().keySet();
				for (String key : changedPathsSet) {
					entryPath = svnLogMap.get(key);
					if (entryPath.getCopyPath() != null) {
						modifiedFileList.add(entryPath.getPath() + " (from " + entryPath.getCopyPath() + " revision "
								+ entryPath.getCopyRevision() + ")");
					} else {
						modifiedFileList.add(entryPath.getPath());
					}
				}
			}
			changedFilesMap.put(logEntry.getRevision(), modifiedFileList);
		}
		return changedFilesMap;
	}

	public String svnLogEntryToString(SVNLogEntry logEntry) {
		String actionType = "";
		List<String> actionList = new ArrayList<>();
		Map<String, SVNLogEntryPath> changePathMap = logEntry.getChangedPaths();
		Iterator<String> it = changePathMap.keySet().iterator();
		while (it.hasNext()) {
			char myType = logEntry.getChangedPaths().get(it.next()).getType();
			if (!actionList.contains(Character.toString(myType))) {
				actionList.add(Character.toString(myType));
			}
		}
		actionType = (actionList.contains("A") ? "A "
				: "  ") + (actionList.contains("D") ? "D " : "  ") + (actionList.contains("M") ? "M" : "  ");
		return logEntry.getRevision() + "\t" + actionType + "\t"
				+ ((logEntry.getAuthor().length() < 9) ? logEntry.getAuthor() + "    " : logEntry.getAuthor()) + "\t"
				+ logEntry.getDate() + "\t" + logEntry.getMessage().replace("\n", "\t");
	}
	
	public long update(File files) throws SVNException {
		SVNUpdateClient updateClient = this.clientManager.getUpdateClient();
		updateClient.setIgnoreExternals(false);
		return updateClient.doUpdate(files, SVNRevision.HEAD, SVNDepth.INFINITY, false, false);
	}

	// private enum ActionType{
	// A("ADD"),D("DELETE"),M("MODIFY");
	// private String desc;
	// ActionType(String desc) {
	// this.desc=desc;
	// }
	// public String getDesc() {
	// return desc;
	// }
	// }

	public void close() {
		if (this.repository != null)
			this.repository.closeSession();
		if (this.clientManager != null)
			this.clientManager.dispose();
	}
}
