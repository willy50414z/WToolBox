package com.willy.file.zip;

import java.io.File;
import java.util.ArrayList;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

public class WZip {
	private ArrayList<File> zipFilePathList = new ArrayList<File>();
	private ArrayList<File> zipFolderPathList = new ArrayList<File>();

	public void add(String path) {
		File f = new File(path);
		if (f.isDirectory()) {
			zipFolderPathList.add(f);
		} else {
			zipFilePathList.add(f);
		}

	}
	
	public void zip(String[] filePaths, String toPath, String pwd) throws Exception {
		for(String filePath : filePaths) {
			this.add(filePath);
		}
		this.zip(toPath, pwd);
	}

	public void zip(String toPath, String pwd) throws Exception {
		ZipFile zipFile;
		ZipParameters zipParams;
		try {
			zipParams = new ZipParameters();
			zipParams.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
			zipParams.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
			if (!pwd.isEmpty()) {
				zipParams.setEncryptFiles(true);
				zipParams.setEncryptionMethod(Zip4jConstants.ENC_METHOD_STANDARD);
				// zipParams.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
				// zipParams.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
				zipParams.setPassword(pwd);
			}
			if (!this.zipFilePathList.isEmpty()) {
				zipFile = new ZipFile(toPath);
				zipFile.addFiles(this.zipFilePathList, zipParams);
			}
			if (!this.zipFolderPathList.isEmpty()) {
				for (File zipFolderPath : zipFolderPathList) {
					if (new File(toPath).isDirectory()) {
						zipFile = new ZipFile(toPath + zipFolderPath.separator + zipFolderPath.getName() + ".zip");
					} else {
						zipFile = new ZipFile(toPath);
					}
					zipFile.addFolder(zipFolderPath, zipParams);
				}
			}
		} catch (Exception e) {
			throw e;
		} finally {
			this.zipFilePathList.clear();
			this.zipFolderPathList.clear();
		}
	}

	/**
	 * 
	 * 
	 * @param zipFilePath
	 * @param destinationDir
	 * @param pwd
	 * @throws Exception
	 */
	public void unZip(String zipFilePath, String destinationDir, String pwd) throws Exception {
		ZipFile zFile = new ZipFile(zipFilePath);

		if (!zFile.isValidZipFile()) {
			throw new ZipException("This zip file is damaged or invalid");
		}
		checkDir(destinationDir);
		if (zFile.isEncrypted()) {
			if (pwd.isEmpty()) {
				throw new NullPointerException("This zip file is encrypted,but no password");
			} else {
				zFile.setPassword(pwd);
			}
		}
		zFile.extractAll(destinationDir);
	}

	/**
	 * 
	 * 
	 * @param targetPath
	 */
	public static void checkDir(String targetPath) {
		File targetDic = new File(targetPath);
		if (!targetDic.exists()) {
			targetDic.mkdirs();
		}
	}

	public static void main(String[] args) throws Exception {
		WZip zip = new WZip();
		// zip.add("D:/ExportOrders_'2012011109879649'.xml");
		// zip.add("D:/script.sql");
		// zip.zip("D:/AAA.zip", "abc");
		// try {
		new WZip().unZip("D:/AAA.zip", "D:/", "cc");
		// }catch(Exception e) {
		// System.out.println(e);
		// }
	}
}