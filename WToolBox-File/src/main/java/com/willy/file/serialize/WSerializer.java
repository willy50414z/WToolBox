package com.willy.file.serialize;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Base64;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import com.willy.util.string.WString;

@Service
public class WSerializer {
	private static final Logger LOGGER = LoggerFactory.getLogger(WSerializer.class);

	public static void main(String[] args) throws ClassNotFoundException {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("a", "h");
		map.put("b", "b");
		map.put("z", "v");
		map.put("x", "f");
		map.put("s", "r");
		try {
			String serStr = new WSerializer().serializeToString(map);
			System.out.println("Serialed String = "+serStr);
			map = (HashMap<String, String>) new WSerializer().unSerializeByString(serStr);
			System.out.println("unSerialed Object = "+WString.toString(map));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void serialize(String filePath, Object obj) throws IOException {
		ObjectOutputStream oos = null;
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(filePath);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(obj);
		} catch (Exception e) {
			LOGGER.error(obj.getClass().getSimpleName() + "序列化失敗", e);
			throw e;
		} finally {
			if (oos != null) {
				oos.flush();
				oos.close();
			}
			if (fos != null) {
				fos.flush();
				fos.close();
			}
		}
	}

	public Object unSerialize(String filePath) throws IOException, ClassNotFoundException {
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		Object resultObj = null;
		try {
			fis = new FileInputStream(filePath);
			ois = new ObjectInputStream(fis);
			resultObj = ois.readObject();
		} catch (IOException e) {
			LOGGER.error(filePath + "反序列化失敗", e);
			throw e;
		} catch (ClassNotFoundException e) {
			LOGGER.error(filePath + "反序列化失敗", e);
			throw e;
		} finally {
			if (fis != null) {
				fis.close();
			}
			if (ois != null) {
				ois.close();
			}
		}
		return resultObj;
	}

	public String serializeToString(Serializable  ser) throws IOException {
		if(ser == null) {
			return null;
		}
		ByteArrayOutputStream baoStream = null;
		ObjectOutputStream ooStream = null;
		try {
			baoStream = new ByteArrayOutputStream();
			ooStream = new ObjectOutputStream(baoStream);
			ooStream.writeObject(ser);
		} catch (IOException e) {
			LOGGER.error(ser.getClass().getSimpleName() + "序列化失敗", e);
			throw e;
		} finally {
			if (ooStream != null) {
				ooStream.close();
			}
			if (baoStream != null) {
				baoStream.close();
			}
		}
		return Base64.getEncoder().encodeToString(baoStream.toByteArray());
	}
	public Object unSerializeByString(String serializedString) throws IOException, ClassNotFoundException {
		if(StringUtils.isEmpty(serializedString)) {
			return null;
		}
		Object o;
		ObjectInputStream ois = null;
		try {
			byte [] data = Base64.getDecoder().decode(serializedString);
			ois= new ObjectInputStream(new ByteArrayInputStream(  data ) );
			o = ois.readObject();
		} catch (ClassNotFoundException e) {
			LOGGER.error(serializedString + "反序列化失敗", e);
			throw e;
		}finally {
			if(ois != null) {
				ois.close();
			}
		}
        return o;
	}
	public String serializeAndCompressToString(Object obj) throws IOException {
		if(obj == null) {
			return null;
		}
		if(!(obj instanceof Serializable)) {
			throw new IOException("傳入參數未實現序列化");
		}
		ByteArrayOutputStream baoStream = null;
		ObjectOutputStream ooStream = null;
		try {
			baoStream = new ByteArrayOutputStream();
			ooStream = new ObjectOutputStream(baoStream);
			ooStream.writeObject(obj);
		} catch (IOException e) {
			LOGGER.error(obj.getClass().getSimpleName() + "序列化失敗", e);
			throw e;
		} finally {
			if (ooStream != null) {
				ooStream.close();
			}
			if (baoStream != null) {
				baoStream.close();
			}
		}
		return WString.compress(Base64.getEncoder().encodeToString(baoStream.toByteArray()));
	}
	public Object unCompressAndUnserializeByString(String serializedString) throws IOException, ClassNotFoundException {
		if(StringUtils.isEmpty(serializedString)) {
			return null;
		}
		Object o;
		ObjectInputStream ois = null;
		try {
			serializedString = WString.unCompress(serializedString);
			byte [] data = Base64.getDecoder().decode(serializedString);
			ois= new ObjectInputStream(new ByteArrayInputStream(  data ) );
			o = ois.readObject();
		} catch (ClassNotFoundException e) {
			LOGGER.error(serializedString + "反序列化失敗", e);
			throw e;
		}finally {
			if(ois != null) {
				ois.close();
			}
		}
        return o;
	}
}
