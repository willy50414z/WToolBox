package com.willy.file.xml;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.willy.util.reflect.WReflect;

public class WJAXP_Dom {
	public static <T> List<T> getConfig(String path,String parentTag,Class<T> cl) throws Exception {
		T beanInstance = null;
		List<T> beanInstanceList=new ArrayList<T>();
		Field[] fields;
		File inputFile = new File(path);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(inputFile);
		doc.getDocumentElement().normalize();
		NodeList nList = doc.getElementsByTagName(parentTag);
		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				beanInstance=cl.newInstance();
				fields=beanInstance.getClass().getDeclaredFields();
				for(Field f : fields) {
					WReflect.setFieldValue(beanInstance, f.getName(), eElement.getElementsByTagName(f.getName().toUpperCase()).item(0).getTextContent());
				}
			}
			beanInstanceList.add(beanInstance);
		}
		return beanInstanceList;
	}
}
