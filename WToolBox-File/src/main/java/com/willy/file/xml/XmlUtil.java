package com.willy.file.xml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XmlUtil {
  public static DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    String FEATURE = null;
    FEATURE = "http://javax.xml.XMLConstants/feature/secure-processing";
    dbFactory.setFeature(FEATURE, true);
    FEATURE = "http://apache.org/xml/features/disallow-doctype-decl";
    dbFactory.setFeature(FEATURE, true);
    FEATURE = "http://xml.org/sax/features/external-parameter-entities";
    dbFactory.setFeature(FEATURE, false);
    FEATURE = "http://xml.org/sax/features/external-general-entities";
    dbFactory.setFeature(FEATURE, false);
    FEATURE = "http://apache.org/xml/features/nonvalidating/load-external-dtd";
    dbFactory.setFeature(FEATURE, false);
    dbFactory.setXIncludeAware(false);
    dbFactory.setExpandEntityReferences(false);
    dbFactory.setValidating(true);
    dbFactory.setFeature("http://xml.org/sax/features/validation", true);
    return dbFactory.newDocumentBuilder();
  }

  public static Document parse(String xmlStr) throws SAXException, IOException, ParserConfigurationException {
    if (xmlStr == null) {
      return null;
    }
    return getDocumentBuilder().parse(new InputSource(new ByteArrayInputStream(xmlStr.getBytes())));
  }

  public static Document parse(byte[] xmlbyte) throws SAXException, IOException, ParserConfigurationException {
    if (xmlbyte == null) {
      return null;
    }
    return getDocumentBuilder().parse(new ByteArrayInputStream(xmlbyte));
  }

  public static String docToStr(Document doc) throws TransformerFactoryConfigurationError, TransformerException {
    Transformer tf = TransformerFactory.newInstance().newTransformer();
    StringWriter outWriter = new StringWriter();
    StreamResult result = new StreamResult(outWriter);
    tf.transform(new DOMSource(doc), result);
    return outWriter.getBuffer().toString();
  }

  public static boolean combine(Document doc, Element father, Element son) throws Exception {
    boolean isdone = false;
    String son_name = son.getNodeName();
    Element subITEM = null;
    if (!isdone) {
      subITEM = doc.createElement(son_name);
      // 複製Attribute
      if (son.hasAttributes()) {
        NamedNodeMap attributes = son.getAttributes();
        for (int i = 0; i < attributes.getLength(); i++) {
          String attribute_name = attributes.item(i).getNodeName();
          String attribute_value = attributes.item(i).getNodeValue();
          subITEM.setAttribute(attribute_name, attribute_value);
        }
      }
      if (son.getChildNodes().getLength() == 1) {
        String sonText = son.getTextContent();
        if (StringUtils.isNotEmpty(sonText)) {
          subITEM.setTextContent(sonText);
        }
      }
      father.appendChild(subITEM);
    } else {
      subITEM = father;
    }

    // 複製子結點
    NodeList sub_messageItems = son.getChildNodes();
    int sub_item_number = sub_messageItems.getLength();
    if (sub_item_number < 2) {
      isdone = true;
    } else {
      for (int j = 0; j < sub_item_number; j++) {
        Element sub_messageItem = (Element) sub_messageItems.item(j);
        isdone = combine(doc, subITEM, sub_messageItem);
      }
    }

    return isdone;
  }
}