package com.willy.file.xml;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.stereotype.Service;
@Service
public class WJAXB {
	public static Object getBeanListfromXml(String listClass) throws Exception {
		JAXBContext context = JAXBContext.newInstance(Class.forName("com.willy.xmlBeans."+listClass));
		Unmarshaller u = context.createUnmarshaller();
		return u.unmarshal(Thread.currentThread().getContextClassLoader().getResourceAsStream("com/willy/xml/"+listClass+".xml"));
	}
	
}
