package com.willy.file.txt;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.map.LinkedMap;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.willy.file.dictionary.WDictionary;
import com.willy.util.log.WLog;
import com.willy.util.reflect.WReflect;
import com.willy.util.string.WRegex;
import com.willy.util.type.WType;

import lombok.Cleanup;

@Service
public class WFile_txt {
	private String encoding = "UTF-8";
	private String separator = "\t";
	private BufferedWriter fw;
	private FileInputStream is;
	// Read
	// RawType-Array
	
	public static void main(String[] args) {
		String aa = "=\"046044\",=\"08群益          \",\"0\",\"0\",\"0\",\"0\",\"0\",\"0\",\"-10,000\",\"0\",\"0\",\"0\",\"0\",\"10,000\",\"-10,000\",\"-10,000\",";
	}

	public String[][] readToAr(String filePath) throws Exception {
		// TODO Auto-generated method stub
		String[][] result = null;
		String strLine;
		@Cleanup
		FileInputStream is = new FileInputStream(filePath);
		@Cleanup
		InputStreamReader isr = new InputStreamReader(is, encoding);
		@Cleanup
		BufferedReader br = new BufferedReader(isr);// bufferedReader

		// 取得row,col Count
		int maxColCount = 0;
		int rowCount = 0;
		while ((strLine = br.readLine()) != null) {// 將CSV檔字串一列一列讀入並存起來直到沒有列為止
			maxColCount = Math.max(maxColCount,
					(WDictionary.getSubFileName(filePath).equalsIgnoreCase("csv") ? splitCsvRowStrToAr(strLine).length
							: strLine.split(separator).length));
			rowCount++;
		}
		// 塞入陣列
		is = new FileInputStream(filePath);
		isr = new InputStreamReader(is, encoding);
		br = new BufferedReader(isr);
		result = new String[rowCount][maxColCount];
		rowCount = 0;
		while ((strLine = br.readLine()) != null) {
			result[rowCount] = (WDictionary.getSubFileName(filePath).equalsIgnoreCase("csv")
					? splitCsvRowStrToAr(strLine)
					: strLine.split(separator));
			rowCount++;
		}
		return result;
	}

	public String[][] readToAr(String filePath, String startKeyWord, String endKeyWord, boolean skipHeader)
			throws Exception {
		String strLine;
		String[] rowStrAr;
		List<String[]> rowStrList = new ArrayList<String[]>();
		boolean startRead = false;
		@Cleanup
		FileInputStream is = new FileInputStream(filePath);
		@Cleanup
		InputStreamReader isr = new InputStreamReader(is, encoding);
		@Cleanup
		BufferedReader br = new BufferedReader(isr);// bufferedReader
		while ((strLine = br.readLine()) != null) {
			// 判斷是否開始讀取
			if (strLine.indexOf(startKeyWord) > -1 || startRead) {// 開始讀取所需的資料
				startRead = true;
			} else {
				continue;
			}
			if (skipHeader) {
				skipHeader = false;
				continue;
			}
			if (endKeyWord != null && (strLine.length() == 0 || strLine.indexOf(endKeyWord) > -1)) {
				break;
			}
			// 若為CSV檔，去除頭尾"
			rowStrAr = (WDictionary.getSubFileName(filePath).equalsIgnoreCase("csv") ? splitCsvRowStrToAr(strLine)
					: strLine.split(separator));
			rowStrList.add(rowStrAr);
		}
		return WType.strArListToArray2D(rowStrList);
	}

	public String readToStr(String filePath) throws IOException {
		String strLine;
		StringBuffer resultStr = new StringBuffer();
		@Cleanup
		FileInputStream is = new FileInputStream(filePath);
		@Cleanup
		InputStreamReader isr = new InputStreamReader(is, encoding);
		@Cleanup
		BufferedReader br = new BufferedReader(isr);// bufferedReader
		while ((strLine = br.readLine()) != null) {// 將CSV檔字串一列一列讀入並存起來直到沒有列為止
			resultStr.append(strLine).append("\r\n");
		}
		return resultStr.toString();
	}

	public List<LinkedMap<String, String>> readToMapList(String filePath, String[] columnsName, String startKey,
			String endKey, boolean readHeader) throws Exception {
		// TODO Auto-generated method stub
		LinkedMap<String, String> resultMap = new LinkedMap<String, String>();
		List<LinkedMap<String, String>> resultList = new ArrayList<LinkedMap<String, String>>();
		String strLine;
		@Cleanup
		FileInputStream is = new FileInputStream(filePath);
		@Cleanup
		InputStreamReader isr = new InputStreamReader(is, encoding);
		@Cleanup
		BufferedReader br = new BufferedReader(isr);// bufferedReader
		// 塞入陣列
		int colIndex;
		String[] rowAr;
		br = new BufferedReader(isr);
		while ((strLine = br.readLine()) != null) {// 將CSV檔字串一列一列讀入並存起來直到沒有列為止
			rowAr = (WDictionary.getSubFileName(filePath).equalsIgnoreCase("csv") ? splitCsvRowStrToAr(strLine)
					: strLine.split(separator));
			for (colIndex = 0; colIndex < columnsName.length; colIndex++) {
				resultMap.put(columnsName[colIndex], rowAr[colIndex]);
			}
			colIndex = 0;
			resultList.add(resultMap);
			resultMap = new LinkedMap<String, String>();
		}
		return resultList;
	}

	public <T> List<T> readToBeanList(String filePath, String[] columnNames, Class<T> cl, String startKeyWord,
			String endKeyWord, boolean skipHeader) throws Exception {
		String strLine;
		String[] rowStrAr;
		T bean = null;
		List<T> beanList = new ArrayList<T>();
		boolean startRead = false;
		@Cleanup
		FileInputStream is = new FileInputStream(filePath);
		@Cleanup
		InputStreamReader isr = new InputStreamReader(is, encoding);
		@Cleanup
		BufferedReader br = new BufferedReader(isr);// bufferedReader
		while ((strLine = br.readLine()) != null) {
			// 判斷是否開始讀取
			if (strLine.indexOf(startKeyWord) > -1 || startRead) {// 開始讀取所需的資料
				startRead = true;
			} else {
				continue;
			}
			if (skipHeader) {
				skipHeader = false;
				continue;
			}
			if (strLine.length() == 0 || strLine.indexOf(endKeyWord) > -1) {
				break;
			}
			// 若為CSV檔，去除頭尾"
			rowStrAr = (WDictionary.getSubFileName(filePath).equalsIgnoreCase("csv") ? splitCsvRowStrToAr(strLine)
					: strLine.split(separator));
			bean = cl.newInstance();
			for (int colIndex = 0; colIndex < Math.min(columnNames.length, rowStrAr.length); colIndex++) {
				String colName = columnNames[colIndex];
				// 欄位名稱不為空才加入物件
				if (!StringUtils.isEmpty(colName)) {
					WLog.debug("WFile_txt\t-\t 將" + rowStrAr[colIndex].replace("\"", "") + "\t塞入 : " + colName);
					WReflect.setFieldValue(bean, colName, rowStrAr[colIndex].replace("\"", ""));
				}
			}
			beanList.add(bean);
		}
		return beanList;
	}
	// Write

	public void writeByAr(String filePath, String[][] dataAr) throws Exception {
		StringBuffer tempRowStr = new StringBuffer();
		for (String[] rowAr : dataAr) {
			for (String cell : rowAr) {
				tempRowStr.append(cell).append(separator);
			}
			tempRowStr.delete(0, tempRowStr.length());
		}
		writeByStr(filePath, tempRowStr.toString());
	}

	public void writeByStr(String filePath, String str) throws IOException {
		writeByStr(filePath, str, true);
	}

	public void writeByStr(String filePath, String str, boolean isAppend) throws IOException {
		try {
			WDictionary.mergeDir(new File(filePath).getParent());
			File file = new File(filePath);
			fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, isAppend), encoding)); // 指點編碼格式，以免讀取時中文字符異常
			fw.append(str);
		} finally {
			if(fw!=null) {
				fw.flush();
				fw.close();
			}
		}
	}

	/**
	 * 分割CSV檔列字串
	 * 
	 * @param rowStr
	 * @return
	 */
	public String[] splitCsvRowStrToAr(String rowStr) {
		rowStr = rowStr.startsWith("=") ? rowStr.substring(1) : rowStr;
		rowStr = rowStr.replace("=\"", "\"");
		List<String> rowStrList = WRegex.getMatchedStrList(rowStr,
				"\\G(?:^|,)(?:\"([^\"]*+(?:\"\"[^\"]*+)*+)\"|([^\",]*+))");
		String[] resultAr = new String[rowStrList.size()];
		for (int cellIndex = 0; cellIndex < resultAr.length; cellIndex++) {
			rowStr = rowStrList.get(cellIndex);
			if (rowStr.endsWith("\"")) {
				resultAr[cellIndex] = rowStr.substring(rowStr.indexOf("\"") + 1, rowStr.lastIndexOf("\""));
			} else {
				resultAr[cellIndex] = rowStr.substring((rowStr.startsWith(",") ? 1 : 0));
			}
		}
		return resultAr;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}
}
