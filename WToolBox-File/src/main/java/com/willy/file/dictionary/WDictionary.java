package com.willy.file.dictionary;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Service;

import com.willy.util.log.WLog;

import lombok.Cleanup;

@Service
public class WDictionary {
	/**
	 * 取得檔案資料夾路徑
	 * 
	 * @param filePath
	 * @return
	 */
	public static String getFileDir(String filePath) {
		return new File(filePath).getParent() + File.separator;
	}

	/**
	 * 取得上層檔案路徑
	 * 
	 * @param dir
	 * @return
	 */
	public static String getParentDir(String dir) {
		return new File(dir).getParent();
	}

	/**
	 * 取得檔案名稱
	 * 
	 * @param filePath
	 * @return
	 */
	public static String getFileName(String filePath) {
		return new File(filePath).getName();
	}

	/**
	 * 取得檔案附檔名
	 * 
	 * @param filePath
	 * @return
	 */
	public static String getSubFileName(String filePath) {
		return filePath.substring(filePath.lastIndexOf(".") + 1).toLowerCase();
	}

//Use
	/**
	 * 移動檔案
	 * 
	 * @param fromPath
	 * @param toPath
	 */
	public static void move(String fromPath, String toPath) {
		File file = new File(fromPath);
		if(!file.exists()) {
			WLog.error(file.getAbsolutePath()+" is not exist!!!");
			return;
		}
		WDictionary.mergeDir(new File(toPath).getParent());
		file.renameTo(new File(toPath));
	}

	/**
	 * 複製檔案，10秒沒完成報錯
	 * 
	 * @param fromPath
	 * @param toPath
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static void copyTo(String fromPath, String toPath) throws IOException {
		if(!new File(fromPath).exists()) {
			WLog.error(fromPath + " is not exist");
			return;
		}
		mergeDir(new File(toPath).getParent());
		@Cleanup
		InputStream input = new FileInputStream(fromPath);
		@Cleanup
		OutputStream output = new FileOutputStream(toPath);
		byte[] buf = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buf)) > 0) {
			output.write(buf, 0, bytesRead);
		}
	}

	/**
	 * merge路徑(沒有該路徑就自己創建)
	 * 
	 * @param fileDir
	 */
	public static void mergeDir(String fileDir) {
		File dir = new File(fileDir);
		if (!dir.exists()) {
			dir.mkdirs();
		}
	}

	public static String rename(String oldFilePath, String newFileName) {
		String fileDic = WDictionary.getFileDir(oldFilePath);
		new File(oldFilePath).renameTo(new File(fileDic + newFileName));
		return fileDic + newFileName;
	}
	
	public static File[] getAllFileInDir(String rootDic, boolean isRecursive) {
		if(isRecursive) {
			List<File> fileList = new ArrayList<File> ();
			File[] fileAr = new File(rootDic).listFiles();
			for(File file : fileAr) {
				if(file.isDirectory()) {
					//資料夾繼續往下撈
					Collections.addAll(fileList, getAllFileInDir(file.getAbsolutePath(),isRecursive));
				}else {
					//檔案放入List
					fileList.add(file);
				}
			}
			fileList.toArray(fileAr);
			return fileAr;
		}else {
			return new File(rootDic).listFiles();
		}
	}

	public static File[] getAllFileInDirFilterByName(String dic, String partialFileName) {
		FileFilter filefilter = new FileFilter() {
			public boolean accept(File file) {
				// if the file extension is .txt return true, else false
				if (file.getName().indexOf(partialFileName) > -1) {
					return true;
				}
				return false;
			}
		};
		return new File(dic).listFiles(filefilter);
	}

	public static String getUniqueFileName(String dir, String fileNameTempl) {
		int uniqueNum = 0;
		String fileName = fileNameTempl.substring(0, fileNameTempl.lastIndexOf("."));
		String subFileName = WDictionary.getSubFileName(fileNameTempl);
		String uniqueFileName = dir + fileNameTempl;
		while (new File(uniqueFileName).exists()) {
			uniqueFileName = fileName + "_" + uniqueNum + "." +subFileName;
			uniqueNum++;
		}
		return uniqueFileName;
	}

	public static void delete(String dir) {
		delete(new File(dir));
	}
	public static void delete(File file) {
		if (!file.exists()) {
			return;
		}
		if (file.isFile() || file.list().length == 0) {
			file.delete();
		} else {
			for (File f : file.listFiles()) {
				delete(f);
			}
			file.delete();
		}
	}
}
