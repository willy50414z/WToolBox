package com.willy.file.bean;

public class ExcelBean<T> {
	private String sheetName;
	private T data;
	
	public ExcelBean(String sheetName, T data) {
		super();
		this.sheetName = sheetName;
		this.data = data;
	}
	public String getSheetName() {
		return sheetName;
	}
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	
}
