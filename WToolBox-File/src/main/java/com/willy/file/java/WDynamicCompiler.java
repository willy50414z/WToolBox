package com.willy.file.java;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import org.springframework.stereotype.Service;

@Service
public class WDynamicCompiler {
	public static void compile(String... files) throws Exception {
		ArrayList<String> ops = new ArrayList<String>();
		ops.add("-Xlint:unchecked");
		ops.add("-encoding");
		ops.add("UTF-8");
		compile(ops, files);
	}
	public static void compile(List<String> ops, String... files) throws Exception {
		JavaCompiler compiler;
		StandardJavaFileManager manager = null;
		try {
			compiler = ToolProvider.getSystemJavaCompiler();
			manager = compiler.getStandardFileManager(null, null, null);
			Iterable<? extends JavaFileObject> it = manager.getJavaFileObjects(files);
			JavaCompiler.CompilationTask task = compiler.getTask(null, manager, null, ops, null, it);
			task.call();
		} catch (Exception e) {
			throw e;
		} finally {
			if (manager != null) {
				try {
					manager.close();
				} catch (IOException e) {
					throw e;
				}
			}
		}
	}
	public static Class<?> loadClass(String packageDir, String className) throws MalformedURLException, ClassNotFoundException {
		File file = new File(packageDir);
        URLClassLoader loader = URLClassLoader
                .newInstance(new URL[] { file.toURI().toURL()});
		return loader.loadClass(className);
	}
}
