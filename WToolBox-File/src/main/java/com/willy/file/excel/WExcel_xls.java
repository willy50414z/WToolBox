package com.willy.file.excel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections4.map.LinkedMap;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Service;

import com.willy.file.bean.ExcelBean;
import com.willy.util.reflect.WReflect;

@Service
public class WExcel_xls{
	//每10000列寫入硬碟，減少記憶體占用
	private SXSSFWorkbook sxssWb;
	private SXSSFSheet sxssSheet;
	private HSSFWorkbook hssfWb;
	private SXSSFRow row;
	private SXSSFCell cell;
	private InputStream is;
	private FileOutputStream fos;
	private Iterator cellIter;
	private Iterator rowIter;

	
	public String[][] readToStrAr(String filePath) throws IOException {
		// TODO Auto-generated method stub
		String[][] result;
		try {
			is = new FileInputStream(filePath);
			hssfWb = new HSSFWorkbook(is);
			result = sheetToExcelBeanStrAr(hssfWb.getSheetAt(0)).getData();
		} finally {
			is.close();
			hssfWb.close();
		}
		return result;
	}
//
//	public LinkedList<String[][]> readAllToListAr(String filePath) throws IOException {
//		// TODO Auto-generated method stub
//		LinkedList<String[][]> xlsxSheetArList;
//		try {
//			is = new FileInputStream(filePath);
//			wb = new SXSSFWorkbook(is);
//			xlsxSheetArList = new LinkedList<String[][]>();
//			for (int sheetIndex = 0; sheetIndex < wb.getNumberOfSheets(); sheetIndex++) {
//				xlsxSheetArList.add(sheetToExcelBeanAr(wb.getSheetAt(sheetIndex)).getData());
//			}
//		} finally {
//			is.close();
//			wb.close();
//		}
//		return xlsxSheetArList;
//	}
//
//	public List<LinkedMap> readToMapList(String filePath, String[] columnsName) throws Exception {
//		List<LinkedMap> resultList;
//		LinkedMap<String, String> resultMap;
//		try {
//			is = new FileInputStream(filePath);
//			wb = new SXSSFWorkbook(is);
//			resultMap = new LinkedMap<String, String>();
//			resultList = new ArrayList<LinkedMap>();
//			sheet = wb.getSheetAt(0);
//
//			int colIndex = 0;
//			rowIter = sheet.iterator();
//			// 逐列遍覽
//			while (rowIter.hasNext()) {
//				row = (SXSSFRow) rowIter.next();
//				cellIter = row.iterator();
//				// 逐欄遍覽
//				while (cellIter.hasNext()) {
//					cell = (SXSSFCell) cellIter.next();
//					resultMap.put(columnsName[colIndex], cell.getStringCellValue());
//					colIndex++;
//				}
//				colIndex = 0;
//				resultList.add(resultMap);
//				resultMap = new LinkedMap<String, String>();
//			}
//		} finally {
//			is.close();
//			wb.close();
//		}
//		return resultList;
//	}

	private ExcelBean<String[][]> sheetToExcelBeanStrAr(HSSFSheet sheet) {
		ExcelBean<String[][]> excelBean;
		// 取得maxColumnCount
		int maxColCount = 0;
		rowIter = sheet.iterator();
		while (rowIter.hasNext()) {
			row = (SXSSFRow) rowIter.next();
			maxColCount = (row.getLastCellNum() > maxColCount) ? row.getLastCellNum() : maxColCount;// 取得最大攔位數
		}
		// 將sheet塞入Arrary
		int rowIndex = 0;
		int colIndex = 0;
		String[][] sheetAr = new String[sheet.getLastRowNum() + 1][maxColCount];
		rowIter = sheet.iterator();
		while (rowIter.hasNext()) {
			row = (SXSSFRow) rowIter.next();
			cellIter = row.iterator();
			while (cellIter.hasNext()) {
				cell = (SXSSFCell) cellIter.next();
				sheetAr[rowIndex][colIndex] = getCellValue(cell);
				colIndex++;
			}
			colIndex = 0;
			rowIndex++;
		}
		excelBean = new ExcelBean<String[][]>(sheet.getSheetName(), sheetAr);
		return excelBean;
	}

	
	public void writeByAr(String filePath, String[][] dataAr) throws Exception {
		// TODO Auto-generated method stub

		try {
			sxssWb = new SXSSFWorkbook(10000);
			sxssSheet = sxssWb.createSheet("Sheet1");// Default sheet Name
			for (int rowIndex = 0; rowIndex < dataAr.length; rowIndex++) {
				row = sxssSheet.createRow(rowIndex);
				for (int colIndex = 0; colIndex < dataAr[0].length; colIndex++) {
					cell = row.createCell(colIndex);
					cell.setCellValue(dataAr[rowIndex][colIndex]);
				}
			}
			fos = new FileOutputStream(filePath);
			sxssWb.write(fos);
		} finally {
			fos.flush();
			fos.close();
			sxssWb.close();
		}
	}

	public <T> List<T> readToBeanList(String filName, String[] columnsName, String startKey, String endKey,
			boolean containsHeader) throws Exception {
		// TODO Auto-generated method stub
		int rowIndex = 0;
		int colIndex = 0;
		T bean = null;
		List<T> beanList = new ArrayList<T>();
		rowIter = sxssSheet.iterator();
		while (rowIter.hasNext()) {
			row = (SXSSFRow) rowIter.next();
			cellIter = row.iterator();
			while (cellIter.hasNext()) {
				cell = (SXSSFCell) cellIter.next();
				WReflect.setFieldValue(bean, columnsName[colIndex], getCellValue(cell));
				colIndex++;
			}
			beanList.add(bean);
			colIndex = 0;
			rowIndex++;
		}
		return beanList;
	}

	public String getCellValue(Cell cell) {
		// TODO Auto-generated method stub
		String cellValue;
		cell=(HSSFCell) cell;
		switch (cell.getCellType().name()) {
		case "STRING":
			cellValue = cell.getStringCellValue();
			break;
		case "NUMERIC":
			cellValue = String.valueOf(cell.getNumericCellValue());
			break;
		case "BOOLEAN":
			cellValue = String.valueOf(cell.getBooleanCellValue());
			break;
		case "DATE":
			cellValue = String.valueOf(cell.getDateCellValue());
			break;
		default:
			cellValue = "";
			break;
		}
		return cellValue;
	}

	
	public ExcelBean readToExcelBean_Ar(String filePath) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	
	public LinkedList<String[][]> readToList_Ar(String filePath) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	
	public LinkedList<ExcelBean> readToExcelBeanList_Ar(String filePath) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	
	public List<LinkedMap> readToMapList(String filePath, String[] columnsName, String startKey, String endKey,
			boolean readHeader) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	
	public <T> List<T> readToBeanList(String filePath, String[] columnsName, Class<T> cl, String startKey,
			String endKey, boolean readHeader) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
