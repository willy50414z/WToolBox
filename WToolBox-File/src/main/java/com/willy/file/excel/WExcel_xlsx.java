package com.willy.file.excel;

import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.map.LinkedMap;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.willy.file.bean.ExcelBean;
import com.willy.util.log.WLog;
import com.willy.util.reflect.WReflect;

@Service
public class WExcel_xlsx {
	private String seperator = "\t";
	private int skipRowCount = -1;
	private int rowIndex = 0;
	private int colIndex = 0;
	private XSSFRow row;
	private XSSFCell cell;
	private SXSSFRow sxssRow;
	private SXSSFCell sxssCell;
	private Iterator cellIter;
	private Iterator rowIter;
	private InputStream is;
	private XSSFWorkbook wb;
	private XSSFSheet sheet;
	private SXSSFSheet sxSheet;
	private BufferedOutputStream bos;
	private String[][] result;
	private List<LinkedMap> resultList;
	private boolean inReadRange = false;
	private SXSSFWorkbook emptyWorkBook = new SXSSFWorkbook(10000);
//	@Autowired
	private CellStyle defaultCellStyle;

	// Read
	// RawType-Array
	
	public String[][] readToStrAr(String filePath) throws Exception {
		// TODO Auto-generated method stub
		try {
			is = new FileInputStream(filePath);
			wb = new XSSFWorkbook(is);
			this.sheet = wb.getSheetAt(0);
			result = sheetToExcelBean_StrAr().getData();
		} finally {
			is.close();
			wb.close();
		}
		return result;
	}

	
	public LinkedList<String[][]> readToList_Ar(String filePath) throws Exception {
		// TODO Auto-generated method stub
		LinkedList<String[][]> xlsxSheetArList;
		try {
			is = new FileInputStream(filePath);
			wb = new XSSFWorkbook(is);
			xlsxSheetArList = new LinkedList<String[][]>();
			for (int sheetIndex = 0; sheetIndex < wb.getNumberOfSheets(); sheetIndex++) {
				this.sheet = wb.getSheetAt(sheetIndex);
				xlsxSheetArList.add(sheetToExcelBean_StrAr().getData());
			}
		} finally {
			is.close();
			wb.close();
		}
		return xlsxSheetArList;
	}

	// RawType-MapList
	
	public List<LinkedMap> readToMapList(String filePath, String[] columnsName, String startKey, String endKey,
			boolean readHeader) throws Exception {
		try {

			resultList = new ArrayList<LinkedMap>();
			LinkedMap<String, String> resultMap = new LinkedMap<String, String>();

			is = new FileInputStream(filePath);
			wb = new XSSFWorkbook(is);

			sheet = wb.getSheetAt(0);
			rowIter = sheet.iterator();
			// 逐列遍覽
			while (rowIter.hasNext()) {
				row = (XSSFRow) rowIter.next();
				// 檢測是否開始讀取
				if (!inReadRange) {
					containsKeyWord(row, startKey);
					if (!inReadRange || !readHeader) {
						continue;
					}
				}
				// 檢測是否停止讀取
				if (inReadRange && containsKeyWord(row, endKey) && endKey != null) {// 已經開始讀才判斷
					break;
				}
				// 讀取
				for (int columnIndex = 0; columnIndex < columnsName.length; columnIndex++) {
					cell = row.getCell(columnIndex);
					resultMap.put(columnsName[columnIndex], getStrCellValue(cell));
				}
				// 逐欄遍覽
				resultList.add(resultMap);
				resultMap = new LinkedMap<String, String>();
			}
		} finally {
			is.close();
			wb.close();
		}
		return resultList;
	}

	// ExcelBean
	
	public ExcelBean readToExcelBean_Ar(String filePath) throws Exception {
		// TODO Auto-generated method stub
		ExcelBean result = null;
		try {
			is = new FileInputStream(filePath);
			wb = new XSSFWorkbook(is);
			this.sheet = wb.getSheetAt(0);
			result = sheetToExcelBean_StrAr();
		} finally {
			is.close();
			wb.close();
		}
		return result;
	}

	
	public LinkedList<ExcelBean> readToExcelBeanList_Ar(String filePath) throws Exception {
		// TODO Auto-generated method stub
		LinkedList<ExcelBean> excelBeanList_Ar;
		try {
			is = new FileInputStream(filePath);
			wb = new XSSFWorkbook(is);
			excelBeanList_Ar = new LinkedList<ExcelBean>();
			for (int sheetIndex = 0; sheetIndex < wb.getNumberOfSheets(); sheetIndex++) {
				this.sheet = wb.getSheetAt(sheetIndex);
				excelBeanList_Ar.add(sheetToExcelBean_StrAr());
			}
		} finally {
			is.close();
			wb.close();
		}
		return excelBeanList_Ar;
	}

	// Class
	public <T> List<T> readToBeanList(String filePath, String[] columnsName, Class<T> cl, boolean readHeader)
			throws Exception {
		return readToBeanList(filePath, columnsName, cl, null, null, readHeader);
	}

	
	public <T> List<T> readToBeanList(String filePath, String[] columnsName, Class<T> cl, String startKey,
			String endKey, boolean readHeader) throws Exception {
		// TODO Auto-generated method stub
		List<T> beanList = new ArrayList<T>();
		try {
			is = new FileInputStream(filePath);
			wb = new XSSFWorkbook(is);
			sheet = wb.getSheetAt(0);
			T bean = cl.newInstance();
			rowIter = sheet.iterator();
			while (rowIter.hasNext()) {
				row = (XSSFRow) rowIter.next();
				// 檢測是否開始讀取
				if (!inReadRange) {
					containsKeyWord(row, startKey);
					if (!inReadRange || !readHeader) {
						continue;
					}
				}
				// 檢測是否停止讀取
				if (inReadRange && containsKeyWord(row, endKey) && endKey != null) {// 已經開始讀才判斷
					break;
				}
				for (int columnIndex = 0; columnIndex < Math.min(columnsName.length,
						row.getLastCellNum()); columnIndex++) {
					cell = row.getCell(columnIndex);
					WReflect.setFieldValue(bean, columnsName[columnIndex], getStrCellValue(cell));
				}
				beanList.add(bean);
				bean = cl.newInstance();
			}
		}catch(Exception e) {
			throw e;
		}finally {
			wb.close();
			is.close();
		}
		return beanList;
	}

	// Write
	
	public void writeByAr(String filePath, String[][] dataAr) throws Exception {
		writeByAr(filePath, "Sheet1", dataAr);
	}
	
	public void writeByArList(String filePath, List<String[]> dataAr) throws Exception {
		writeByArList(filePath, "Sheet1", dataAr);
	}

	public void writeByAr(String filePath, String sheetName, String[][] dataAr) throws Exception {
		//ColNum,Length
		String cellValue;
		HashMap<Integer,Integer> cellValueLengthMap=new HashMap<Integer,Integer>();//取得各欄最大長度
		try {
			getDefaultCellStyle();
			sxSheet = emptyWorkBook.createSheet(sheetName);// Default sheet Name
			for (rowIndex = 0; rowIndex < dataAr.length; rowIndex++) {
				sxssRow = sxSheet.createRow(rowIndex);
				for (int colIndex = 0; colIndex < dataAr[0].length; colIndex++) {
					cellValue=(dataAr[rowIndex][colIndex]==null?"":dataAr[rowIndex][colIndex]);
					sxssCell = sxssRow.createCell(colIndex);
					sxssCell.setCellStyle(defaultCellStyle);
					sxssCell.setCellValue(cellValue);
					if(cellValueLengthMap.get(colIndex)==null || cellValue.length()>cellValueLengthMap.get(colIndex)) {
						cellValueLengthMap.put(colIndex, cellValue.length());
					}
					
				}
			}
			Set<Integer> keySet=cellValueLengthMap.keySet();
			for(int key : keySet) {
				sheet.setColumnWidth(key, Math.min(cellValueLengthMap.get(key)*400,25500));
			}
			bos = new BufferedOutputStream(new FileOutputStream(filePath));
			emptyWorkBook.write(bos);
		} catch (Exception e) {
			WLog.error(e);
		} finally {
			if(bos!=null) {
				bos.flush();
				bos.close();
			}
			
		}
	}
	
	public void writeByArList(String filePath, String sheetName, List<String[]> dataArList) throws Exception {
		//ColNum,Length
		String cellValue;
		HashMap<Integer,Integer> cellValueLengthMap=new HashMap<Integer,Integer>();//取得各欄最大長度
		try {
			getDefaultCellStyle();
			sxSheet = emptyWorkBook.createSheet(sheetName);// Default sheet Name
			int rowIndex=0;
			for(String[] rowAr : dataArList) {
				sxssRow = sxSheet.createRow(rowIndex);
				for (int colIndex = 0; colIndex < rowAr.length; colIndex++) {
					cellValue=(rowAr[colIndex]==null?"":rowAr[colIndex]);
					sxssCell = sxssRow.createCell(colIndex);
					sxssCell.setCellStyle(defaultCellStyle);
					sxssCell.setCellValue(cellValue);
					if(cellValueLengthMap.get(colIndex)==null || cellValue.length()>cellValueLengthMap.get(colIndex)) {
						cellValueLengthMap.put(colIndex, cellValue.length());
					}
					
				}
				rowIndex++;
			}
			Set<Integer> keySet=cellValueLengthMap.keySet();
			for(int key : keySet) {
				sxSheet.setColumnWidth(key, Math.min(cellValueLengthMap.get(key)*400,25500));
			}
			bos = new BufferedOutputStream(new FileOutputStream(filePath));
			emptyWorkBook.write(bos);
		} catch (Exception e) {
			WLog.error(e);
		} finally {
			if(bos!=null) {
				bos.flush();
				bos.close();
			}
			
		}
	}

	// Tools
	private ExcelBean<String[][]> sheetToExcelBean_StrAr() {
		// TODO Auto-generated method stub
		// 取得maxColumnCount
		int maxColCount = 0;
		rowIndex = 0;
		this.sheet = (XSSFSheet) sheet;
		rowIter = this.sheet.iterator();
		while (rowIter.hasNext()) {
			row = (XSSFRow) rowIter.next();
			maxColCount = (row.getLastCellNum() > maxColCount) ? row.getLastCellNum() : maxColCount;// 取得最大攔位數
		}
		// 將sheet塞入Arrary
		String[][] result = new String[this.sheet.getLastRowNum() + 1][maxColCount];
		rowIter = this.sheet.iterator();
		while (rowIter.hasNext()) {
			row = (XSSFRow) rowIter.next();
			cellIter = row.iterator();
			while (cellIter.hasNext()) {
				cell = (XSSFCell) cellIter.next();
				result[cell.getAddress().getRow()][cell.getAddress().getColumn()] = getStrCellValue(cell);
				colIndex++;
			}
			colIndex = 0;
			rowIndex++;
		}
		return new ExcelBean<String[][]>(this.sheet.getSheetName(), result);
	}

	/**
	 * 
	 * @param row
	 * @param keyWord
	 * @return
	 */
	private boolean containsKeyWord(XSSFRow row, String keyWord) {
		boolean rowInReadRange = false;
		if (keyWord == null) {
			this.inReadRange = true;
			rowInReadRange = true;
		} else if (keyWord.startsWith("skip")) {
			if (skipRowCount < 0) {// 第一次檢測，紀錄跳過行數
				this.skipRowCount = Integer.valueOf(keyWord.split(":")[1]);
				this.skipRowCount--;
			} else if (skipRowCount == 0) {// 跳完了，開始讀取
				this.inReadRange = true;
				rowInReadRange = true;
				this.skipRowCount = -1;
			} else {// 還在跳
				this.skipRowCount--;
			}
		} else {
			StringBuffer rowStr = new StringBuffer();
			cellIter = row.cellIterator();
			while (cellIter.hasNext()) {
				cell = (XSSFCell) cellIter.next();
				rowStr.append(getStrCellValue(cell)).append(seperator);
			}
			if (rowStr.indexOf(keyWord) > -1) {
				this.inReadRange = true;
				rowInReadRange = true;
			}
		}
		return rowInReadRange;
	}

	public String getSeperator() {
		return seperator;
	}

	public void setSeperator(String seperator) {
		this.seperator = seperator;
	}

	private ExcelBean<String[][]> sheetToExcelBean_Map() {
		// TODO Auto-generated method stub
		// 取得maxColumnCount
		int maxColCount = 0;
		rowIndex = 0;
		this.sheet = (XSSFSheet) sheet;
		rowIter = this.sheet.iterator();
		while (rowIter.hasNext()) {
			row = (XSSFRow) rowIter.next();
			maxColCount = (row.getLastCellNum() > maxColCount) ? row.getLastCellNum() : maxColCount;// 取得最大攔位數
		}
		// 將sheet塞入Arrary
		String[][] result = new String[this.sheet.getLastRowNum() + 1][maxColCount];
		rowIter = this.sheet.iterator();
		while (rowIter.hasNext()) {
			row = (XSSFRow) rowIter.next();
			cellIter = row.iterator();
			while (cellIter.hasNext()) {
				cell = (XSSFCell) cellIter.next();
				result[rowIndex][colIndex] = getStrCellValue(cell);
				colIndex++;
			}
			colIndex = 0;
			rowIndex++;
		}
		return new ExcelBean<String[][]>(this.sheet.getSheetName(), result);
	}

	private String getStrCellValue(Cell cell) {
		// TODO Auto-generated method stub
		if (cell == null) {
			return "";
		}
		String cellValue;
		XSSFCell xssfCell = (XSSFCell) cell;
		switch (xssfCell.getCellType().name()) {
		case "STRING":
			cellValue = xssfCell.getStringCellValue();
			break;
		case "NUMERIC":
			cellValue = String.valueOf(xssfCell.getNumericCellValue());
			break;
		case "BOOLEAN":
			cellValue = String.valueOf(xssfCell.getBooleanCellValue());
			break;
		case "DATE":
			cellValue = String.valueOf(xssfCell.getDateCellValue());
			break;
		default:
			cellValue = "";
			break;
		}
		return cellValue;
	}
	public CellStyle getDefaultCellStyle() {
		XSSFFont font = (XSSFFont) emptyWorkBook.createFont();
		font.setFontName("標楷體"); // 字体
		// font.setItalic(true); //是否使用斜体
		// font.setStrikeout(true); //是否使用划线
		XSSFCellStyle cellStyle = (XSSFCellStyle) emptyWorkBook.createCellStyle();
		cellStyle.setFont(font);
		cellStyle.setWrapText(true);
		this.defaultCellStyle=cellStyle;
		return cellStyle;
	}
}
